/*
 Navicat Premium Data Transfer

 Source Server         : 222.186.34.109
 Source Server Type    : MySQL
 Source Server Version : 80300 (8.3.0)
 Source Host           : 222.186.34.109:3307
 Source Schema         : jc_club

 Target Server Type    : MySQL
 Target Server Version : 80300 (8.3.0)
 File Encoding         : 65001

 Date: 20/02/2024 16:43:51
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '权限名称',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父id',
  `type` tinyint NULL DEFAULT NULL COMMENT '权限类型 0菜单 1操作',
  `menu_url` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '菜单路由',
  `status` tinyint NULL DEFAULT NULL COMMENT '状态 0启用 1禁用',
  `shows` tinyint NULL DEFAULT NULL COMMENT '展示状态 0展示 1隐藏',
  `icon` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '图标',
  `permission_key` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '权限唯一标识',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
INSERT INTO `auth_permission` VALUES (1, '新增题目', 0, 1, '1adiwd/awdw', 0, 0, 'httt://1.png', 'subject:add', NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for auth_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_role`;
CREATE TABLE `auth_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `role_name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_key` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '角色唯一标识',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未被删除，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_role
-- ----------------------------
INSERT INTO `auth_role` VALUES (1, '管理员', 'admin_user', NULL, NULL, NULL, NULL, 1);
INSERT INTO `auth_role` VALUES (2, '普通用户', 'normal_user', NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for auth_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_role_permission`;
CREATE TABLE `auth_role_permission`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色id',
  `permission_id` bigint NULL DEFAULT NULL COMMENT '权限id',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_role_permission
-- ----------------------------
INSERT INTO `auth_role_permission` VALUES (1, 2, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `auth_role_permission` VALUES (6, 1, 1, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for auth_user
-- ----------------------------
DROP TABLE IF EXISTS `auth_user`;
CREATE TABLE `auth_user`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '用户名称/账号',
  `nick_name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `email` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '电话',
  `password` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '密码',
  `sex` tinyint NULL DEFAULT NULL COMMENT '性别',
  `avatar` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '头像',
  `status` tinyint NULL DEFAULT 0 COMMENT '1是禁用，0是启用',
  `introduce` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '个人介绍',
  `ext_json` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '特殊字段',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除状态，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_user
-- ----------------------------
INSERT INTO `auth_user` VALUES (1, '鸡翅', '经典鸡翅', '12312312@qq.com', '12321312', '123123', 1, 'http://d12321.dwdw.png', 1, '个人介绍', '留存json', NULL, NULL, NULL, NULL, 0);
INSERT INTO `auth_user` VALUES (31, 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '温柔书生', NULL, NULL, NULL, 1, 'https://web-183.oss-cn-beijing.aliyuncs.com/icon/a1fdf32d-95f9-4961-957c-0a0a6493fdf9.png', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `auth_user` VALUES (32, 'o-Pg16bKVdoSPOUQ6mk_DCKFEqvs', NULL, NULL, NULL, NULL, NULL, 'https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402121828810.webp', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `auth_user` VALUES (33, 'o-Pg16WGdUgN8axO8p7cytrHNTGU', '鸡王', 'hzm@jnxiangchen.com', '190076', NULL, 1, 'https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402121828810.webp', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `auth_user` VALUES (34, 'o-Pg16aK7PKS7gghDMEWi37WyVrA', NULL, NULL, NULL, NULL, NULL, 'https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402121828810.webp', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `auth_user` VALUES (35, 'o-Pg16ZV-gfsehuLgGZDKO0v19fQ', NULL, NULL, NULL, NULL, NULL, 'https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402121828810.webp', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `auth_user` VALUES (36, 'o-Pg16Ry53lE0T-HOXFJj1O8b-qc', NULL, NULL, NULL, NULL, NULL, 'https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402121828810.webp', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0);
INSERT INTO `auth_user` VALUES (37, 'o-Pg16RVo0OLJr8jrWhAHE04koRA', NULL, NULL, NULL, NULL, NULL, 'https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402121828810.webp', 1, NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for auth_user_role
-- ----------------------------
DROP TABLE IF EXISTS `auth_user_role`;
CREATE TABLE `auth_user_role`  (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `user_id` bigint NULL DEFAULT NULL COMMENT '用户id',
  `role_id` bigint NULL DEFAULT NULL COMMENT '角色id',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除，1是删除状态\n',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '用户 角色 关系映射表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of auth_user_role
-- ----------------------------
INSERT INTO `auth_user_role` VALUES (1, 1, 1, NULL, NULL, NULL, 0, NULL);
INSERT INTO `auth_user_role` VALUES (21, 31, 1, NULL, NULL, NULL, 0, NULL);
INSERT INTO `auth_user_role` VALUES (22, 32, 1, NULL, NULL, NULL, 0, NULL);
INSERT INTO `auth_user_role` VALUES (23, 33, 1, NULL, NULL, NULL, 0, NULL);
INSERT INTO `auth_user_role` VALUES (24, 34, 1, NULL, NULL, NULL, 0, NULL);
INSERT INTO `auth_user_role` VALUES (25, 35, 1, NULL, NULL, NULL, 0, NULL);
INSERT INTO `auth_user_role` VALUES (26, 36, 1, NULL, NULL, NULL, 0, NULL);
INSERT INTO `auth_user_role` VALUES (27, 37, 1, NULL, NULL, NULL, 0, NULL);

-- ----------------------------
-- Table structure for subject_brief
-- ----------------------------
DROP TABLE IF EXISTS `subject_brief`;
CREATE TABLE `subject_brief`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_id` int NULL DEFAULT NULL COMMENT '题目id',
  `subject_answer` text CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL COMMENT '题目答案',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 70 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '简答题表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_brief
-- ----------------------------
INSERT INTO `subject_brief` VALUES (61, 138, '<ul><li style=\"text-align: start;\">通过<span style=\"color: currentcolor;\"><code>控制反转和依赖注入</code></span>实现松耦合。</li><li style=\"text-align: start;\">支持<span style=\"color: currentcolor;\"><code>面向切面</code></span>的编程，并且把应用业务逻辑和系统服务分开。</li><li style=\"text-align: start;\">通过切面和模板<span style=\"color: currentcolor;\"><code>减少样板式代码</code></span>。</li><li style=\"text-align: start;\">声明式<span style=\"color: currentcolor;\"><code>事务的支持</code></span>。可以从单调繁冗的事务管理代码中解脱出来，通过声明式方式灵活地进行事务的管理，提高开发效率和质量。</li><li style=\"text-align: start;\">方便<span style=\"color: currentcolor;\"><code>集成各种优秀框架</code></span>。内部提供了对各种优秀框架的直接支持（如：Hessian、Quartz、MyBatis等）。</li><li style=\"text-align: start;\">方便程序的测试。Spring<span style=\"color: currentcolor;\"><code>支持Junit4</code></span>，添加注解便可以测试Spring程序。</li></ul>', NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_brief` VALUES (62, 139, '<ol><li style=\"text-align: start;\">MySQL 客户端与服务器间建立连接，客户端发送一条查询给服务器；</li><li style=\"text-align: start;\">服务器先检查查询缓存，如果命中了缓存，则立刻返回存储在缓存中的结果；否则进入下一阶段；</li><li style=\"text-align: start;\">服务器端进行 SQL 解析、预处理，生成合法的解析树；</li><li style=\"text-align: start;\">再由优化器生成对应的执行计划；</li><li style=\"text-align: start;\">MySQL 根据优化器生成的执行计划，调用相应的存储引擎的 API 来执行，并将执行结果返回给客户端。</li></ol>', NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_brief` VALUES (63, 140, '<p style=\"text-align: start;\">Java 内存模型（Java Memory Model，JMM）是一种规范，定义了 Java 程序中多线程环境下内存访问和操作的规则和语义，<span style=\"color: currentcolor;\"><code>主要是解决 CPU 缓存一致性问题和操作系统优化指令重排序的问题的</code></span>。<br>Java 内存模型主要包括以下内容：</p><ol><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>主内存</code></span>：所有线程共享的内存区域,包含了对象的字段、方法和运行时常量池等数据。</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>工作内存</code></span>：每个线程拥有自己的工作内存,用于存储主内存中的数据的副本。线程只能操作工作内存中的数据。</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>内存间交互操作</code></span>：线程通过读取和写入操作与主内存进行交互。读操作将数据从主内存复制到工作内存，写操作将修改后的数据刷新到主内存。</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>原子性</code></span>：原子性指的是一个或多个操作要么全部执行成功要么全部执行失败。</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>可见性</code></span>：一个线程对共享变量的修改,另一个线程能够立刻看到。（synchronized,volatile）</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>有序性</code></span>：保证程序的执行顺序按照一定的规则进行，不会出现随机的重排序现象。</li></ol>', NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_brief` VALUES (64, 141, '<ol><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>类加载检查</code></span>：当虚拟机遇到一条 new 指令时，首先检查是否能在常量池中定位到这个类的符号引用，并且检查这个符号引用所指向的类是否已完成了类加载过程。如果没有，那先执行类加载。</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>分配内存</code></span>：在类加载检查通过后，接下来虚拟机将为对象实例分配内存。</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>初始化</code></span>：分配到的内存空间都初始化为零值，通过这个操作保证了对象的字段可以不赋初始值就直接使用，程序能访问到这些字段的数据类型所对应的零值。</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>设置对象头</code></span>：Hotspot 虚拟机的对象头包括：存储对象自身的运行时数据（哈希码、分代年龄、锁标志等等）、类型指针和数据长度（数组对象才有），类型指针就是对象指向它的类信息的指针，虚拟机通过这个指针来确定这个对象是哪个类的实例。</li><li style=\"text-align: start;\"><span style=\"color: currentcolor;\"><code>按照Java代码进行初始化</code></span></li></ol>', NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_brief` VALUES (65, 142, '<p style=\"text-align: start;\">策略模式在很多地方用到，如 Java SE 中的容器布局管理就是一个典型的实例，Java SE 中的每个容器都存在多种布局供用户选择。</p><p style=\"text-align: start;\">在程序设计中，通常在以下几种情况中使用策略模式较多：</p><ul><li style=\"text-align: start;\">一个系统需要动态地在几种算法中选择一种时，可将每个算法封装到策略类中</li><li style=\"text-align: start;\">一个类定义了多种行为，并且这些行为在这个类的操作中以多个条件语句的形式出现，可将每个条件分支移入它们各自的策略类中以代替这些条件语句</li><li style=\"text-align: start;\">系统中各算法彼此完全独立，且要求对客户隐藏具体算法的实现细节时</li><li style=\"text-align: start;\">系统要求使用算法的客户不应该知道其操作的数据时，可使用策略模式来隐藏与算法相关的数据结构</li><li style=\"text-align: start;\">多个类只区别在表现行为不同，可以使用策略模式，在运行时动态选择具体要执行的行为</li></ul>', NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_brief` VALUES (66, 143, '<ul><li style=\"text-align: start;\">HashMap 在 JDK 1.7 时，是通过数组 + 链表实现的，而在 JDK 1.8 时，HashMap 是通过数组 + 链表或红黑树实现的。在 JDK 1.8 之后，如果链表的数量大于阈值（默认为 8），并且数组长度大于 64 时，为了查询效率会将链表升级为红黑树，如果 数组的长度小于 64，那么 HashMap 会优先选择对数组进行扩容 ，而不是把链表转换成红黑树, 当红黑树的节点小于等于 6 时，为了节省内存空间会将红黑树退化为链表。</li><li style=\"text-align: start;\">将红黑树退化为链表是因为节点数量较少时，红黑树对性能的提升并不明显，反而占用了更多的内存空间</li></ul>', NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_brief` VALUES (67, 145, 'Mysql是个数据库', NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_brief` VALUES (68, 146, '不可重复读主要涉及到修改数据，而幻读则主要涉及到插入或删除数据。解决方案不同：不可重复读的解决方案通常是使用行锁或者表锁来解决，而幻读的解决方案通常是使用间隙锁来解决。', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-19 12:04:00', NULL, NULL, 0);
INSERT INTO `subject_brief` VALUES (69, 147, '第一范式：每个列都不可以再拆分。第二范式：在第一范式的基础上，要求非主键列完全依赖于主键，而不能是依赖于主键的一部分。第三范式：在第二范式的基础上，要求非主键列只依赖于主键，不依赖于其他非主键。', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-19 23:06:31', NULL, NULL, 0);

-- ----------------------------
-- Table structure for subject_category
-- ----------------------------
DROP TABLE IF EXISTS `subject_category`;
CREATE TABLE `subject_category`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `category_name` varchar(16) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `category_type` tinyint NULL DEFAULT NULL COMMENT '分类类型',
  `image_url` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '图标连接',
  `parent_id` bigint NULL DEFAULT NULL COMMENT '父级id（根据大类查询所有子级分类就是利用这个父级ID查询的）',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` tinyint(1) NULL DEFAULT 0 COMMENT '是否删除 0: 未删除 1: 已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 46 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '题目分类表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_category
-- ----------------------------
INSERT INTO `subject_category` VALUES (1, '后端', 1, 'http://image/123', 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (23, '前端', 1, 'http://image/123', 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (24, 'Spring', 2, 'http://image/123', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (25, 'MySQL', 2, 'http://image/123', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (27, 'JUC并发编程', 2, 'http://image/123', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (28, 'JVM虚拟机', 2, 'http://image/123', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (29, 'HTML', 2, 'http://image/123', 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (30, 'JS', 2, 'http://image/123', 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (31, 'CSS', 2, 'http://image/123', 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (32, 'Vue', 2, 'http://image/123', 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (33, 'React', 2, 'http://image/123', 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (34, '运维', 1, 'http://image/123', 0, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (35, 'Linux', 2, 'http://image/123', 34, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (36, 'Dokcer', 2, 'http://image/123', 34, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (37, 'Jenkins', 2, 'http://image/123', 34, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (38, 'Kubernetes', 2, 'http://image/123', 34, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (40, '设计模式', 2, 'http://image/123', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (41, '数据结构', 2, 'http://image/123', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (43, 'SpringBoot', 3, 'http://image/123', 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (44, 'TypeScript', 2, 'http://image/123', 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_category` VALUES (45, 'Nginx', 2, 'http://image/123', 34, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for subject_info
-- ----------------------------
DROP TABLE IF EXISTS `subject_info`;
CREATE TABLE `subject_info`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_name` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '题目名称',
  `subject_difficult` tinyint NULL DEFAULT NULL COMMENT '题目难度',
  `settle_name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '出题人名',
  `subject_type` tinyint NULL DEFAULT NULL COMMENT '题目类型 1单选 2多选 3判断 4简答',
  `subject_score` tinyint NULL DEFAULT NULL COMMENT '题目分数',
  `subject_parse` varchar(512) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '题目解析',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '默认未删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 148 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '题目详情表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_info
-- ----------------------------
INSERT INTO `subject_info` VALUES (138, 'Spring的优点是什么？', 1, NULL, 4, 1, '解析什么', NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_info` VALUES (139, '一条 SQL 查询语句是如何执行的？', 1, NULL, 4, 1, '解析什么', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-20 05:38:21', NULL, NULL, 0);
INSERT INTO `subject_info` VALUES (140, '什么是Java 内存模型?', 1, NULL, 4, 1, '解析什么', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-20 05:38:24', NULL, NULL, 0);
INSERT INTO `subject_info` VALUES (141, '对象的创建过程（new 一个对象在堆中的历程）', 1, NULL, 4, 1, '解析什么', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-20 05:38:19', NULL, NULL, 0);
INSERT INTO `subject_info` VALUES (142, '策略模式的应用场景', 1, NULL, 4, 1, '解析什么', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-20 05:38:17', NULL, NULL, 0);
INSERT INTO `subject_info` VALUES (143, '说一下HashMap的实现原理？', 1, NULL, 4, 1, '解析什么', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-20 05:38:16', NULL, NULL, 0);
INSERT INTO `subject_info` VALUES (145, 'Mysql是个什么东西？', 1, NULL, 4, 2, '题目解析', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-20 05:38:14', NULL, NULL, 0);
INSERT INTO `subject_info` VALUES (146, '不可重复读和幻读有什么区别?', 1, NULL, 4, 2, '题目解析', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-19 12:03:44', NULL, NULL, 0);
INSERT INTO `subject_info` VALUES (147, ' 数据库三大范式是什么?', 1, NULL, 4, 2, '题目解析', 'o-Pg16Vjvee8j--X3IzYCaTKpGmY', '2024-02-19 23:06:31', NULL, NULL, 0);

-- ----------------------------
-- Table structure for subject_judge
-- ----------------------------
DROP TABLE IF EXISTS `subject_judge`;
CREATE TABLE `subject_judge`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_id` bigint NULL DEFAULT NULL COMMENT '题目id',
  `is_correct` tinyint NULL DEFAULT NULL COMMENT '是否正确（1正确,0错误）',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '判断题表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_judge
-- ----------------------------

-- ----------------------------
-- Table structure for subject_label
-- ----------------------------
DROP TABLE IF EXISTS `subject_label`;
CREATE TABLE `subject_label`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `label_name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '标签分类',
  `sort_num` int NULL DEFAULT NULL COMMENT '排序',
  `category_id` bigint NULL DEFAULT NULL COMMENT '这个分类id对应分类表中分类类型为1的分类',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 63 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '题目标签表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_label
-- ----------------------------
INSERT INTO `subject_label` VALUES (48, 'SpringBoot', 1, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (49, 'SpringMVC', 2, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (50, 'Spring', 3, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (51, 'MySQL', 4, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (52, '多线程', 5, 1, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (53, 'Docker', 6, 34, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (54, 'Linux', 7, 34, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (55, 'Jenkins', 8, 34, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (56, 'Kubernetes', 9, 34, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (57, 'HTML', 10, 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (58, 'JS', 11, 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (59, 'CSS', 12, 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (60, 'Vue', 13, 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (61, 'React', 14, 23, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_label` VALUES (62, 'JVM', 15, 1, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for subject_mapping
-- ----------------------------
DROP TABLE IF EXISTS `subject_mapping`;
CREATE TABLE `subject_mapping`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_id` bigint NULL DEFAULT NULL COMMENT '题目id',
  `category_id` bigint NULL DEFAULT NULL COMMENT '分类id',
  `label_id` bigint NULL DEFAULT NULL COMMENT '标签id',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除状态，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 333 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '题目 分类 标签 关系映射表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_mapping
-- ----------------------------
INSERT INTO `subject_mapping` VALUES (322, 138, 24, 50, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (323, 139, 25, 51, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (324, 140, 27, 52, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (325, 141, 28, 62, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (326, 142, 40, 48, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (327, 142, 40, 50, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (328, 143, 41, 50, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (330, 145, 25, 51, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (331, 146, 25, 51, NULL, NULL, NULL, NULL, 0);
INSERT INTO `subject_mapping` VALUES (332, 147, 25, 51, NULL, NULL, NULL, NULL, 0);

-- ----------------------------
-- Table structure for subject_multiple
-- ----------------------------
DROP TABLE IF EXISTS `subject_multiple`;
CREATE TABLE `subject_multiple`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_id` bigint NULL DEFAULT NULL COMMENT '题目id',
  `option_type` bigint NULL DEFAULT NULL COMMENT '选项类型（a,b,c,d）',
  `option_content` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '选项内容',
  `is_correct` tinyint NULL DEFAULT NULL COMMENT '是否正确',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '更新人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '多选题信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_multiple
-- ----------------------------

-- ----------------------------
-- Table structure for subject_radio
-- ----------------------------
DROP TABLE IF EXISTS `subject_radio`;
CREATE TABLE `subject_radio`  (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `subject_id` bigint NULL DEFAULT NULL COMMENT '题目id',
  `option_type` tinyint NULL DEFAULT NULL COMMENT 'a,b,c,d',
  `option_content` varchar(128) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '选项内容',
  `is_correct` tinyint NULL DEFAULT NULL COMMENT '是否正确',
  `created_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `created_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  `is_deleted` int NULL DEFAULT 0 COMMENT '0是未删除状态，1是删除状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 85 CHARACTER SET = utf8mb3 COLLATE = utf8mb3_general_ci COMMENT = '单选题信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of subject_radio
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
