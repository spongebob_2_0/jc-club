## jc-club
这是一款前后端分离开发的微服务项目

从前端页面打开加载的四个接口展开对数据库表的介绍：

### 1. 题目分类表（subject_category）

![image-20240211091514081](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402110915117.png)

queryPrimaryCategory这个接口是根据分类类型查询分类，`分类类型 categoryType` 字段 是 刷题模块对应的网页加载时前端传递给后端的值，就是通过这个值查询出数据库分类表中包含这个类型的所有分类作为顶层分类。

![image-20240211131410352](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111314391.png)

![image-20240211091524121](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402110915147.png)

queryCategoryAndLabel这个接口是一次性查询出所以子分类和子分部对应的标签的，这个子分类怎么理解呢，简单点说就是在`题目分类表（subject_category）`下面，每个分类里面都有一个父级id，这个父级id对应的就是分类表里面的id，也就是说分类表里面的分类也是有层级关系的，比如说在我的分类表里面有一下分类，后端（它是顶层分类，父级id为0，id为1），还有一些java，python，go，c语言（它们都属于后端模块，所以我将它的父级id设置为1，与后端的id进行关联），这样解释应该就能明白了吧！



### 2. 标签表（subject_label）

接下来就是介绍标签表，我们可以想象一下，每一个小分类下面肯定会包含很多东西吧，比如java来说，java 这个语言发展到现在，像Spring，SpringMVC，SpringBoot这些算是java的象征吧，我们可以把这些当做java的标签，那么具体在数据库是如何映射的呢，在数据库`subject_label标签表`中，每条数据中都会有一个`分类id（category_id）`字段对应的就是分类表（subject_category）中 的`id字段`，这样就把每个分类下对应的标签关系建立起来了；

![image-20240211091312225](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402110913264.png)

注意：在顶层分类下会包含多个子分类，每个子分类下又包含多个标签，同时这些标签可能会对应多个子分类，这些子分类也可能对应多个其他分类；

![image-20240211131839163](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111318204.png)

> 注意这里的标签id只跟一级分类的id进行关联（也就是分类类型为1的），之前理解错了，待修正 TODO



### 3. 题目 分类 标签 关系映射表（subject_mapping）

至此，我们分类和子分类，子分类和标签的关系就建立起来了，那么我们要知道，既然这是个刷题模块，那么对应的每个标签下面是不是还应该有题目呢，那么此时就需要一张映射表，可以把题目和标签映射起来，同时还要把子分类映射起来，这样在这种映射表下面的每一条数据就会形成这样一种关系，

子分类 id  -> 标签id -> 题目id

举个例子来说， java下包含的标签SpringBoot，SpringBoot又有对应的面试题（SpringBoot的自动装配原理是什么?），这就是`题目 分类 标签 关系映射表（subject_mapping）`的作用，

![image-20240211090913611](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402110909685.png)

话不多说开始做，先建立一张`题目分类 标签 关系映射表（subject_mapping）`，首先映射表中的`题目id（subject_id）`字段 对应的题目详情表中`id`字段，映射表中的`分类id（category_id）`字段 对应的分类表（subject_category）中的`id`字段，映射表中的`标签id（label_id）`字段对于的是标签表中的`id`字段，以上就是关于题目映射表的全部内容；

![image-20240211134402230](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111344302.png)

> 后面查询题目列表就是通过这张表关联题目详情表快速过滤完成查询的



### 4. 题目详情表（subject_info）

![image-20240211135156197](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111351240.png)

![image-20240211135705656](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111357705.png)



### 5. 新增题目

#### 5.1 单选题目表（subject_radio）

![image-20240211082010312](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402110820348.png)

那接下来我要说的是就是上面遗留的问题，怎么确定题目关系映射表里面存的`题目id（subject_id）`是哪个题目表里面的题目，这个问题也很好解决，在我增加一条题目数据的时候，

![image-20240211124112520](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111241569.png)

![image-20240211123958940](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111239984.png)

新增单选题目时，我们前端会将以下题目信息发送给后端，后端先将这样一条数据直接存入到数据库的题目表中（subject_info），同时返回这条数据在题目详情表中的`id`放入到`subjectInfo对象`中

![image-20240211114650306](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111146332.png)

![image-20240211114848872](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111148914.png)



然后通过工厂+策略模式将题目在插入到对应类型的题目表中，比如我这条新增的数据是单选题，那我肯定要插入到单选表中

![image-20240211115009775](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111150800.png)

![image-20240211115312767](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111153811.png)

![image-20240211115813500](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111158525.png)

作完以上操作以后，现在题目表和单选表里面已经有数据了，`题目表`里面主要是记录题目的类型，难度，解析，分数 这些数据，而 `单选表` 主要记录该种类型（这里是单选表）的一些选项内容，正确答案是哪个。



#### 5.2 多选题目表（subject_multiple）

![image-20240211124515273](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111245317.png)

![image-20240211124649545](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111246581.png)

![image-20240211125803406](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111258444.png)



#### 5.3 判断题目表（subject_judge）

![image-20240211130138570](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111301618.png)

![image-20240211130221873](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111302905.png)

![image-20240211130247677](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111302712.png)



#### 5.4 简答题目表（subject_brief）

![image-20240211130741680](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111307717.png)

![image-20240211130830501](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111308524.png)

![image-20240211130854859](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111308892.png)



### 6. 查询题目列表 （/getSubjectPage）

为了以后刚方便的查询出具体哪个分类，哪个标签下对应的这些题目，前面我设置的`题目 分类 标签 映射 关系表（subject_mapping）`就起作用了，我们直接把 这些 题目id ，该题目对应的分类和标签，全部通过foreach循环遍历插入到这张映射表中，以后我们如果需要查询题目列表 通过这张表就能很快的帮我们过滤出来。

![image-20240211114102645](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111141681.png)



接下来说一下前端是如何查询题目列表的，首次登录加载到题目模块的时候会按以下方式进行一次查询

![image-20240211121634033](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111216081.png)

后面我们点击哪个标签，就会查询出哪个标签下的所有题目！！！

具体是如何查询的呢？下面是具体实现代码和sql的详细解释，这里简单做一下说明，

第一次通过多表查询出所有的题目数量作为后面分页的总页数

![image-20240211123510849](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111235896.png)

![image-20240211122928959](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111229005.png)

![Snipaste_2024-02-11_12-38-49](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111238447.png)



### 7. 查询题目详情（/querySubjectInfo）

![image-20240211144140959](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111441013.png)

![image-20240211144617241](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402111446300.png)

### 6.查询分类及标签优化，提示性能80%

最初我们在查询标签的时候是通过一个forEach循环去同步查询标签的，由于每个分类都都对应着多个不同的标签，随着分类和标签的不断增加，查询性能就会越差。基于此，我想到了利用线程池去优化获取标签的这个动作，将每个分类查询标签的行为作为一个任务添加到线程池，这样所有的分类查询标签的行为就会并发执行，大大提升了查询效率；

- 具体代码实现方式如下：

**第一种写法**使用了`FutureTask`直接与线程池结合的方式。这种方式更接近底层的线程处理逻辑，给予了开发者更多的控制权，但代码相对复杂，阅读和维护成本较高。`FutureTask`需要显式地添加到列表中，并且手动提交到线程池执行。获取结果时，需要遍历`FutureTask`列表，通过`.get()`方法阻塞等待每个任务的完成，并手动处理结果。

![image-20240212073245262](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402120732343.png)



**第二种写法**通过`CompletableFuture.supplyAsync()`方法简化了并发处理。这种方式隐藏了部分底层线程池的直接操作，使得代码更加简洁、易读。`CompletableFuture`提供了丰富的API来处理异步计算结果，使得异步编程更加方便。结果的合并通过流式操作实现，代码更加简洁易懂。

![image-20240212131910074](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402121319173.png)



**性能比较：**

- 在性能方面，两种方法在多线程处理时的性能相近，因为它们本质上都是利用线程池并发执行任务。性能差异主要来源于任务的本身执行时间以及线程池的配置。
- `CompletableFuture`提供了更加灵活的错误处理和结果组合方式，可以更方便地实现复杂的并发逻辑，比如组合多个异步结果、处理错误等。

**结论：**

- 如果关注代码的简洁性和易用性，推荐使用`CompletableFuture`的方式。
- 如果需要更细粒度的控制线程行为，或者是对底层线程处理有特殊需求，可以选择`FutureTask`的方式。

### 8. 用户表（auth_user）

![image-20240213014741555](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130147646.png)

![image-20240213014902217](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130149249.png)



### 9. 角色表（auth_role）

![image-20240213015006888](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130150933.png)

![image-20240213021642946](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130216981.png)



### 10. 用户角色关系映射表（auth_user_role）

![image-20240213015153332](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130151378.png)

![image-20240213015834128](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130158183.png)

### 11. 权限表（auth_permission）

![image-20240213020336376](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130203423.png)

![image-20240213020441132](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130204158.png)

### 12. 角色权限映射表（auth_role_permission）

![image-20240213020538945](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130205990.png)

![image-20240213021210291](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130212332.png)

### 13. 注册接口的实现

- 先校验前端传来的参数是否符合要求
- 检查用户注册的信息是否存在数据库
- 如果用户是通过公众号登录的话则不需要对密码进行加盐加密处理
- 设置用户的基本参数信息，根据前端传来的信息做一些处理保存到数据库即可
- `设置用户的角色信息`（这里可以设置用户是管理员还是普通用户），确定设置哪个角色就去数据库角色表中查出该角色的详细信息，然后将用户 对应的 角色 插入到 用户 角色映射表中，后面可以通过这张表快速定位该用户的角色信息（也就是确定该用户是管理员还是普通用户）。
- `将用户的角色信息缓存到Redis中` （使用 auth.role + 用户名 作为 key，该用户对应的角色信息作为value 缓存进redis ）
- 然后还要`将该用户对应的角色的所有权限也缓存进reids`，根据提供的角色id去角色 权限映射表 中快速定位到该角色的所有权限， 缓存所有用户权限信息到Redis （auth.permission + 用户名 作为key，该用户的权限列表作为value 存入redis里面）。
- 至此整个注册流程到此结束

![image-20240213023905302](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130239419.png)



### 14. 登录接口的实现

- 用户扫描二维码关注公众号回复验证码，会得到一个随机的三位数验证码，同时服务端会将其缓存进redis

（loginCode + 验证码 构建key，value就是用户自己公众号的唯一id）

- 用户在前端输入验证码，如果没在缓存中找到，也就是根据key没有获取到openId，说明验证码错误，直接返回
- 如果找到了，说明验证码正确，利用在找到的openId作为用户名去数据库查询是否存在，如何不存在，说明该用户还未注册账户，先进行注册，如果存在，查到该用户对应角色的所有权限缓存进redis（虽然注册的时候已经存过一次了，并且没有设置过期时间，但是未了防止redis存储的被误删，所以每次登录的使用都要把用户对应的权限再存一遍）
- 最后通过sa-token框架进行登录，返回登录成功的token信息，后续用户访问的时候在请求头里面携带这个token就不需要再次进行登录操作了，sa-token框架会自动放行。
- 这个token就是用户登录身份的凭证，在sa-token框架中使用`StpUtil.checkLogin()`来检查用户是否登录就是靠这个token 去认证的

![image-20240213025607322](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130256401.png)



### 13. 将所有微服务注册到nacos注册中心

通过对每个服务 进行一下配置，将所有服务注册到nacos注册中心

![在这里插入图片描述](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130432071.png)

![image-20240213043214391](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130432490.png)

我们可以将一些常用的配置写入nacos的配置文件中，这样可以在不修改代码的情况下去切换一些常用的服务配置

![image-20240213043503953](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130435009.png)



### 14. 鉴权架构设计

`jc-club-auth`：这个服务承载了我们所有的基础数据源。他不管鉴权，只管数据相关的持久化操作以及业务操作，提供出各种各样的权限相关的接口。

`nacos`：将 auth 服务以及 subject 服务都注册到上面。内部进行调用，不对外暴露。通过 nacos 实现我们的服务发现。

`gateway（网关）`：网关层会对外提供服务，内部实现路由，鉴权。整体我们采取 token 的方式来与前端进行交互。由网关来决定当前用户是否可以操作到后面的业务逻辑。

![image-20240213043721552](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130437590.png)

![image-20230917224934628](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130438775.jpg)

`权限控制`：网关作为微服务入口，需要校验用户是是否有请求资格，如果没有则进行拦截。

`鉴权框架`这里我们选择的是Sa-Token ，一个轻量级 Java 权限认证框架，让鉴权变得简单、优雅！

![image-20240213044800567](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130448636.png)

当Gateway网关根据路径匹配到对应的服务之后，需要将请求转发给每个微服务，但是在转发之前，

我们需要对每个请求进行权限校验，需要校验用户是是否有请求资格，如果没有则进行拦截。

![image-20240213045612614](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130456680.png)

![image-20240213054805952](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130548989.png)

![image-20240213055212797](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130552858.png)

![image-20240213055814414](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130558492.png)

#### gateway 集成 redis 及 refactor（重构） 鉴权，实现分布式会话鉴权

> 上面这种利用官网提供的自定义鉴权扩展接口，去实现我们我自己获取权限信息的方法，再将我们获取到信息交给Sa-Token框架去校验，这就是重构鉴权的逻辑。
>
> 然后在我们获取信息 实现方法中，因为我们在注册，登录的时候都已经将这些信息使用自定义的键值对存入到redis里面了，然后直接在redis里面获取就可以了；
>
> 微服务架构下，我们的应用会被拆分成多个服务单元运行在不同的服务器或容器上，通过Redis来共享会话信息，可以确保用户的认证状态跨服务保持一致。

**知识点补充：**在传统的单体应用中，用户的会话信息（如登录状态、权限信息等）通常存储在服务器的内存中或者通过会话Cookie在客户端和服务器之间传递。这种方式在应用只运行在单个服务器上时是可行的，因为所有用户请求都是由同一个应用实例处理，会话信息可以轻松地保持一致。然而，当应用需要扩展或者拆分成多个服务运行在不同的服务器或者容器上时（即分布式系统），传统的会话管理方式就会遇到挑战。

#### 分布式会话鉴权的概念

分布式会话鉴权是指在分布式系统中，确保用户会话的一致性和安全性的一种机制。它允许用户的会话状态跨多个服务和应用共享，这样用户在系统中的任何一个部分登录后，就无需在其他部分重新登录。

#### 分布式系统中共享会话信息的含义

在分布式系统中共享会话信息，意味着用户的登录状态和权限信息等会话数据，不再局限于存储在单一服务器的内存中，而是存储在所有服务都能访问到的中心位置，如分布式缓存（Redis、Memcached等）或数据库中。这样，不论用户的请求是由系统中的哪个服务处理，该服务都能访问到相同的会话信息，实现用户的无缝访问和权限验证。

#### 分布式会话鉴权的实现机制

**会话标识传递**：当用户登录成功后，系统会生成一个唯一的`会话标识`（如Token、Session ID等），并返回给客户端（通常是浏览器）。客户端后续的每次请求都会携带这个会话标识。

**中心化存储会话数据**：系统会将与会话标识相关的会话数据（如用户身份信息、权限列表等）存储在中心化的存储系统中（如Redis）。这样，系统中的任何服务都可以通过会话标识查询到相应的会话数据。

**会话验证与更新**：服务接收到客户端请求后，会首先提取`会话标识`，并查询`中心存储系统`中的会话数据来验证用户的登录状态和权限。



### 15. 网关层的全局异常处理

网关层的全局异常处理是为了确保即便在请求转发或鉴权等网关层面发生错误时，也能够向客户端返回统一格式和明确信息的错误响应，从而提升整体的系统健壮性和用户体验。

注意网关层的全局异常处理与每个微服务内部的全局异常处理遵循相同的原理：

- **统一的错误格式**：无论是在网关层还是在微服务层面，全局异常处理都旨在确保所有的错误响应都遵循一个统一的格式。这样，前端应用就可以以一致的方式处理错误，无论这些错误发生在哪个层次。
- **改善用户体验**：通过提供明确且一致的错误信息，有助于前端开发者构建更好的用户界面和体验，例如显示友好的错误消息或者根据不同的错误类型做出相应的界面调整。
- **简化错误处理**：当所有的服务（包括网关和各个微服务）都遵循相同的错误处理约定时，前端的错误处理逻辑可以被大大简化，因为它不需要为来自不同源的错误设计不同的处理策略。

![image-20240213053931078](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130539171.png)

### 16. 微信公众号开发

![image-20240213061322576](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130613606.png)

- 整体流程：用户扫公众号码。然后发一条消息：验证码。我们通过 api 回复一个随机的码。存入 redis

- redis 的主要结构，就是 openId 加验证码

- 用户在验证码框输入之后，点击登录，进入我们的注册模块，同时关联角色和权限。就实现了网关的统一鉴权。

- 用户就可以进行操作，用户可以根据个人的 openId 来维护个人信息。

- 用户登录成功之后，返回 token，前端的所有请求都带着 token 就可以访问拉。

公众号开发文档：https://developers.weixin.qq.com/doc/offiaccount/Getting_Started/Overview.html

> 首先我们需要一个域名映射到我们后端微信服务的端口上面来，如果基于本地开发，我们可以先用内网穿透进行测试开发，后期上线了再使用自己服务器的域名去开发。

内网穿透的软件有很多，这里推荐：https://natapp.cn/  （官方地址）

https://natapp.cn/article/natapp_newbie（NATAPP1分钟快速新手图文教程）

![image-20240213071754923](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130717983.png)

> 服务上线时需要通过Nginx做反向代理来完成公众号的配置

![image-20240213071947403](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130719436.png)

![image-20240213072458210](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130724278.png)

上面这个是基于自己的微信公众号开发的，如果我们没有自己的微信公众号，也可以使用微信公众号的测试号去开发

测试号地址：https://mp.weixin.qq.com/debug/cgi-bin/sandboxinfo?action=showinfo&t=sandbox/index

![image-20240213073226493](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130732561.png)

#### 代码实现：采用策略模式对公众号消息事件完成解耦

回调消息接入指南：https://developers.weixin.qq.com/doc/offiaccount/Basic_Information/Access_Overview.html

接收公众号消息体文档：https://developers.weixin.qq.com/doc/offiaccount/Message_Management/Receiving_standard_messages.html

**为什么这里使用策略模式呢?**

因为对于微信公众号来说，会有很多种类型的行为事件，这里以我们现在网站登录、注册的场景来举例，在用户扫了我们网页的二维码以后，用户可能会关注我们的公众号，也有可能给我们公众号发送消息，如果是用户关注事件我们需要做出对应的处理，比如可以给用户回复一些公众号简介和个人简介之类的；

如果接收到的是用户发送给我们的消息，我们需要根据具体的消息类型来决定是否回复，回复什么。

比如在登录的时候需要用户发送验证码三个字给我们的公众号，我们的公众号在接收到验证码的事件的时候，将生成的验证码回复给用户的微信号，如果用户发送的消息不是我们公众号想要的消息，我们可以选择不做任何处理。

基于此，我们就可以使用`策略模式`来进行一个解耦，把用户的行为事件进行一个抽取，对于用户不同的操作进行不同的实现；

![image-20240213084814782](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130848877.png)

![image-20240213085248488](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130852547.png)

![image-20240213085401267](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130854313.png)

![image-20240213085919734](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402130859799.png)

#### 微信公众号关注事件处理器实现

```java
/**
 * 微信公众号关注事件处理器实现
 * 用于处理用户关注公众号后的消息回复等逻辑
 *
 * @author: WuYimin
 * @date: 2024-02-10
 */
@Component
@Slf4j
public class SubscribeMsgHandlerImpl implements WxChatMsgHandler {

	/**
	 * 获取处理的消息类型
	 * @return 消息类型枚举，这里是关注事件
	 */
	@Override
	public WxChatMsgTypeEnum getMsgType() {
		return WxChatMsgTypeEnum.SUBSCRIBE;
	}

	/**
	 * 处理关注事件的业务逻辑
	 * @param messageMap 消息内容映射，包含了微信发送过来的消息数据
	 * @return 响应给用户的消息内容，这里是一段感谢关注的文本消息
	 */
	@Override
	public String dealMsg(Map<String, String> messageMap) {
		log.info("触发用户关注事件！");
		// 用户的微信号
		String fromUserName = messageMap.get("FromUserName");
		// 公众号的微信号
		String toUserName = messageMap.get("ToUserName");
		// 关注时回复给用户的感谢消息内容
		String subscribeContent = "感谢您的关注，我是温柔书生！欢迎来加入我的公众号一起学习编程！";
		// 构造返回的消息xml格式字符串
		String content = "<xml>\n" +
				"  <ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>\n" +
				"  <FromUserName><![CDATA[" + toUserName + "]]></FromUserName>\n" +
				"  <CreateTime>12345678</CreateTime>\n" +
				"  <MsgType><![CDATA[text]]></MsgType>\n" +
				"  <Content><![CDATA[" + subscribeContent + "]]></Content>\n" +
				"</xml>";
		return content;
	}
}
```

#### 微信公众号接收到文本消息后的处理器实现

```java
/**
 * 微信公众号接收到文本消息后的处理器实现
 * 用于处理用户发送给公众号的文本消息，特别是处理包含特定关键词的消息，如"验证码"，
 * 并生成验证码发送给用户。
 *
 * @author: WuYimin
 * @date: 2024-02-10
 */
@Component
@Slf4j
public class ReceiveTextMsgHandlerImpl implements WxChatMsgHandler {

	@Resource
	private RedisUtil redisUtil;

	// 定义接收到的文本消息中需要响应的关键词
	private static final String KEY_WORD = "验证码";

	// 验证码在Redis中的前缀
	private static final String LOGIN_PREFIX = "loginCode";

	/**
	 * 指定处理的消息类型
	 * @return 文本消息类型
	 */
	@Override
	public WxChatMsgTypeEnum getMsgType() {
		return WxChatMsgTypeEnum.TEXT_MSG;
	}

	/**
	 * 处理文本消息的具体逻辑
	 * 当消息内容为"验证码"时，生成一个随机验证码并通过Redis缓存，然后回复用户验证码信息
	 *
	 * @param messageMap 包含微信消息内容的Map
	 * @return 构造的响应消息的XML字符串
	 */
	@Override
	public String dealMsg(Map<String, String> messageMap) {
		log.info("接收到文本消息事件");
		// 获取消息内容
		String content = messageMap.get("Content");
		// 如果消息不是请求验证码，则不处理
		if (!KEY_WORD.equals(content)) {
			return "";
		}
		// 用户的微信号
		String fromUserName = messageMap.get("FromUserName");
		// 公众号的微信号
		String toUserName = messageMap.get("ToUserName");

		// 生成随机验证码
		Random random = new Random();
		int num = random.nextInt(1000);
		// 构造在Redis中存储验证码的键
		String numKey = redisUtil.buildKey(LOGIN_PREFIX, String.valueOf(num));
		// 将验证码存储到Redis中，设置5分钟过期
		redisUtil.setNx(numKey, fromUserName, 5L, TimeUnit.MINUTES);
		// 构造验证码信息内容
		String numContent = "您当前的验证码是：" + num + "！ 5分钟内有效";
		// 构造回复给用户的消息XML格式字符串
		String replyContent = "<xml>\n" +
				"  <ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>\n" +
				"  <FromUserName><![CDATA[" + toUserName + "]]></FromUserName>\n" +
				"  <CreateTime>12345678</CreateTime>\n" +
				"  <MsgType><![CDATA[text]]></MsgType>\n" +
				"  <Content><![CDATA[" + numContent + "]]></Content>\n" +
				"</xml>";

		return replyContent;
	}
}
```

### 17 OSS接入

目前对接的 minio和阿里云oss，要考虑，如果作为公共的 oss 服务，如何切换到其他的 oss。作为基础的 oss 服务，切换等等动作，不应该要求业务方进行改造，以及对切换有感知。

![image-20240213113254349](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402131132409.png)

![image-20240213103855679](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402131038761.png)

`适配器模式`（Adapter Pattern）是一种结构型设计模式，它允许接口不兼容的对象能够相互合作。这种模式主要解决的是兼容性问题，让原本因接口不匹配而不能一起工作的类可以一起工作。

比如我一个oss服务的Service接口定义了多种功能，但不是所有的OSS实现都支持这些功能。这种情况下，适配器模式就非常有用了。我们可以为每个具体的OSS实现（如阿里云OSS、腾讯云COS等）创建一个适配器，这些适配器实现了你的Service接口，并在内部将调用转换为对应OSS实现的调用。这样，即使这些OSS实现的API不直接匹配你的Service接口，你的业务代码也可以通过适配器与它们无缝合作，无需关心底层的差异。

适配器模式的`主要优点`包括：

- **增强了类的透明性和复用**：你的业务代码只需要与Service接口合作，无需直接与具体的OSS实现打交道，这提高了代码的可复用性和透明性。
- **灵活性和扩展性**：当引入新的OSS实现时，只需添加相应的适配器即可，无需修改现有业务代码，这增加了系统的灵活性和扩展性。
- **解耦**：适配器将业务代码与具体的OSS实现解耦，使得业务代码、适配器和OSS实现之间的依赖关系更加清晰，易于管理和维护。

**代码实现思路如下**：

#### 1. 定义统一接口（StorageAdapter）

定义了一个`StorageAdapter`接口，这个接口声明了一系列与文件存储操作相关的方法，如`createBucket`、`uploadFile`、`getAllBucket`等。这个接口充当了适配器模式中的目标接口，即希望上层应用使用的接口。

#### 2. 实现适配器

- **MinIO适配器（`MinioStorageAdapter`）**：这个类实现了`StorageAdapter`接口，内部使用`MinioUtil`工具类（封装了MinIO API调用的工具类）来实现接口中声明的方法。这个适配器使得上层应用可以通过统一的`StorageAdapter`接口与MinIO进行交互。
- **阿里云OSS适配器（`AliyunStorageAdapter`）**：同样实现了`StorageAdapter`接口，但内部使用阿里云OSS的客户端`OSS`来实现具体的操作。这个适配器让上层应用可以不修改任何代码就切换到阿里云OSS作为存储服务。

#### 3. 服务层使用适配器（FileService）

在服务层，你通过依赖注入（DI）的方式注入了`StorageAdapter`，并通过这个适配器接口来实现文件上传、下载、获取URL等操作。这样，`FileService`就可以独立于具体的存储服务实现，实现真正的解耦。

#### 4. 优势和好处

- **解耦**：上层业务逻辑（如`FileService`）不直接依赖于具体的存储服务实现（如MinIO或阿里云OSS），只依赖于`StorageAdapter`接口，增加了代码的灵活性和可维护性。
- **扩展性**：如果将来需要接入更多的存储服务，只需实现额外的适配器即可，无需修改上层业务逻辑。
- **统一接口**：通过适配器模式提供了一个统一的操作接口，上层应用不需要关心底层存储的具体差异。

### 18. 自定义线程工厂

我们先来看一下源码对于默认的线程池工厂的实现

![image-20240213222006428](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402132220505.png)

![image-20240213222052363](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402132220412.png)

![image-20240213222616992](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402132226062.png)

基于源码的实现以后，我们是不是完全可以自己`自定义一个线程池工厂`，继承源码的工厂类，重写源码默认线程工程方法的实现，我们可以传一个参数进去，`自定义我们自己的线程池前缀`。

```java
/**
 * 自定义名称的线程工厂
 * 用于创建具有特定名称前缀的线程，便于识别和管理。
 *
 * 这个类的主要目的是提供一种创建线程时自动赋予有意义名称的方式，从而在多线程环境中简化线程管理和问题诊断。
 * 通过自定义线程名称，开发者可以更容易地区分和识别不同线程的作用，尤其是在查看日志文件或使用调试工具时
 *
 * @author: ChickenWing
 * @date: 2023/11/26
 */
public class CustomNameThreadFactory implements ThreadFactory {

    private static final AtomicInteger poolNumber = new AtomicInteger(1); // 线程池编号，用于区分不同的线程池
    private final ThreadGroup group; // 线程组，用于管理创建的线程
    private final AtomicInteger threadNumber = new AtomicInteger(1); // 线程编号，用于给线程命名时区分不同线程
    private final String namePrefix; // 线程名称前缀，用于标识线程属于哪个线程池和它的序号

    /**
     * 构造函数，创建一个具有指定名称前缀的线程工厂。
     * 如果传入的名称为空，则默认使用"pool"作为名称前缀。
     *
     * @param name 线程名称前缀
     */
    CustomNameThreadFactory(String name) {
        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup() :
                Thread.currentThread().getThreadGroup(); // 获取安全管理器指定的线程组，如果没有则使用当前线程的线程组
        if (StringUtils.isBlank(name)) {
            name = "pool"; // 如果未指定名称，则使用默认名称"pool"
        }
        namePrefix = name + "-" +
                poolNumber.getAndIncrement() +
                "-thread-"; // 构造线程名称前缀，包含自定义名称、线程池编号和"thread-"
    }

    /**
     * 创建新线程的方法。
     * 使用指定的Runnable对象和构造时定义的名称前缀创建新线程。
     *
     * @param r 要在新线程中执行的Runnable任务
     * @return 创建的新线程
     */
    @Override
    public Thread newThread(Runnable r) {
        Thread t = new Thread(group, r,
                namePrefix + threadNumber.getAndIncrement(),
                0); // 创建新线程，名称包含前缀和线程编号
        if (t.isDaemon()){
            t.setDaemon(false); // 确保线程不是守护线程
        }
        if (t.getPriority() != Thread.NORM_PRIORITY){
            t.setPriority(Thread.NORM_PRIORITY); // 确保线程具有正常优先级
        }
        return t;
    }

}
```

### 19. 用户上下文打通

![image-20240216170447094](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161704211.png)

#### 1.网关全局过滤器拦截请求获取token进行解析

用户发送请求，请求头中携带着Token（用户的身份标识），请求先来到网关层，Sa-Token框架`首先会对登录请求进行校验`，校验成功之后将请求`转发给各个微服务`，转发之前被会`网关层`的全局过滤器（`GlobalFilter`）拦截，全局过滤器通过`解析Token`，将获取到的登录ID添加到请求头中

> 需要注意的是，如果是登录或者是获取用户信息的请求，Sa-Token框架和全局过滤器的拦截都必须放开，因为这两个请求在未登录之前是不会携带Token的

![在这里插入图片描述](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402171856526.png)

![image-20240216172232184](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161722261.png)

![image-20240217190208002](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402171902067.png)

#### 2.获取当前登录用户的loginId

`登录拦截器`会从请求头中取出loginId，存入上下文对象中

![image-20240216172630866](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161726926.png)

#### 3.使用Feign远程调用其它模块的服务

![image-20240216172757229](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161727291.png)

#### 4.Feign请求拦截器

Feign远程调用其它模块的服务之前,会被`Feign请求拦截器`拦截,从HTTP请求中获取loginld的请求头`(用于获取登录信息的)`，将其`存入Feign请求的头部中`，后续使用Feign调用其它模块服务的时候，被调用的服务都可以接收到这个头部的信息

![image-20240216173155898](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161731003.png)

#### 5.微服务拦截拿到用户信息

当Feign调用auth服务的时候的，会被auth服务的`登录拦截器`拦下，可以将loginld存入这个模块的的上下文对象中

![image-20240216173605242](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161736316.png)

#### 6.使用Feign调用微服务成功

![image-20240216174544591](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161745657.png)

![image-20240216174642418](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161746490.png)

### 20. guava本地缓存

#### 本地缓存初始版本

先使用配置类构建一个本地缓存的Bean对象，以便在启动时注入到Spring容器中

![image-20240216192420079](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161924150.png)

查询分类和标签时，优先从缓存中获取，如果缓存中不存在，再从数据库中获取，同时存入缓存中一份，以便下次在缓存中获取

![image-20240216192228851](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402161922951.png)

#### 本地缓存优化后版本

将获取缓存的逻辑的逻辑全都抽取到一个工具类里面去处理，后续需要扩展缓存的处理逻辑直接修改工具类即可，不需要去动业务层的代码逻辑

![image-20240217180248254](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402171802353.png)

![image-20240217180421172](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402171804261.png)

### 21. 自定义封装ES

1、有自己的封装好的工具

2、集群，索引等等都要兼容的配置的概念

3、不想用 data 的这种方式，不够扩展

> 整体基于 es 的原生的 client 来去做。

![image-20240220074410448](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200744543.png)

#### ES配置类

自定义一个配置类，用于读取yml文件的ES配置信息

![image-20240220074605651](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200746710.png)

![image-20240220074742829](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200747877.png)

#### ES集群类

在实际的生产环境中，Elasticsearch（ES）通常不是作为单个节点运行的，而是以集群的形式出现。这样做的好处是：

1. **可扩展性（Scalability）**：当数据量增长或查询负载增加时，单个ES节点可能难以应对。通过使用集群，你可以通过添加更多的节点来水平扩展你的系统，从而增加存储容量和提高查询处理能力。
2. **高可用性（High Availability）**：在集群环境中，数据可以在多个节点之间复制。这意味着，即使某个节点失败，集群中的其他节点仍然可以提供数据和服务，从而确保系统的高可用性。
3. **故障容忍（Fault Tolerance）**：与高可用性紧密相关，集群通过在多个节点间复制数据，提供了故障容忍能力。这样，即使出现硬件故障或其他问题，也不会导致数据丢失。
4. **负载均衡（Load Balancing）**：集群允许请求在多个节点之间分配，从而分散负载。这有助于防止任何单一节点成为瓶颈，从而提高整体性能和响应时间。
5. **灵活的容量规划（Flexible Capacity Planning）**：随着业务需求的变化，集群可以灵活地添加或移除节点以适应存储需求和处理能力的变化，无需停机或显著影响服务。

![image-20240220074828221](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200748275.png)

#### ES索引类

在Elasticsearch（ES）中，索引是存储相关数据的基本单位，可以类比为关系型数据库中的“数据库”概念。

![image-20240220074934738](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200749800.png)

#### 自定义的ES工具类

自定义`EsRestClient`工具类在使用Elasticsearch进行数据操作时带来了多项好处，主要包括：

1. **封装和简化操作**：`EsRestClient`封装了Elasticsearch客户端的常用操作，如文档的增删改查、批量操作、分词查询等，简化了直接使用Elasticsearch原生客户端API时的复杂性。这样，开发者可以通过更简洁的方法完成相应的操作，提高开发效率。
2. **统一管理**：通过在单个工具类中集中管理与Elasticsearch相关的操作，可以更容易地维护和更新这些操作。例如，如果Elasticsearch客户端API更新或者业务需求变化，只需在`EsRestClient`中做修改即可，而不需要在多个地方修改。
3. **错误处理和日志记录**：`EsRestClient`提供了统一的错误处理和日志记录机制。在执行Elasticsearch操作时，可以捕获异常、记录错误日志，方便问题的调试和解决。
4. **连接管理**：`EsRestClient`管理Elasticsearch客户端的连接，包括初始化连接和使用连接池等，确保资源的有效利用。特别是在面对多个Elasticsearch集群时，可以通过配置管理不同的客户端连接，满足不同环境或不同业务需求的数据操作。
5. **扩展性和灵活性**：自定义工具类允许根据具体业务需求扩展Elasticsearch的操作功能，比如添加特定的查询逻辑、数据处理方法等，提供了更高的灵活性。
6. **提高代码的重用性**：在多个项目或模块中，可能会重复使用到相同的Elasticsearch操作逻辑。通过将这些逻辑封装在`EsRestClient`中，可以很方便地在不同项目之间共享和重用代码，避免了代码的重复编写。

```java
/**
 * 自定义的ES工具类
 */
@Component
@Slf4j
public class EsRestClient {

    // 用于存储不同Elasticsearch集群客户端的映射
    public static Map<String, RestHighLevelClient> clientMap = new HashMap<>();

    @Resource
    private EsConfigProperties esConfigProperties; // 自动注入Elasticsearch配置属性

    private static final RequestOptions COMMON_OPTIONS; // 通用的请求选项

    static {
        // 初始化通用请求选项
        RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
        COMMON_OPTIONS = builder.build();
    }

    // 在Bean的初始化之后执行此方法，用于初始化连接Elasticsearch集群的客户端。
    @PostConstruct
    public void initialize() {
        // 从配置文件中读取Elasticsearch集群的配置信息。
        List<EsClusterConfig> esConfigs = esConfigProperties.getEsConfigs();
        for (EsClusterConfig esConfig : esConfigs) {
            // 记录日志信息，显示正在初始化的Elasticsearch集群名称和节点信息。
            log.info("initialize.config.name:{},node:{}", esConfig.getName(), esConfig.getNodes());
            // 调用下面的方法初始化RestHighLevelClient客户端。
            RestHighLevelClient restHighLevelClient = initRestClient(esConfig);
            if (restHighLevelClient != null) {
                // 如果客户端初始化成功，则将其存入一个静态Map中，键为集群名称，值为对应的客户端实例。
                clientMap.put(esConfig.getName(), restHighLevelClient);
            } else {
                // 如果客户端初始化失败，则记录错误日志。
                log.error("config.name:{},node:{}.initError", esConfig.getName(), esConfig.getNodes());
            }
        }
    }

    // 根据Elasticsearch集群配置初始化RestHighLevelClient客户端的方法。
    private RestHighLevelClient initRestClient(EsClusterConfig esClusterConfig) {
        // 将配置中的节点信息（ip:port格式的字符串）分割成数组。
        String[] ipPortArr = esClusterConfig.getNodes().split(",");
        List<HttpHost> httpHostList = new ArrayList<>(ipPortArr.length);
        for (String ipPort : ipPortArr) {
            // 再将每个节点信息分割成IP和端口。
            String[] ipPortInfo = ipPort.split(":");
            if (ipPortInfo.length == 2) {
                // 根据IP和端口创建HttpHost对象，并加入到列表中。
                HttpHost httpHost = new HttpHost(ipPortInfo[0], NumberUtils.toInt(ipPortInfo[1]));
                httpHostList.add(httpHost);
            }
        }
        // 将List转换成HttpHost数组。
        HttpHost[] httpHosts = new HttpHost[httpHostList.size()];
        httpHostList.toArray(httpHosts);

        // 使用HttpHost数组创建RestClientBuilder。
        RestClientBuilder builder = RestClient.builder(httpHosts);
        // 使用RestClientBuilder创建RestHighLevelClient客户端实例。
        RestHighLevelClient restHighLevelClient = new RestHighLevelClient(builder);
        return restHighLevelClient;
    }


    // 根据集群名称获取Elasticsearch客户端实例
    private static RestHighLevelClient getClient(String clusterName) {
        // 从clientMap中根据集群名称获取对应的RestHighLevelClient实例
        return clientMap.get(clusterName);
    }

    // 向Elasticsearch插入文档的方法
    public static boolean insertDoc(EsIndexInfo esIndexInfo, EsSourceData esSourceData) {
        try {
            // 创建一个索引请求，指定索引名
            IndexRequest indexRequest = new IndexRequest(esIndexInfo.getIndexName());
            // 设置要插入的文档内容
            indexRequest.source(esSourceData.getData());
            // 设置文档ID
            indexRequest.id(esSourceData.getDocId());
            // 获取对应集群的客户端并执行索引请求，使用通用的请求选项
            getClient(esIndexInfo.getClusterName()).index(indexRequest, COMMON_OPTIONS);
            // 如果执行无异常，返回true表示插入成功
            return true;
        } catch (Exception e) {
            // 记录异常信息
            log.error("insertDoc.exception:{}", e.getMessage(), e);
        }
        // 出现异常返回false表示插入失败
        return false;
    }

    // 更新Elasticsearch中的文档
    public static boolean updateDoc(EsIndexInfo esIndexInfo, EsSourceData esSourceData) {
        try {
            // 创建一个更新请求，指定索引名和文档ID
            UpdateRequest updateRequest = new UpdateRequest();
            updateRequest.index(esIndexInfo.getIndexName());
            updateRequest.id(esSourceData.getDocId());
            // 设置要更新的文档内容
            updateRequest.doc(esSourceData.getData());
            // 获取对应集群的客户端并执行更新请求，使用通用的请求选项
            getClient(esIndexInfo.getClusterName()).update(updateRequest, COMMON_OPTIONS);
            // 如果执行无异常，返回true表示更新成功
            return true;
        } catch (Exception e) {
            // 记录异常信息
            log.error("updateDoc.exception:{}", e.getMessage(), e);
        }
        // 出现异常返回false表示更新失败
        return false;
    }

    // 批量更新文档的方法
    public static boolean batchUpdateDoc(EsIndexInfo esIndexInfo, List<EsSourceData> esSourceDataList) {
        try {
            boolean flag = false; // 用于标记是否有文档被添加到批处理请求中
            BulkRequest bulkRequest = new BulkRequest(); // 创建批量请求对象
            // 遍历要更新的文档数据列表
            for (EsSourceData esSourceData : esSourceDataList) {
                String docId = esSourceData.getDocId(); // 获取每个文档的ID
                // 如果文档ID不为空
                if (StringUtils.isNotBlank(docId)) {
                    UpdateRequest updateRequest = new UpdateRequest(); // 创建更新请求
                    updateRequest.index(esIndexInfo.getIndexName()); // 设置索引名
                    updateRequest.id(docId); // 设置文档ID
                    updateRequest.doc(esSourceData.getData()); // 设置要更新的内容
                    bulkRequest.add(updateRequest); // 将更新请求添加到批量请求中
                    flag = true; // 标记已有文档被添加到批处理请求
                }
            }

            // 如果有文档被添加到批处理请求
            if (flag) {
                BulkResponse bulk = getClient(esIndexInfo.getClusterName()).bulk(bulkRequest, COMMON_OPTIONS); // 执行批量请求
                if (bulk.hasFailures()) { // 如果批量操作中有失败
                    return false; // 返回false表示批量更新失败
                }
            }

            return true; // 返回true表示批量更新成功
        } catch (Exception e) {
            log.error("batchUpdateDoc.exception:{}", e.getMessage(), e); // 记录异常信息
        }
        return false; // 出现异常返回false表示批量更新失败
    }

    // 删除索引中的所有文档
    public static boolean delete(EsIndexInfo esIndexInfo) {
        try {
            DeleteByQueryRequest deleteByQueryRequest = new DeleteByQueryRequest(esIndexInfo.getIndexName()); // 创建删除请求，指定索引名
            deleteByQueryRequest.setQuery(QueryBuilders.matchAllQuery()); // 设置查询条件为匹配所有文档
            BulkByScrollResponse response = getClient(esIndexInfo.getClusterName())
                    .deleteByQuery(deleteByQueryRequest, COMMON_OPTIONS); // 执行删除操作
            long deleted = response.getDeleted(); // 获取被删除的文档数量
            log.info("deleted.size:{}", deleted); // 记录删除的文档数量
            return true; // 返回true表示删除成功
        } catch (Exception e) {
            log.error("delete.exception:{}", e.getMessage(), e); // 记录异常信息
        }
        return false; // 出现异常返回false表示删除失败
    }


    // 删除指定ID的文档方法
    public static boolean deleteDoc(EsIndexInfo esIndexInfo, String docId) {
        try {
            DeleteRequest deleteRequest = new DeleteRequest(esIndexInfo.getIndexName()); // 创建删除请求，并指定索引名称
            deleteRequest.id(docId); // 设置要删除的文档ID
            DeleteResponse response = getClient(esIndexInfo.getClusterName()).delete(deleteRequest, COMMON_OPTIONS); // 执行删除操作，并获取响应
            log.info("deleteDoc.response:{}", JSON.toJSONString(response)); // 记录删除操作的响应信息
            return true; // 如果操作成功完成，返回true
        } catch (Exception e) {
            log.error("deleteDoc.exception:{}", e.getMessage(), e); // 如果操作过程中出现异常，记录异常信息
        }
        return false; // 如果操作失败或出现异常，返回false
    }

    // 检查指定ID的文档是否存在方法
    public static boolean isExistDocById(EsIndexInfo esIndexInfo, String docId) {
        try {
            GetRequest getRequest = new GetRequest(esIndexInfo.getIndexName()); // 创建一个获取请求，并指定索引名称
            getRequest.id(docId); // 设置要检查的文档ID
            return getClient(esIndexInfo.getClusterName()).exists(getRequest, COMMON_OPTIONS); // 执行检查操作，如果文档存在返回true，否则返回false
        } catch (Exception e) {
            log.error("isExistDocById.exception:{}", e.getMessage(), e); // 如果操作过程中出现异常，记录异常信息
        }
        return false; // 如果操作失败或出现异常，返回false
    }


    // 通过ID获取文档内容
    public static Map<String, Object> getDocById(EsIndexInfo esIndexInfo, String docId) {
        try {
            GetRequest getRequest = new GetRequest(esIndexInfo.getIndexName()); // 创建获取请求，指定索引名称
            getRequest.id(docId); // 设置要获取的文档ID
            GetResponse response = getClient(esIndexInfo.getClusterName()).get(getRequest, COMMON_OPTIONS); // 执行获取操作，并获取响应
            Map<String, Object> source = response.getSource(); // 从响应中提取文档内容
            return source; // 返回文档内容
        } catch (Exception e) {
            log.error("getDocById.exception:{}", e.getMessage(), e); // 如果操作过程中出现异常，记录异常信息
        }
        return null; // 如果操作失败或出现异常，返回null
    }

    // 通过ID获取文档的指定字段
    public static Map<String, Object> getDocById(EsIndexInfo esIndexInfo, String docId, String[] fields) {
        try {
            GetRequest getRequest = new GetRequest(esIndexInfo.getIndexName()); // 创建获取请求，指定索引名称
            getRequest.id(docId); // 设置要获取的文档ID
            FetchSourceContext fetchSourceContext = new FetchSourceContext(true, fields, null); // 创建字段过滤上下文，指定要获取的字段
            getRequest.fetchSourceContext(fetchSourceContext); // 设置请求的字段过滤上下文
            GetResponse response = getClient(esIndexInfo.getClusterName()).get(getRequest, COMMON_OPTIONS); // 执行获取操作，并获取响应
            Map<String, Object> source = response.getSource(); // 从响应中提取文档内容
            return source; // 返回文档内容
        } catch (Exception e) {
            log.error("getDocById.exception:{}", e.getMessage(), e); // 如果操作过程中出现异常，记录异常信息
        }
        return null; // 如果操作失败或出现异常，返回null
    }
    

    // 使用Term查询进行搜索
    public static SearchResponse searchWithTermQuery(EsIndexInfo esIndexInfo, EsSearchRequest esSearchRequest) {
        try {
            BoolQueryBuilder bq = esSearchRequest.getBq(); // 获取布尔查询构建器
            String[] fields = esSearchRequest.getFields(); // 获取需要检索的字段
            int from = esSearchRequest.getFrom(); // 获取查询的起始位置（分页用）
            int size = esSearchRequest.getSize(); // 获取查询的大小（即每页显示的记录数）
            Long minutes = esSearchRequest.getMinutes(); // 获取滚动搜索的持续时间
            Boolean needScroll = esSearchRequest.getNeedScroll(); // 判断是否需要使用滚动搜索
            String sortName = esSearchRequest.getSortName(); // 获取排序字段名
            SortOrder sortOrder = esSearchRequest.getSortOrder(); // 获取排序方式（升序或降序）

            // 创建搜索源构建器，并设置查询条件、检索字段、分页信息等
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(bq); // 设置查询条件
            searchSourceBuilder.fetchSource(fields, null).from(from).size(size); // 设置检索的字段和分页信息

            // 如果有高亮设置，添加高亮配置
            if (Objects.nonNull(esSearchRequest.getHighlightBuilder())) {
                searchSourceBuilder.highlighter(esSearchRequest.getHighlightBuilder());
            }

            // 如果有排序字段名，添加排序配置
            if (StringUtils.isNotBlank(sortName)) {
                searchSourceBuilder.sort(sortName);
            }

            // 默认按评分降序排序
            searchSourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));

            // 创建搜索请求，设置搜索类型、索引名和搜索源
            SearchRequest searchRequest = new SearchRequest();
            searchRequest.searchType(SearchType.DEFAULT); // 设置搜索类型
            searchRequest.indices(esIndexInfo.getIndexName()); // 设置索引名
            searchRequest.source(searchSourceBuilder); // 设置搜索源

            // 如果需要滚动搜索，设置滚动时间
            if (needScroll) {
                Scroll scroll = new Scroll(TimeValue.timeValueMinutes(minutes));
                searchRequest.scroll(scroll);
            }

            // 执行搜索并返回搜索响应
            SearchResponse search = getClient(esIndexInfo.getClusterName()).search(searchRequest, COMMON_OPTIONS);
            return search;
        } catch (Exception e) {
            log.error("searchWithTermQuery.exception:{}", e.getMessage(), e); // 捕获异常并记录
        }
        return null; // 如果出现异常，返回null
    }


    // 批量插入文档
    public static boolean batchInsertDoc(EsIndexInfo esIndexInfo, List<EsSourceData> esSourceDataList) {
        // 日志记录批量新增的文档数量和索引名
        if (log.isInfoEnabled()) {
            log.info("批量新增ES文档数量:" + esSourceDataList.size());
            log.info("索引名:" + esIndexInfo.getIndexName());
        }
        try {
            boolean flag = false; // 标记是否有文档被添加到批处理请求中
            BulkRequest bulkRequest = new BulkRequest(); // 创建批量请求

            // 遍历文档数据列表，将每个文档添加到批量请求中
            for (EsSourceData source : esSourceDataList) {
                String docId = source.getDocId(); // 获取文档ID
                if (StringUtils.isNotBlank(docId)) { // 检查文档ID是否非空
                    IndexRequest indexRequest = new IndexRequest(esIndexInfo.getIndexName()); // 创建索引请求
                    indexRequest.id(docId); // 设置文档ID
                    indexRequest.source(source.getData()); // 设置文档数据
                    bulkRequest.add(indexRequest); // 将索引请求添加到批量请求中
                    flag = true; // 标记已有文档添加到批处理请求
                }
            }

            // 如果批量请求中有文档，执行批量操作
            if (flag) {
                BulkResponse response = getClient(esIndexInfo.getClusterName()).bulk(bulkRequest, COMMON_OPTIONS); // 执行批量请求
                if (response.hasFailures()) { // 检查批量操作是否有失败
                    return false; // 如果有失败，返回false
                }
            }
        } catch (Exception e) {
            log.error("batchInsertDoc.error", e); // 捕获异常并记录
            return false; // 发生异常，返回false
        }

        return true; // 批量插入操作成功
    }

    // 通过查询条件更新文档
    public static boolean updateByQuery(EsIndexInfo esIndexInfo, QueryBuilder queryBuilder, Script script, int batchSize) {
        // 日志记录更新操作的索引名
        if (log.isInfoEnabled()) {
            log.info("通过查询条件更新文档，索引名:" + esIndexInfo.getIndexName());
        }
        try {
            UpdateByQueryRequest updateByQueryRequest = new UpdateByQueryRequest(esIndexInfo.getIndexName()); // 创建通过查询更新请求
            updateByQueryRequest.setQuery(queryBuilder); // 设置查询条件
            updateByQueryRequest.setScript(script); // 设置更新时使用的脚本
            updateByQueryRequest.setBatchSize(batchSize); // 设置每批处理的文档数目
            updateByQueryRequest.setAbortOnVersionConflict(false); // 设置遇到版本冲突时是否中止操作

            // 执行通过查询更新操作
            BulkByScrollResponse response = getClient(esIndexInfo.getClusterName()).updateByQuery(updateByQueryRequest, RequestOptions.DEFAULT);
            // 检查操作中是否有失败
            List<BulkItemResponse.Failure> failures = response.getBulkFailures();
            if (!failures.isEmpty()) { // 如果有失败，记录失败信息并返回false
                failures.forEach(failure -> log.error("更新操作中的失败：" + failure.getMessage()));
                return false;
            }
        } catch (Exception e) {
            log.error("updateByQuery.error", e); // 捕获异常并记录
            return false; // 发生异常，返回false
        }
        return true; // 更新操作成功
    }


    /**
     * 使用Elasticsearch的分词器对文本进行分词
     *
     * @param esIndexInfo Elasticsearch索引信息，包含索引名和集群名
     * @param text 要分词的文本
     * @return 分词结果列表，每个元素是一个分词后的词
     * @throws Exception 如果执行分词请求失败或处理响应时出现错误
     */
    public static List<String> getAnalyze(EsIndexInfo esIndexInfo, String text) throws Exception {
        List<String> list = new ArrayList<String>(); // 存储分词结果的列表
        Request request = new Request("GET", "_analyze"); // 创建分词请求，指定方法为GET，路径为_analyze
        JSONObject entity = new JSONObject(); // 创建JSON对象用于设置请求体
        entity.put("analyzer", "ik_smart"); // 设置使用的分词器，这里使用ik_smart分词器
        entity.put("text", text); // 设置要分词的文本
        request.setJsonEntity(entity.toJSONString()); // 将JSON对象转换为字符串并设置为请求体

        // 执行分词请求，获取响应
        Response response = getClient(esIndexInfo.getClusterName()).getLowLevelClient().performRequest(request);
        // 解析响应体，获取分词结果
        JSONObject tokens = JSONObject.parseObject(EntityUtils.toString(response.getEntity()));
        JSONArray arrays = tokens.getJSONArray("tokens"); // 从响应中获取分词结果数组
        // 遍历分词结果数组，提取分词并添加到结果列表中
        for (int i = 0; i < arrays.size(); i++) {
            JSONObject obj = JSON.parseObject(arrays.getString(i)); // 获取每个分词结果的JSON对象
            list.add(obj.getString("token")); // 提取分词结果中的token字段（实际的分词）并添加到列表中
        }
        return list; // 返回分词结果列表
    }
}
```

#### 封装的ES请求类

![image-20240220075803476](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200758573.png)

#### 封装的ES返回类

![image-20240220075849811](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200758857.png)

#### 插入题目的时候同步到ES中

![image-20240220080217773](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200802837.png)

![image-20240220080349447](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200803500.png)

![image-20240220080309041](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200803108.png)

#### 通过关键词进行全文检索题目（ES）

![image-20240220080831586](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200808652.png)

![image-20240220080918452](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200809506.png)

##### 1. 通过ES去查询题目列表

![image-20240220081630138](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200816207.png)

##### 2. 封装ES查询请求的方法

![image-20240220082018687](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200820752.png)

##### 3. 使用自定义的ES工具类进行查询，并设置高亮

![image-20240220082320032](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200823125.png)

##### 4. 将查询结果转换到我们封装的实体类中

![image-20240220081041636](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200810764.png)

##### 5. 最终将通过ES查询的结果返回给前端

![image-20240220082927034](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200829092.png)

### 22. 实现题目贡献排行榜

![image-20240220083704174](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200837236.png)

#### 基于MySQL数据库实现

现在数据库里面的 createby 字段。用户的标识是唯一的，那我直接通过 group by 的形式统计 count。

select count(1),create_by from subject_info group by create_by limit 0,5;

数据量比较小，并发也比较小。这种方案是 ok 的。保证可以走到索引，返回速度快，不要产生慢 sql。

在数据库层面加一层缓存，接受一定的延时性。

![image-20240220083841622](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200838694.png)

![image-20240220084204706](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200842766.png)

#### 基于Reids缓存实现

有序集合，不允许重复的成员，然后每一个 key 都会包含一个 score 分数的概念。redis 根据分数可以帮助我们做从小到大，和从大到小的一个处理。

有序集合的 key 不可重复，score 重复。

它通过我们的一个哈希表来实现的，添加，删除，查找，复杂度 o(1) ，最大数量是 2 32 次方-1.

zadd

zrange

zincrby

zscore

这种的好处在于，完全不用和数据库做任何的交互，纯纯的通过缓存来做，速度非常快，要避免一些大 key 的问题。

因为是`完全基于Redis缓存实现排行榜`，`不与数据库做任何交互`,所以我们每次在添加题目的时候，需要在Redis中做一个记录，每当有一个用户贡献题目的时候，我们需要将其在redis中记录的贡献题目总量+1。

![image-20240220084957368](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200849429.png)

![image-20240220085056649](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200850721.png)

基于Redis获取题目贡献榜

![image-20240220085322309](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200853403.png)

### 23. 点赞与收藏功能设计

点赞与收藏的逻辑是非常一样的，我们这里就选取点赞功能来给大家做开发。

按照我们 职趣club 的设计，点赞业务其实涉及几个方面，

一、我们肯定要知道`一个题目被多少人点过赞`，还要知道，`每个人他点赞了哪些题目`。

二、`点赞的业务特性，频繁`。用户一多，时时刻刻都在进行点赞啊，收藏啊等等处理，如果说我们采取传统的数据库的模式啊，这个交互量是非常大的，很难去抗住这个并发问题，所以我们采取 redis 的方式来做。

三、`查询的数据交互`，我们可以和 redis 直接来做，持久化的数据，通过数据库查询即可，这个数据如何去同步到数据库，我们就采取的定时任务 xxl-job 定期来刷数据。

![image-20240220091322738](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402200913795.png)

记录的时候三个关键信息，点赞的人，被点赞的题目，点赞的状态。

我们最终的数据结构就是 hash，string 类型。

hash，存到一个键里面，键里是一个 map，他又分为 hashkey 和 hashval。

hashkey，subjectId:userId，val 就存的是点赞的状态 1 是点赞 0 是不点赞。

string 类型 key subjectId，val 即使我们的题目被点赞的数量

string 类型。key subjectId:userId.

#### 1. 新增点赞

![image-20240223193753360](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231937482.png)

#### 2. 新增点赞的具体实现

![image-20240223193932952](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231939005.png)

##### Redis点赞状态前缀

![image-20240223202008607](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232020660.png)

##### Reids点赞数量前缀

![image-20240223201930183](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232019233.png)

##### Redis点赞详情前缀

![image-20240223202335704](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232023747.png)

##### 代码具体实现

![image-20240223194928653](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231949737.png)

#### 3. 判断是否点赞

![image-20240223195148489](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231951561.png)

![image-20240223195058753](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231950804.png)

#### 4. 获取点赞数量

![image-20240223195246791](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231952847.png)

![image-20240223195257816](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231952884.png)

#### 5. 查询我的点赞列表

![image-20240223195343055](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231953132.png)

![image-20240223195502833](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231955920.png)

![image-20240223195738185](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402231957252.png)

### 24. 集成xxl-job同步点赞数据

[XXL-JOB详解（整合springboot）保姆级教程]: https://blog.csdn.net/qq_57581439/article/details/128319069

**具体安装参考上面这篇博客**

![image-20240223200440066](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232004135.png)

![image-20240223200828054](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232008147.png)

![image-20240223200306354](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232003436.png)

#### 1. 引入xxl-job依赖

```xml
        <dependency>
            <groupId>com.xuxueli</groupId>
            <artifactId>xxl-job-core</artifactId>
            <version>2.3.1</version>
        </dependency>
```

#### 2. yml文件配置

![image-20240223201013346](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232010409.png)

#### 3. 添加xxl-job config配置类

```java
/**
 * xxl-job config
 *
 * @author xuxueli 2017-04-28
 */
@Configuration
public class XxlJobConfig {
    private Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);

    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;

    @Value("${xxl.job.accessToken}")
    private String accessToken;

    @Value("${xxl.job.executor.appname}")
    private String appname;

    @Value("${xxl.job.executor.address}")
    private String address;

    @Value("${xxl.job.executor.ip}")
    private String ip;

    @Value("${xxl.job.executor.port}")
    private int port;

    @Value("${xxl.job.executor.logpath}")
    private String logPath;

    @Value("${xxl.job.executor.logretentiondays}")
    private int logRetentionDays;


    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        logger.info(">>>>>>>>>>> xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(adminAddresses);
        xxlJobSpringExecutor.setAppname(appname);
        xxlJobSpringExecutor.setAddress(address);
        xxlJobSpringExecutor.setIp(ip);
        xxlJobSpringExecutor.setPort(port);
        xxlJobSpringExecutor.setAccessToken(accessToken);
        xxlJobSpringExecutor.setLogPath(logPath);
        xxlJobSpringExecutor.setLogRetentionDays(logRetentionDays);

        return xxlJobSpringExecutor;
    }

    /**
     * 针对多网卡、容器内部署等情况，可借助 "spring-cloud-commons" 提供的 "InetUtils" 组件灵活定制注册IP；
     *
     *      1、引入依赖：
     *          <dependency>
     *             <groupId>org.springframework.cloud</groupId>
     *             <artifactId>spring-cloud-commons</artifactId>
     *             <version>${version}</version>
     *         </dependency>
     *
     *      2、配置文件，或者容器启动变量
     *          spring.cloud.inetutils.preferred-networks: 'xxx.xxx.xxx.'
     *
     *      3、获取IP
     *          String ip_ = inetUtils.findFirstNonLoopbackHostInfo().getIpAddress();
     */
}
```

#### 4. 同步点赞数据

![image-20240223201303767](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232013825.png)

![image-20240223202808368](https://web-183.oss-cn-beijing.aliyuncs.com/typora/202402232028459.png)
