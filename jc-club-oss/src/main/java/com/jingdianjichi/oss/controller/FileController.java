package com.jingdianjichi.oss.controller;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.jingdianjichi.oss.service.FileService;
import com.jingdianjichi.oss.entity.Result;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-02-07
 */
@RestController
//@RequestMapping("/oss")
public class FileController {

	@Resource
	private FileService fileService;

	@NacosValue(value="${storage.service.type}",autoRefreshed = true)
	private String storageType;

	/**
	 * 获取所有的桶
	 * @return
	 */
	@RequestMapping("/testGetAllBuckets")
	public String testGetAllBuckets() 	{
		List<String> allBucket = fileService.getAllBucket();
		return allBucket.get(0);
	}

	/**
	 * 测试
	 * @return
	 */
	@RequestMapping("/testNacos")
	public String testNacos() {
		return storageType;
	}


	/**
	 * 获取文件url地址
	 * @param bucketName
	 * @param ObjectName
	 * @return
	 */
	@RequestMapping("/getUrl")
	public String getUrl(String bucketName, String ObjectName) {
		return fileService.getUrl(bucketName,ObjectName);
	}

	/**
	 * 上传文件
	 * @param uploadFile  上传的文件
	 * @param bucket	  桶名
	 * @param objectName 文件夹名
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/upload")
	public Result upload(MultipartFile uploadFile, String bucket, String objectName) throws Exception {
		String url = fileService.uploadFile(uploadFile, bucket, objectName);
		return Result.ok(url);
	}

}
