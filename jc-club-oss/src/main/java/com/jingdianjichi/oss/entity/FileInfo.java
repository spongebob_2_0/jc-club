package com.jingdianjichi.oss.entity;

import lombok.Data;

/**
 * 文件信息类
 * 用于存储文件的基本信息，包括文件名、是否为目录以及ETag值。
 * 利用Lombok库自动生成getter和setter方法。
 *
 * @author: WuYimin
 * Date: 2024-02-07
 */
@Data
public class FileInfo {

	/**
	 * 文件名
	 * 存储文件或目录的名称。
	 */
	private String fileName;

	/**
	 * 目录标志
	 * 一个布尔值，表示该文件实体是否为目录。
	 * true表示是目录，false表示不是目录。
	 */
	private Boolean directoryFlag;

	/**
	 * ETag
	 * 文件的ETag值，通常用于验证文件的版本变化。
	 * ETag是文件的唯一标识符，可以用来检查文件是否已经被修改。
	 */
	private String etag;

}
