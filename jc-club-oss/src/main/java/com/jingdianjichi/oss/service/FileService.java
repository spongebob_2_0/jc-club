package com.jingdianjichi.oss.service;

import com.jingdianjichi.oss.adapter.StorageAdapter;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-02-07
 */
@Service
public class FileService {

	public final StorageAdapter storageAdapter;

	public FileService(StorageAdapter storageAdapter) {
		this.storageAdapter = storageAdapter;
	}

	/**
	 * 列出所有存储桶的名称
	 *
	 * @return 存储桶名称列表
	 */
	public List<String> getAllBucket() {
		return storageAdapter.getAllBucket();
	}


	/**
	 * 获取文件路径
	 */
	public String getUrl(String bucketName,String objectName) {
		return storageAdapter.getUrl(bucketName,objectName);
	}

	/**
	 * 上传文件
	 *
	 * @param bucket 云存储服务中的存储桶名称，用于存储上传的文件。
	 * @return 返回上传文件的URL地址，这个地址可以用来直接访问或下载文件。
	 */
	public String uploadFile(MultipartFile file, String bucket, String objectName){

		// 避免文件覆盖 (用UUID生成一个随机值拼接在文件后缀名的前面)
		String originalFilename = file.getOriginalFilename();  //获取文件名
		String fileName = UUID.randomUUID().toString() + originalFilename.substring(originalFilename.lastIndexOf("."));

		// 上传到"icon"文件夹，objectName为"icon/" + 原始文件名
		String finalObjectName = objectName + "/" + fileName;

		// 调用storageAdapter的uploadFile方法上传文件到指定文件夹下面
		storageAdapter.uploadFile(file, bucket, finalObjectName);

		// 调用storageAdapter的getUrl方法，传入bucket和更新后的finalObjectName，以获取该文件的访问URL。
		// 这个URL可以用来访问或分享上传的文件。
		return storageAdapter.getUrl(bucket, finalObjectName);
	}

}
