package com.jingdianjichi.oss.adapter;

import com.jingdianjichi.oss.entity.FileInfo;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.List;

/**
 * 文件存储适配器
 *
 * @author: WuYimin
 * Date: 2024-02-07
 */
@Service
public interface StorageAdapter {

	/**
	 * 创建存储桶
	 * 如果指定的存储桶不存在，则创建它。
	 *
	 * @param bucket 存储桶名称
	 */
	public void createBucket(String bucket);

	/**
	 * 上传文件到MinIO
	 *
	 * @param uploadFile 文件
	 * @param bucket 存储桶名称
	 * @param objectName 文件在存储桶中的对象名称
	 */
	public void uploadFile(MultipartFile uploadFile,  String bucket, String objectName);

	/**
	 * 列出所有存储桶的名称
	 *
	 * @return 存储桶名称列表
	 */
	public List<String> getAllBucket() ;

	/**
	 * 列出指定存储桶中的所有文件
	 *
	 * @param bucket 存储桶名称
	 * @return 文件信息列表
	 */
	public List<FileInfo> getAllFile(String bucket);

	/**
	 * 从MinIO下载文件
	 *
	 * @param bucket 存储桶名称
	 * @param objectName 文件对象名称
	 * @return 文件的输入流
	 */
	public InputStream downLoad(String bucket, String objectName);

	/**
	 * 删除存储桶
	 *
	 * @param bucket 要删除的存储桶名称
	 */
	public void deleteBucket(String bucket);

	/**
	 * 删除存储桶中的文件对象
	 *
	 * @param bucket 存储桶名称
	 * @param objectName 文件对象名称
	 */
	public void deleteObject(String bucket, String objectName);

	/**
	 * 获取文件地址
	 * @param bucket
	 * @param objectName
	 * @return
	 */
	String getUrl(String bucket, String objectName);
}
