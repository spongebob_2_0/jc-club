package com.jingdianjichi.oss.adapter;

import com.jingdianjichi.oss.entity.FileInfo;
import com.jingdianjichi.oss.adapter.StorageAdapter;
import com.jingdianjichi.oss.util.MinioUtil;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.List;

/**
 * Minio存储服务实现类
 * 实现了 StorageAdapter 接口，提供了对Minio存储操作的具体实现。
 *
 * @author: WuYimin
 * Date: 2024-02-07
 */
public class MinioStorageAdapter implements StorageAdapter {

	@Resource
	private MinioUtil minioUtil;

	/**
	 * minioUrl
	 */
	@Value("${minio.url}")
	private String url;

	/**
	 * 创建存储桶
	 * 如果存储桶不存在，则创建新的存储桶。
	 *
	 * @param bucket 存储桶的名称
	 */
	@Override
	@SneakyThrows
	public void createBucket(String bucket) {
		minioUtil.createBucket(bucket);
	}

	/**
	 * 上传文件到存储桶
	 * 将文件上传到指定的存储桶中。如果指定了objectName，则上传的文件将保存在该路径下。
	 *
	 * @param uploadFile 要上传的文件
	 * @param bucket 存储桶的名称
	 * @param objectName 文件在存储桶中的路径和名称
	 */
	@Override
	@SneakyThrows
	public void uploadFile(MultipartFile uploadFile, String bucket, String objectName) {
		minioUtil.createBucket(bucket); // 确保存储桶存在
		if (objectName != null) {
			minioUtil.uploadFile(uploadFile.getInputStream(), bucket, objectName + "/" + uploadFile.getOriginalFilename());
		} else {
			minioUtil.uploadFile(uploadFile.getInputStream(), bucket, uploadFile.getOriginalFilename());
		}
	}

	/**
	 * 获取所有存储桶的名称
	 *
	 * @return 存储桶名称列表
	 */
	@Override
	@SneakyThrows
	public List<String> getAllBucket() {
		return minioUtil.getAllBucket();
	}

	/**
	 * 获取指定存储桶中的所有文件信息
	 *
	 * @param bucket 存储桶的名称
	 * @return 文件信息列表
	 */
	@Override
	@SneakyThrows
	public List<FileInfo> getAllFile(String bucket) {
		return minioUtil.getAllFile(bucket);
	}

	/**
	 * 从存储桶中下载文件
	 *
	 * @param bucket 存储桶的名称
	 * @param objectName 文件在存储桶中的路径和名称
	 * @return 文件的输入流
	 */
	@Override
	@SneakyThrows
	public InputStream downLoad(String bucket, String objectName) {
		return minioUtil.downLoad(bucket, objectName);
	}

	/**
	 * 删除存储桶
	 * 如果存储桶为空，则可以删除。
	 *
	 * @param bucket 存储桶的名称
	 */
	@Override
	@SneakyThrows
	public void deleteBucket(String bucket) {
		minioUtil.deleteBucket(bucket);
	}

	/**
	 * 删除存储桶中的文件
	 *
	 * @param bucket 存储桶的名称
	 * @param objectName 文件在存储桶中的路径和名称
	 */
	@Override
	@SneakyThrows
	public void deleteObject(String bucket, String objectName) {
		minioUtil.deleteObject(bucket, objectName);
	}

	@Override
	@SneakyThrows
	public String getUrl(String bucket, String objectName) {
		return url + "/" + bucket + "/" + objectName;
	}
}
