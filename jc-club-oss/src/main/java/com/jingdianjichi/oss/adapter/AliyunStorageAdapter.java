package com.jingdianjichi.oss.adapter;

import com.aliyun.oss.OSS;
import com.jingdianjichi.oss.entity.FileInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.util.List;

public class AliyunStorageAdapter implements StorageAdapter {

	@Resource
	private OSS ossClient;

	@Value("${aliyun.oss.bucketName}")
	private String defaultBucketName; // 默认存储桶

	@Value("${aliyun.oss.endpoint}")
	private String endpoint;   // 阿里云域名



	@Override
	public void uploadFile(MultipartFile uploadFile, String bucket, String finalObjectName) {
		try {
			// 若未指定bucket，则使用默认bucket
			if (bucket == null || bucket.isEmpty()) {
				bucket = defaultBucketName;
			}
			ossClient.putObject(bucket, finalObjectName, uploadFile.getInputStream());
		} catch (Exception e) {
			throw new RuntimeException("上传文件到OSS失败", e);
		}
	}

	@Override
	public String getUrl(String bucketName, String finalObjectName) {
		// 拼接访问路径：桶名+域名+文件访问路径
		return String.format("https://%s.%s/%s", bucketName, this.endpoint, finalObjectName);

	}


	@Override
	public void createBucket(String bucket) {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public List<String> getAllBucket() {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public List<FileInfo> getAllFile(String bucket) {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public InputStream downLoad(String bucket, String objectName) {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public void deleteBucket(String bucket) {
		throw new UnsupportedOperationException("Operation not supported.");
	}

	@Override
	public void deleteObject(String bucket, String objectName) {
		throw new UnsupportedOperationException("Operation not supported.");
	}
}
