package com.jingdianjichi.oss.config;

import com.jingdianjichi.oss.adapter.AliyunStorageAdapter;
import com.jingdianjichi.oss.adapter.MinioStorageAdapter;
import com.jingdianjichi.oss.adapter.StorageAdapter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RefreshScope
public class StorageConfig {

	// 从nacos中读取存储服务类型（例如：minio、aliyun）
	@Value("${storage.service.type}")
	private String storageType;

	/**
	 * 根据配置动态选择并返回相应的存储服务实现。
	 * 如果配置为"minio"，则返回MinIO的实现；如果配置为"aliyun"，则返回阿里云的实现。
	 * 如果没有匹配的配置，则抛出IllegalArgumentException异常。
	 *
	 * @return StorageAdapter 的实现实例。
	 */
	@Bean
	@RefreshScope
	public StorageAdapter storageAdapter() {
		switch (storageType) {
			case "minio":
				return new MinioStorageAdapter();
			case "aliyun":
				// 使用注入的阿里云OSS配置来实例化AliyunStorageAdapter
				return new AliyunStorageAdapter();
			default:
				throw new IllegalArgumentException("未找到对应的文件存储处理器！");
		}
	}
}
