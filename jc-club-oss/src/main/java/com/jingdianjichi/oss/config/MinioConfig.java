package com.jingdianjichi.oss.config;

import io.minio.MinioClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Minio配置管理工具
 * 这个类用于配置Minio的客户端，以便在Spring Boot项目中使用Minio服务。
 * 主要配置包括服务的URL、访问密钥和密钥。
 *
 * @author: WuYimin
 * Date: 2024-02-07
 */
@Configuration
public class MinioConfig {

	/**
	 * Minio服务的URL
	 */
	@Value("${minio.url}")
	private String url;

	/**
	 * Minio账户的访问密钥
	 */
	@Value("${minio.accessKey}")
	private String accessKey;

	/**
	 * Minio账户的访问密钥对应的密钥
	 */
	@Value("${minio.secretKey}")
	private String secretKey;

	/**
	 * 构造MinioClient的Bean
	 * 该方法配置了Minio的客户端，用于与Minio服务进行交互。
	 *
	 * @return MinioClient的实例，配置了endpoint（服务URL）和credentials（访问密钥和密钥）。
	 */
	@Bean
	public MinioClient getMinioClient() {
		return MinioClient.builder()
				.endpoint(url) // 设置Minio服务的URL
				.credentials(accessKey, secretKey) // 设置访问密钥和密钥
				.build(); // 构建MinioClient实例
	}
}
