package com.jingdianjichi.club.gateway.auth;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.reactor.filter.SaReactorFilter;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 权限认证配置器
 * @author wuyimin
 */
@Configuration
public class SaTokenConfigure {

    /**
     * 注册 Sa-Token全局过滤器
      * @return
     */
    @Bean
    public SaReactorFilter getSaReactorFilter() {
        return new SaReactorFilter()
                .addInclude("/**") // 拦截全部path
                .addExclude("/auth/user/getUserInfo")
                .addExclude("/auth/user/doLogin")
                .addExclude("/auth/user/isLogin")
                //.addExclude("/oss/**")
                .setAuth(obj -> {
                    System.out.println("-------- Sa-Token校验path：" + SaHolder.getRequest().getRequestPath());

                    //这行代码的作用是对于路径匹配/auth/**的所有请求，都需要进行角色检查，确保当前用户具有admin角色
                    // 除了/auth/user/doLogin这个特定的路径。
                        //SaRouter.match("/auth/**", "/auth/user/doLogin", r -> StpUtil.checkRole("normal"));

                    // 其他鉴权配置
                    SaRouter.match("/oss/**", r -> StpUtil.checkLogin());
                    SaRouter.match("/subject/subject/add", r -> StpUtil.checkPermission("subject:add"));
                    SaRouter.match("/subject/**", r -> StpUtil.checkLogin());
                    SaRouter.match("/auth/**", r -> StpUtil.checkLogin());
                    System.out.println("-------- Sa-Token校验path：" + SaHolder.getRequest().getRequestPath()+"  成功");
                });
    }
}
