package com.jingdianjichi.club.gateway.filter;

import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 全局过滤器
 * 作用就是从当前会话的token中取出loginId存放在上下文对象中，以后后续在这个请求中用到可以直接从上下文对象中取出来
 *
 * @author: WuYimin
 * Date: 2024-02-13
 */
@Component
@Slf4j
public class LoginFilter implements GlobalFilter {

	// Sa-Token全局过滤器 优于 网关层的全局过滤器执行，所有到这里要过滤的请求已经没有网关的前缀了
	@SneakyThrows
	@Override
	public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
		// 获取当前请求对象
		ServerHttpRequest request = exchange.getRequest();
		// 创建一个可修改的请求构造器，用于修改请求
		ServerHttpRequest.Builder mutate = request.mutate();
		// 获取请求的URI路径
		String url = request.getURI().getPath();
		// 记录日志，输出当前过滤的URL
		log.info("LoginFilter.filter.url:{}", url);
		// 如果当前请求路径为登录接口或者获取用户信息的接口，则直接放行，不进行检查
		if (url.equals("/user/doLogin") || url.equals("/user/getUserInfo") || url.equals("/user/isLogin")
				|| url.equals("/subject/category/queryPrimaryCategory") ||
				url.equals("/subject/category/queryCategoryAndLabel") ||
				url.equals("/subject/getSubjectPage")) {
			return chain.filter(exchange);
		}
		// 使用SaToken框架获取当前会话的Token信息
		SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
		// 从Token信息中获取登录ID
		String loginId = (String) tokenInfo.getLoginId();
		// 检查登录ID是否为空，如果为空，则抛出异常，表示未获取到用户信息
		if (StringUtils.isEmpty(loginId)) {
			throw new Exception("未获取到用户信息！");
		}
		// 如果用户已登录，将登录ID添加到请求头中（添加的还是原来的http请求里面）
		mutate.header("loginId", loginId);
		// 修改当前请求，加入修改后的请求头，然后继续过滤链的执行
		return chain.filter(exchange.mutate().request(mutate.build()).build());
	}

}
