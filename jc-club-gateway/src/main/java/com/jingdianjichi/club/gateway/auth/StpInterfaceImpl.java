package com.jingdianjichi.club.gateway.auth;

import cn.dev33.satoken.stp.StpInterface;
import com.alibaba.cloud.commons.lang.StringUtils;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.jingdianjichi.club.gateway.entity.AuthPermission;
import com.jingdianjichi.club.gateway.entity.AuthRole;
import com.jingdianjichi.club.gateway.redis.RedisUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 自定义权限验证接口扩展
 */
@Component
public class StpInterfaceImpl implements StpInterface {

	// 注入自定义的Redis工具类，用于操作Redis数据
	@Resource
	private RedisUtil redisUtil;

	// 定义存储权限信息的Redis键前缀
	private String authPermissionPrefix = "auth.permission";

	// 定义存储角色信息的Redis键前缀
	private String authRolePrefix = "auth.role";

	// 根据登录ID和登录类型获取用户的权限列表
	@Override
	public List<String> getPermissionList(Object loginId, String loginType) {
		// 调用getAuth方法查询权限信息
		return getAuth(loginId.toString(), authPermissionPrefix);
	}

	// 根据登录ID和登录类型获取用户的角色
	@Override
	public List<String> getRoleList(Object loginId, String loginType) {
		// 调用getAuth方法查询角色信息
		return getAuth(loginId.toString(), authRolePrefix);
	}

	/**
	 * 权限认证
	 * 根据登录ID和前缀获取权限或角色信息的通用方法
	 * @param loginId
	 * @param prefix
	 * @return
	 */
	//
	private List<String> getAuth(String loginId, String prefix) {
		// 构建Redis键
		String authKey = redisUtil.buildKey(prefix, loginId);
		// 从Redis获取对应的值
		String authValue = redisUtil.get(authKey);
		// 如果值为空，则返回空列表
		if (StringUtils.isBlank(authValue)) {
			return Collections.emptyList();
		}
		List<String> authList = new LinkedList<>();
		// 如果是查询角色
		if (authRolePrefix.equals(prefix)) {
			// 将JSON字符串转换为角色对象
			List<AuthRole> roleList = new Gson().fromJson(authValue, new TypeToken<List<AuthRole>>() {}.getType());
			// 从角色对象列表中提取角色键，并收集到列表中
			authList = roleList.stream().map(AuthRole::getRoleKey).collect(Collectors.toList());
		} else if (authPermissionPrefix.equals(prefix)) { // 如果是查询权限
			// 将JSON字符串转换为权限对象
			List<AuthPermission> authPermissionList = new Gson()
					.fromJson(authValue, new TypeToken<List<AuthPermission>>() {}.getType());
			// 从权限对象列表中提取权限键，并收集到列表中
			authList = authPermissionList.stream().map(AuthPermission::getPermissionKey).collect(Collectors.toList());
		}
		return authList;
	}
}
