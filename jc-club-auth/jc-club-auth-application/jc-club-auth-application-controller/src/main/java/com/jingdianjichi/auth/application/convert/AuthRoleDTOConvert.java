package com.jingdianjichi.auth.application.convert;


import com.jingdianjichi.auth.application.dto.AuthRoleDTO;
import com.jingdianjichi.auth.application.dto.AuthUserDTO;
import com.jingdianjichi.auth.domain.entity.AuthRoleBO;
import com.jingdianjichi.auth.domain.entity.AuthUserBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现DTO（数据传输对象）与BO（业务对象）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface AuthRoleDTOConvert {

    // 通过MapStruct的Mappers工厂类获取AuthRoleDTOConvert的实例
    AuthRoleDTOConvert INSTANCE = Mappers.getMapper(AuthRoleDTOConvert.class);

    List<AuthRoleBO> convertDTOToBOList(List<AuthRoleDTO> authRoleDTOList);

    AuthRoleBO convertDtoToBo(AuthRoleDTO authRoleDTO);

    AuthRoleDTO convertBOToDTO(AuthRoleBO authRoleBO);

}