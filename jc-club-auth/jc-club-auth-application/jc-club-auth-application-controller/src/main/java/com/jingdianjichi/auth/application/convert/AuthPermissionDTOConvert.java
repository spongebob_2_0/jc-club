package com.jingdianjichi.auth.application.convert;


import com.jingdianjichi.auth.application.dto.AuthPermissionDTO;
import com.jingdianjichi.auth.domain.entity.AuthPermissionBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现DTO（数据传输对象）与BO（业务对象）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface AuthPermissionDTOConvert {

    // 通过MapStruct的Mappers工厂类获取AuthPermissionDTOConvert的实例
    AuthPermissionDTOConvert INSTANCE = Mappers.getMapper(AuthPermissionDTOConvert.class);

    List<AuthPermissionBO> convertDTOToBOList(List<AuthPermissionDTO> authPermissionDTOList);

    AuthPermissionBO convertDtoToBo(AuthPermissionDTO authRoleDTO);

    AuthPermissionDTO convertBOToDTO(AuthPermissionBO authPermissionBO);

}