package com.jingdianjichi.auth.application.convert;


import com.jingdianjichi.auth.entity.AuthUserDTO;
import com.jingdianjichi.auth.domain.entity.AuthUserBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现DTO（数据传输对象）与BO（业务对象）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface AuthUserDTOConvert {

    // 通过MapStruct的Mappers工厂类获取SubjectAnswerDTOConvert的实例
    AuthUserDTOConvert INSTANCE = Mappers.getMapper(AuthUserDTOConvert.class);

    // 定义一个方法，将 AuthUserDTO 列表转换为 AuthUserBO 表
    List<AuthUserBO> convertDTOToBOList(List<AuthUserDTO> authUserDTOList);

    // 定义一个方法，将 AuthUserDTO 转换为 AuthUserBO
    AuthUserBO convertDtoToBo(AuthUserDTO authUserDTO);

    AuthUserDTO convertBOToDTO(AuthUserBO authUserBO);

}