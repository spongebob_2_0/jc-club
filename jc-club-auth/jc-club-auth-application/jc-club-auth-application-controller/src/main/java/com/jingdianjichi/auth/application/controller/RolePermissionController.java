package com.jingdianjichi.auth.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.auth.application.convert.AuthRolePermissionDTOConvert;
import com.jingdianjichi.auth.application.dto.AuthRolePermissionDTO;
import com.jingdianjichi.auth.entity.Result;
import com.jingdianjichi.auth.domain.entity.AuthRolePermissionBO;
import com.jingdianjichi.auth.domain.service.AuthRolePermissionDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 角色controller
 */
@RestController
@RequestMapping("/rolePermission/")
@Slf4j
public class RolePermissionController {

    @Resource
    private AuthRolePermissionDomainService authRolePermissionDomainService;

    /**
     * 新增角色权限
     * @param authRolePermissionDTO
     * @return
     */
    @RequestMapping("add")
    public Result<Boolean> add(@RequestBody AuthRolePermissionDTO authRolePermissionDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("RolePermissionController.add.dto:{}", JSON.toJSONString(authRolePermissionDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(authRolePermissionDTO.getRoleId(), "角色id不能为空！");
            Preconditions.checkArgument(!CollectionUtils.isEmpty(authRolePermissionDTO.getPermissionIdList()),
                    "权限关联不能为空！");

            // 转换DTO为BO
            AuthRolePermissionBO authRolePermissionBO = AuthRolePermissionDTOConvert.INSTANCE.convertDtoToBo(authRolePermissionDTO);
            // 调用领域服务实新增服务
            Boolean result = authRolePermissionDomainService.add(authRolePermissionBO);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("RolePermissionController.add.error：{}", e.getMessage(), e);
            return Result.fail("新增角色权限失败！");
        }
    }





}
