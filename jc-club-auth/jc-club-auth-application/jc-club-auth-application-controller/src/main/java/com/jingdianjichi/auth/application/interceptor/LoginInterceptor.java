package com.jingdianjichi.auth.application.interceptor;

import com.jingdianjichi.auth.context.LoginContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * auth服务的登录拦截器
 *
 * @author: WuYimin
 * Date: 2024-02-14
 */
public class LoginInterceptor implements HandlerInterceptor {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		String loginId = request.getHeader("loginId");
		LoginContextHolder.set("loginId",loginId);
		return true;
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		LoginContextHolder.remove();
	}
}
