package com.jingdianjichi.auth.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.auth.application.convert.AuthRoleDTOConvert;
import com.jingdianjichi.auth.application.dto.AuthRoleDTO;
import com.jingdianjichi.auth.entity.Result;
import com.jingdianjichi.auth.domain.entity.AuthRoleBO;
import com.jingdianjichi.auth.domain.service.AuthRoleDomainService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 角色controller
 */
@RestController
@RequestMapping("/role/")
@Slf4j
public class RoleController {

    @Resource
    private AuthRoleDomainService authRoleDomainService;

    /**
     * 新增角色
     * @param authRoleDTO
     * @return
     */
    @RequestMapping("add")
    public Result<Boolean> add(@RequestBody AuthRoleDTO authRoleDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("RoleController.add.dto:{}", JSON.toJSONString(authRoleDTO));
            }
            // 校验输入参数
            Preconditions.checkArgument(StringUtils.isNotBlank(authRoleDTO.getRoleName()), "角色名称不能为空！");
            Preconditions.checkArgument(StringUtils.isNotBlank(authRoleDTO.getRoleKey()), "角色key不能为空！");

            // 转换DTO为BO
            AuthRoleBO authRoleBO = AuthRoleDTOConvert.INSTANCE.convertDtoToBo(authRoleDTO);
            // 调用领域服务实现注册服务
            Boolean result = authRoleDomainService.add(authRoleBO);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("RoleController.add.error：{}", e.getMessage(), e);
            return Result.fail("新增角色失败！");
        }
    }

    /**
	 * 更新用户信息
     * @param authRoleDTO
	 * @return
     */
    @RequestMapping("update")
    public Result<Boolean> update(@RequestBody AuthRoleDTO authRoleDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("RoleController.update.dto:{}", JSON.toJSONString(authRoleDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(authRoleDTO.getId() , "用户ID不能为空！");


            // 转换DTO为BO
            AuthRoleBO authRoleBO = AuthRoleDTOConvert.INSTANCE.convertDtoToBo(authRoleDTO);
            // 调用领域服务实现注册服务
            Boolean result = authRoleDomainService.update(authRoleBO);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("RoleController.update.error：{}", e.getMessage(), e);
            return Result.fail("更新用户信息失败！");
        }
    }

    /**
	 * 删除用户信息
     * @param authRoleDTO
	 * @return
     */
    @RequestMapping("delete")
    public Result<Boolean> delete(@RequestBody AuthRoleDTO authRoleDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("RoleController.delete.dto:{}", JSON.toJSONString(authRoleDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(authRoleDTO.getId() , "用户ID不能为空！");

            // 转换DTO为BO
            AuthRoleBO authRoleBO = AuthRoleDTOConvert.INSTANCE.convertDtoToBo(authRoleDTO);
            // 调用领域服务实现删除服务
            Boolean result = authRoleDomainService.delete(authRoleBO);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("RoleController.delete.error：{}", e.getMessage(), e);
            return Result.fail("删除用户信息失败！");
        }
    }


}
