package com.jingdianjichi.auth.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.auth.application.convert.AuthPermissionDTOConvert;
import com.jingdianjichi.auth.application.dto.AuthPermissionDTO;
import com.jingdianjichi.auth.domain.entity.AuthPermissionBO;
import com.jingdianjichi.auth.domain.service.AuthPermissionDomainService;
import com.jingdianjichi.auth.entity.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 角色controller
 */
@RestController
@RequestMapping("/permission/")
@Slf4j
public class PermissionController {

    @Resource
    private AuthPermissionDomainService permissionDomainService;

    /**
     * 新增权限
     * @param authPermissionDTO
     * @return
     */
    @RequestMapping("add")
    public Result<Boolean> add(@RequestBody AuthPermissionDTO authPermissionDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("PermissionController.add.dto:{}", JSON.toJSONString(authPermissionDTO));
            }
            // 校验输入参数
            Preconditions.checkArgument(StringUtils.isNotBlank(authPermissionDTO.getName()), "权限名称不能为空！");
            Preconditions.checkNotNull(authPermissionDTO.getParentId(),"权限父级id不能为空！");

            // 转换DTO为BO
            AuthPermissionBO authPermissionBO = AuthPermissionDTOConvert.INSTANCE.convertDtoToBo(authPermissionDTO);
            // 调用领域服务实新增服务
            Boolean result = permissionDomainService.add(authPermissionBO);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("PermissionController.add.error：{}", e.getMessage(), e);
            return Result.fail("新增角色失败！");
        }
    }

    /**
	 * 更新权限
     * @param authPermissionDTO
	 * @return
     */
    @RequestMapping("update")
    public Result<Boolean> update(@RequestBody AuthPermissionDTO authPermissionDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("PermissionController.update.dto:{}", JSON.toJSONString(authPermissionDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(authPermissionDTO.getId() , "权限ID不能为空！");


            // 转换DTO为BO
            AuthPermissionBO authPermissionBO = AuthPermissionDTOConvert.INSTANCE.convertDtoToBo(authPermissionDTO);
            // 调用领域服务实现注册服务
            Boolean result = permissionDomainService.update(authPermissionBO);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("PermissionController.update.error：{}", e.getMessage(), e);
            return Result.fail("更新权限信息失败！");
        }
    }

    /**
     * 删除权限
     *
     * @param authPermissionDTO
     * @return
     */
    @RequestMapping("delete")
    public Result delete(@RequestBody AuthPermissionDTO authPermissionDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("PermissionController.delete.dto:{}", JSON.toJSONString(authPermissionDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(authPermissionDTO.getId() , "权限ID不能为空！");


            // 转换DTO为BO
            AuthPermissionBO authPermissionBO = AuthPermissionDTOConvert.INSTANCE.convertDtoToBo(authPermissionDTO);
            // 调用领域服务实现删除服务
            Boolean result = permissionDomainService.delete(authPermissionBO);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("PermissionController.delete.error：{}", e.getMessage(), e);
            return Result.fail("删除用户信息失败！");
        }
    }

    /**
     * 启用/禁用权限
     * @param authPermissionDTO
     * @return
     */
    @RequestMapping("changeStatus")
    public Result<Boolean> changeStatus(@RequestBody AuthPermissionDTO authPermissionDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("PermissionController.changeStatus.dto:{}", JSON.toJSONString(authPermissionDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(authPermissionDTO.getId() , "权限ID不能为空！");


            // 转换DTO为BO
            AuthPermissionBO authPermissionBO = AuthPermissionDTOConvert.INSTANCE.convertDtoToBo(authPermissionDTO);
            // 调用领域服务实现删除服务
            Boolean result = permissionDomainService.changeStatus(authPermissionBO);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("PermissionController.changeStatus.error：{}", e.getMessage(), e);
            return Result.fail("删除用户信息失败！");
        }
    }


    /**
     * 查询用户权限
     */
    @RequestMapping("getPermission")
    public Result<Boolean> getPermission(String userName) {
        try {
            log.info("PermissionController.getPermission.userName:{}",userName);
            Preconditions.checkArgument(!StringUtils.isBlank(userName), "用户id不能为空");
            return Result.ok(permissionDomainService.getPermission(userName));
        } catch (Exception e) {
            log.error("PermissionController.getPermission.error:{}", e.getMessage(), e);
            return Result.fail("查询用户权限信息失败");
        }
    }

}
