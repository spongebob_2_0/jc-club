package com.jingdianjichi.basic.mapper;

import com.jingdianjichi.basic.entity.AuthPermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【auth_permission】的数据库操作Mapper
* @createDate 2024-02-09 08:49:18
* @Entity com.jingdianjichi.basic.entity.AuthPermission
*/
public interface AuthPermissionMapper extends BaseMapper<AuthPermission> {

	List<AuthPermission> listByICondition(@Param("permissionIdList") List<Long> permissionIdList);
}




