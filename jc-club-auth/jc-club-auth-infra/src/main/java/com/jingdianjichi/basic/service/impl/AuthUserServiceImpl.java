package com.jingdianjichi.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jingdianjichi.basic.entity.AuthUser;
import com.jingdianjichi.basic.service.AuthUserService;
import com.jingdianjichi.basic.mapper.AuthUserMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【auth_user】的数据库操作Service实现
* @createDate 2024-02-09 02:53:05
*/
@Service
public class AuthUserServiceImpl extends ServiceImpl<AuthUserMapper, AuthUser>
    implements AuthUserService{

}




