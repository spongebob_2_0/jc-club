package com.jingdianjichi.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jingdianjichi.basic.entity.AuthRole;
import com.jingdianjichi.basic.service.AuthRoleService;
import com.jingdianjichi.basic.mapper.AuthRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【auth_role】的数据库操作Service实现
* @createDate 2024-02-09 06:24:37
*/
@Service
public class AuthRoleServiceImpl extends ServiceImpl<AuthRoleMapper, AuthRole>
    implements AuthRoleService{

}




