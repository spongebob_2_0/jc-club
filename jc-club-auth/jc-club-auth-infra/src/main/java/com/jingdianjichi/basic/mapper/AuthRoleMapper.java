package com.jingdianjichi.basic.mapper;

import com.jingdianjichi.basic.entity.AuthRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author WuYimin
* @description 针对表【auth_role】的数据库操作Mapper
* @createDate 2024-02-09 06:24:37
* @Entity com.jingdianjichi.basic.entity.AuthRole
*/
public interface AuthRoleMapper extends BaseMapper<AuthRole> {

}




