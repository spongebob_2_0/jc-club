package com.jingdianjichi.basic.service;

import com.jingdianjichi.basic.entity.AuthUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author WuYimin
* @description 针对表【auth_user】的数据库操作Service
* @createDate 2024-02-09 02:53:05
*/
public interface AuthUserService extends IService<AuthUser> {

}
