package com.jingdianjichi.basic.mapper;

import com.jingdianjichi.basic.entity.AuthUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author WuYimin
* @description 针对表【auth_user】的数据库操作Mapper
* @createDate 2024-02-09 02:53:05
* @Entity com.jingdianjichi.basic.entity.AuthUser
*/
public interface AuthUserMapper extends BaseMapper<AuthUser> {

}




