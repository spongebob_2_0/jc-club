package com.jingdianjichi.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jingdianjichi.basic.entity.AuthPermission;
import com.jingdianjichi.basic.service.AuthPermissionService;
import com.jingdianjichi.basic.mapper.AuthPermissionMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author WuYimin
* @description 针对表【auth_permission】的数据库操作Service实现
* @createDate 2024-02-09 08:49:18
*/
@Service
public class AuthPermissionServiceImpl extends ServiceImpl<AuthPermissionMapper, AuthPermission>
    implements AuthPermissionService{

	@Resource
	private AuthPermissionMapper authPermissionMapper;

	@Override
	public List<AuthPermission> listByICondition(List<Long> permissionIdList) {

		return authPermissionMapper.listByICondition(permissionIdList);
	}
}




