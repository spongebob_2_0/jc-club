package com.jingdianjichi.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jingdianjichi.basic.entity.AuthRolePermission;
import com.jingdianjichi.basic.service.AuthRolePermissionService;
import com.jingdianjichi.basic.mapper.AuthRolePermissionMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【auth_role_permission(角色权限表)】的数据库操作Service实现
* @createDate 2024-02-09 11:49:51
*/
@Service
public class AuthRolePermissionServiceImpl extends ServiceImpl<AuthRolePermissionMapper, AuthRolePermission>
    implements AuthRolePermissionService{

}




