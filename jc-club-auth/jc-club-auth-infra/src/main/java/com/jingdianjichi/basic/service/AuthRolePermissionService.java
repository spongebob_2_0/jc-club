package com.jingdianjichi.basic.service;

import com.jingdianjichi.basic.entity.AuthRolePermission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author WuYimin
* @description 针对表【auth_role_permission(角色权限表)】的数据库操作Service
* @createDate 2024-02-09 11:49:51
*/
public interface AuthRolePermissionService extends IService<AuthRolePermission> {

}
