package com.jingdianjichi.basic.mapper;

import com.jingdianjichi.basic.entity.AuthRolePermission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author WuYimin
* @description 针对表【auth_role_permission(角色权限表)】的数据库操作Mapper
* @createDate 2024-02-09 11:49:51
* @Entity com.jingdianjichi.basic.entity.AuthRolePermission
*/
public interface AuthRolePermissionMapper extends BaseMapper<AuthRolePermission> {

}




