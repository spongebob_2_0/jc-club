package com.jingdianjichi.basic.service;

import com.jingdianjichi.basic.entity.AuthPermission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【auth_permission】的数据库操作Service
* @createDate 2024-02-09 08:49:18
*/
public interface AuthPermissionService extends IService<AuthPermission> {

	List<AuthPermission> listByICondition(List<Long> permissionIdList);
}
