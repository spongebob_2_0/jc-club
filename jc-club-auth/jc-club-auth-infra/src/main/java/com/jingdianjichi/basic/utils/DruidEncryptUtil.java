package com.jingdianjichi.basic.utils;

import com.alibaba.druid.filter.config.ConfigTools;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

/**
 * 数据库加密Util
 *
 * @author: WuYimin
 * Date: 2024-02-02
  */
public class DruidEncryptUtil {

    // 定义存储公钥的静态变量
    private static String publicKey;

    // 定义存储私钥的静态变量
    private static String privateKey;

    // 静态代码块，在类加载时执行一次
    static {
        try {
            // 生成512位的密钥对
            String[] keyPair = ConfigTools.genKeyPair(512);
            // 第一个元素是私钥
            privateKey = keyPair[0];
            // 打印私钥
            System.out.println("privateKey：" + privateKey);
            // 第二个元素是公钥
            publicKey = keyPair[1];
            // 打印公钥
            System.out.println("publicKey：" + publicKey);
        } catch (NoSuchAlgorithmException e) {
            // 捕获并抛出算法不存在异常
            throw new RuntimeException(e);
        } catch (NoSuchProviderException e) {
            // 捕获并抛出提供者不存在异常
            throw new RuntimeException(e);
        }
    }

    // 加密方法，输入明文，返回密文
    public static String encrypt(String plainText) throws Exception {
        // 使用私钥加密明文
        String encrypt = ConfigTools.encrypt(privateKey, plainText);
        // 打印加密后的密文
        System.out.println("encrypt：" + encrypt);
        // 返回加密后的密文
        return encrypt;
    }

    // 解密方法，输入密文，返回明文
    public static String decrypt(String encryptText) throws Exception {
        // 使用公钥和加密过的文本进行解密操作
        String decrypt = ConfigTools.decrypt(publicKey, encryptText);
        // 打印解密后的结果
        System.out.println("decrypt：" + decrypt);
        // 返回解密后的文本
        return decrypt;
    }


    // 主方法，用于测试加密功能
    public static void main(String[] args) throws Exception {
        // 加密字符串“183193”
        String encrypt = encrypt("183193");
        // 打印加密结果
        System.out.println("encrypt：" + encrypt);
    }
}
