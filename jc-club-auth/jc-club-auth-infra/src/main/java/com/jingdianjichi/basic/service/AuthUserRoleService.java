package com.jingdianjichi.basic.service;

import com.jingdianjichi.basic.entity.AuthUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author WuYimin
* @description 针对表【auth_user_role】的数据库操作Service
* @createDate 2024-02-09 07:42:44
*/
public interface AuthUserRoleService extends IService<AuthUserRole> {

}
