package com.jingdianjichi.basic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jingdianjichi.basic.entity.AuthUserRole;
import com.jingdianjichi.basic.service.AuthUserRoleService;
import com.jingdianjichi.basic.mapper.AuthUserRoleMapper;
import org.springframework.stereotype.Service;

/**
* @author WuYimin
* @description 针对表【auth_user_role】的数据库操作Service实现
* @createDate 2024-02-09 07:42:44
*/
@Service
public class AuthUserRoleServiceImpl extends ServiceImpl<AuthUserRoleMapper, AuthUserRole>
    implements AuthUserRoleService{

}




