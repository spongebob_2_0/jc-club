package com.jingdianjichi.basic.mapper;

import com.jingdianjichi.basic.entity.AuthUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author WuYimin
* @description 针对表【auth_user_role】的数据库操作Mapper
* @createDate 2024-02-09 07:42:44
* @Entity com.jingdianjichi.basic.entity.AuthUserRole
*/
public interface AuthUserRoleMapper extends BaseMapper<AuthUserRole> {

}




