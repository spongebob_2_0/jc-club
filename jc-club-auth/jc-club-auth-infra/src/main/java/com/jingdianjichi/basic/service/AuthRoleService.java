package com.jingdianjichi.basic.service;

import com.jingdianjichi.basic.entity.AuthRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author WuYimin
* @description 针对表【auth_role】的数据库操作Service
* @createDate 2024-02-09 06:24:37
*/
public interface AuthRoleService extends IService<AuthRole> {

}
