package com.jingdianjichi.auth.domain.convert;


import com.jingdianjichi.auth.domain.entity.AuthPermissionBO;
import com.jingdianjichi.basic.entity.AuthPermission;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现BO（业务对象）与 基础层实体类对象 之间的转换。
 *
 * @author: WuYimin
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface AuthPermissionBOConvert {

    // 通过MapStruct的Mappers工厂类获取 AuthPermissionBOConvert 的实例
    AuthPermissionBOConvert INSTANCE = Mappers.getMapper(AuthPermissionBOConvert.class);

    List<AuthPermission> convertBoToEntityList(List<AuthPermissionBO> authPermissionBOList);

    AuthPermission convertBoToEntity(AuthPermissionBO authPermissionBO);

    AuthPermissionBO convertToBO(AuthPermission authPermission);

}