package com.jingdianjichi.auth.domain.constants;

/**
 * 服务常量
 *
 * @author: WuYimin
 * Date: 2024-02-09
 */
public class AuthConstant {

	public static final String NORMAL_USER ="normal_user";

	public static final String ADMIN_USER = "admin_user";



}
