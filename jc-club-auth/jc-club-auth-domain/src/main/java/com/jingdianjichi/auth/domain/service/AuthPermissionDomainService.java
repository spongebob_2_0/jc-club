package com.jingdianjichi.auth.domain.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jingdianjichi.auth.domain.entity.AuthPermissionBO;
import com.jingdianjichi.basic.entity.AuthPermission;

import java.util.List;

/**
* @author WuYimin
* @description 针对表【auth_permission】的数据库操作Service
* @createDate 2024-02-09 08:49:18
*/
public interface AuthPermissionDomainService {

	/**
	 * 新增权限
	 * @param authPermissionBO
	 * @return
	 */
	Boolean add(AuthPermissionBO authPermissionBO);

	/**
	 * 更新权限
	 * @param authPermissionBO
	 * @return
	 */
	Boolean update(AuthPermissionBO authPermissionBO);

	/**
	 * 删除权限
	 * @param authPermissionBO
	 * @return
	 */
	Boolean delete(AuthPermissionBO authPermissionBO);

	/**
	 * 启用/禁用权限
	 * @param authPermissionBO
	 * @return
	 */
	Boolean changeStatus(AuthPermissionBO authPermissionBO);

	/**
	 * 查询用户权限
	 * @param userName
	 * @return
	 */
	List<String> getPermission(String userName);
}
