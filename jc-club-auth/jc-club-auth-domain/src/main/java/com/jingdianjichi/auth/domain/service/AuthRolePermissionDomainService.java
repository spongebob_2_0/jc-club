package com.jingdianjichi.auth.domain.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jingdianjichi.auth.domain.entity.AuthRolePermissionBO;
import com.jingdianjichi.basic.entity.AuthRolePermission;

/**
* @author WuYimin
* @description 针对表【auth_role_permission(角色权限表)】的数据库操作Service
* @createDate 2024-02-09 11:49:51
*/
public interface AuthRolePermissionDomainService {

	/**
	 * 新增角色权限
	 * @param authRolePermissionBO
	 * @return
	 */
	Boolean add(AuthRolePermissionBO authRolePermissionBO);
}
