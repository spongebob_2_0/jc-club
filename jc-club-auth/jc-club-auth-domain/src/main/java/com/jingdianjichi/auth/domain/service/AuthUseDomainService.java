package com.jingdianjichi.auth.domain.service;

import cn.dev33.satoken.stp.SaTokenInfo;
import com.jingdianjichi.auth.domain.entity.AuthRoleBO;
import com.jingdianjichi.auth.domain.entity.AuthUserBO;
import com.jingdianjichi.basic.entity.AuthUser;

/**
 * 用户领域层service
 */
public interface AuthUseDomainService  {

	/**
	 * 用户注册
	 * @param authUserBO
	 */
	Boolean register(AuthUserBO authUserBO);

	/**
	 * 更新用户信息
	 * @param authUserBO
	 * @return
	 */
	Boolean update(AuthUserBO authUserBO);

	/**
	 * 删除用户信息
	 * @param authUserBO
	 * @return
	 */
	Boolean delete(AuthUserBO authUserBO);

	/**
	 * 用户启用/禁用
	 * @param authUserBO
	 * @return
	 */
	Boolean changeStatus(AuthUserBO authUserBO);

	/**
	 * 个人信息查询
	 * @param authUserBO
	 * @return
	 */
	AuthUserBO getUserInfo(AuthUserBO authUserBO);

	/**
	 * 登录接口
	 * @param validCode
	 * @return
	 */
	SaTokenInfo doLogin(String validCode);
}
