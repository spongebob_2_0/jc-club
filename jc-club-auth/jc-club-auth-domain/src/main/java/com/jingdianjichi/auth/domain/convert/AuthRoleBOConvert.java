package com.jingdianjichi.auth.domain.convert;


import com.jingdianjichi.auth.domain.entity.AuthRoleBO;
import com.jingdianjichi.auth.domain.entity.AuthUserBO;
import com.jingdianjichi.basic.entity.AuthRole;
import com.jingdianjichi.basic.entity.AuthUser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现BO（业务对象）与 基础层实体类对象 之间的转换。
 *
 * @author: WuYimin
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface AuthRoleBOConvert {

    // 通过MapStruct的Mappers工厂类获取 AuthRoleBOConvert 的实例
    AuthRoleBOConvert INSTANCE = Mappers.getMapper(AuthRoleBOConvert.class);

    List<AuthRole> convertBoToEntityList(List<AuthRoleBO> authRoleBOList);

    AuthRole convertBoToEntity(AuthRoleBO authRoleBO);

    AuthUserBO convertToBO(AuthRole authRole);

}