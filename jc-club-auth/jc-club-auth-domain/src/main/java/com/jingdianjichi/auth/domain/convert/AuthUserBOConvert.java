package com.jingdianjichi.auth.domain.convert;


import com.jingdianjichi.auth.domain.entity.AuthUserBO;
import com.jingdianjichi.basic.entity.AuthUser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现BO（业务对象）与 基础层实体类对象 之间的转换。
 *
 * @author: WuYimin
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface AuthUserBOConvert {

    // 通过MapStruct的Mappers工厂类获取 AuthUserBOConvert 的实例
    AuthUserBOConvert INSTANCE = Mappers.getMapper(AuthUserBOConvert.class);

    // 定义一个方法，将 AuthUserBO 列表转换为 AuthUser 表
    List<AuthUser> convertBoToEntityList(List<AuthUserBO> authUserBOList);

    // 定义一个方法，将 AuthUserBO 转换为 AuthUser
    AuthUser convertBoToEntity(AuthUserBO AuthUserBO);

    AuthUserBO convertToBO(AuthUser AuthUser);

}