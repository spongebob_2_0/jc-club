package com.jingdianjichi.auth.domain.convert;


import com.jingdianjichi.auth.domain.entity.AuthRolePermissionBO;
import com.jingdianjichi.basic.entity.AuthRolePermission;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现BO（业务对象）与 基础层实体类对象 之间的转换。
 *
 * @author: WuYimin
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface AuthRolePermissionBOConvert {

    // 通过MapStruct的Mappers工厂类获取 AuthRolePermissionBOConvert 的实例
    AuthRolePermissionBOConvert INSTANCE = Mappers.getMapper(AuthRolePermissionBOConvert.class);

    List<AuthRolePermission> convertBoToEntityList(List<AuthRolePermissionBO> authRolePermissionBOList);

    AuthRolePermission convertBoToEntity(AuthRolePermissionBO authRolePermissionBO);

    AuthRolePermissionBO convertToBO(AuthRolePermission authRolePermission);

}