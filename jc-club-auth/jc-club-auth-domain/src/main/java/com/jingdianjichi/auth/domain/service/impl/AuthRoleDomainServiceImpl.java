package com.jingdianjichi.auth.domain.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.jingdianjichi.auth.common.enums.IsDeleteFlagEnum;
import com.jingdianjichi.auth.domain.convert.AuthRoleBOConvert;
import com.jingdianjichi.auth.domain.entity.AuthRoleBO;
import com.jingdianjichi.auth.domain.service.AuthRoleDomainService;
import com.jingdianjichi.basic.entity.AuthRole;
import com.jingdianjichi.basic.service.AuthRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AuthRoleDomainServiceImpl implements AuthRoleDomainService {

	@Resource
	private AuthRoleService authRoleService; // 注入AuthRoleService服务，用于操作auth_role表的数据库操作

	/**
	 * 新增角色
	 * @param authRoleBO 角色业务对象，包含了角色信息
	 * @return 新增结果，成功为true，失败为false
	 */
	@Override
	public Boolean add(AuthRoleBO authRoleBO) {
		// 将业务对象转换为数据库实体对象
		AuthRole authRole = AuthRoleBOConvert.INSTANCE.convertBoToEntity(authRoleBO);
		// 设置角色为未删除状态
		authRole.setIsDeleted(IsDeleteFlagEnum.UN_DELETED.getCode());
		// 保存角色信息到数据库
		boolean result = authRoleService.save(authRole);
		return result;
	}

	/**
	 * 更新角色
	 * @param authRoleBO 需要更新的角色业务对象
	 * @return 更新结果，成功为true，失败为false
	 */
	@Override
	public Boolean update(AuthRoleBO authRoleBO) {
		// 将业务对象转换为数据库实体对象
		AuthRole authRole = AuthRoleBOConvert.INSTANCE.convertBoToEntity(authRoleBO);
		// 创建LambdaUpdateWrapper来构造更新条件和更新值
		LambdaUpdateWrapper<AuthRole> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
		// 设置更新条件为角色ID，并更新角色名称和角色键
		lambdaUpdateWrapper.eq(AuthRole::getId, authRole.getId())
				.set(AuthRole::getRoleName, authRole.getRoleName())
				.set(AuthRole::getRoleKey, authRole.getRoleKey());
		// 执行更新操作
		boolean result = authRoleService.update(null, lambdaUpdateWrapper);
		return result;
	}

	/**
	 * 删除角色
	 * @param authRoleBO 需要删除的角色业务对象
	 * @return 删除结果，成功为true，失败为false
	 */
	@Override
	public Boolean delete(AuthRoleBO authRoleBO) {
		// 将业务对象转换为数据库实体对象
		AuthRole authRole = AuthRoleBOConvert.INSTANCE.convertBoToEntity(authRoleBO);
		// 创建LambdaUpdateWrapper来构造更新条件和更新值
		LambdaUpdateWrapper<AuthRole> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
		// 设置更新条件为角色ID，并将角色标记为已删除
		lambdaUpdateWrapper.eq(AuthRole::getId, authRole.getId())
				.set(AuthRole::getIsDeleted, IsDeleteFlagEnum.DELETED.getCode());
		// 执行更新操作，实际上是逻辑删除
		boolean result = authRoleService.update(null, lambdaUpdateWrapper);
		return result;
	}
}


