package com.jingdianjichi.auth.domain.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jingdianjichi.auth.domain.entity.AuthRoleBO;
import com.jingdianjichi.basic.entity.AuthRole;

/**
* @author WuYimin
* @description 针对表【auth_role】的数据库操作Service
* @createDate 2024-02-09 06:24:37
*/
public interface AuthRoleDomainService {

	/**
	 * 新增角色
	 * @param authRoleBO
	 * @return
	 */
	Boolean add(AuthRoleBO authRoleBO);

	/**
	 * 更新角色
	 * @param authRoleBO
	 * @return
	 */
	Boolean update(AuthRoleBO authRoleBO);

	/**
	 * 删除角色
	 * @param authRoleBO
	 * @return
	 */
	Boolean delete(AuthRoleBO authRoleBO);
}
