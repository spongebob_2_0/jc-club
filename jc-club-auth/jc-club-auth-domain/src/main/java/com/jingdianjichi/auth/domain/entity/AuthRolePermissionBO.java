package com.jingdianjichi.auth.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 角色权限表
 * @TableName auth_role_permission
 */
@Data
public class AuthRolePermissionBO implements Serializable {
    /**
     * 
     */
    private Long id;

    /**
     * 角色id

     */
    private Long roleId;

    /**
     * 权限id
     */
    private Long permissionId;

    private List<Long> permissionIdList;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private String updateBy;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 0是未删除，1是删除状态
     */
    private Integer isDeleted;

}