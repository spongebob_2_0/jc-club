package com.jingdianjichi.auth.domain.util;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.UUID;

public class PasswordTools {

    /**
     * 生成盐值加密
     * @param password 明文密码
     * @return 加盐加密的密码
     */
    public static String encrypt(String password) {
        //1.产生盐值
        String salt = UUID.randomUUID().toString().replace("-","");
        //2.使用(盐值+明文密码) 得到加密的密码
        String finalPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        //3.将盐值和加密的密码共同返回(合并盐值和加密码)
        String dbPassword = salt + "$" + finalPassword;
        return dbPassword;
    }

    /**
     * 用已有盐值加密
     * @param password 密明文密码
     * @param salt  盐值
     * @return      加盐加密的密码
     */
    public static String encrypt(String password, String salt) {
        //1.使用(盐值+明文密码) 得到加密的密码
        String finalPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes());
        //2.将盐值和加密的密码共同返回(合并盐值和加密码)
        String dbPassword = salt + "$" + finalPassword;
        return dbPassword;
    }


    /**
     * 验证加盐加密密码
     * @param password  明文密码(不一定对, 需要验证明文密码)
     * @param dbPassword 数据库存储的密码(包含: salt+$+加盐加密密码)
     * @return
     */
    public static boolean decrypt(String password, String dbPassword) {
        boolean result = false;
        if(StringUtils.hasLength(password) && StringUtils.hasLength(dbPassword)
                && dbPassword.length() == 65 && dbPassword.contains("$")) {  //参数正确
            //1. 得到盐值
            String[] passwordArr = dbPassword.split("\\$");
            //1.1 盐值
            String salt = passwordArr[0];
            //1.2 得到正确密码的加盐加密密码
            String finalPassword = passwordArr[1];
            //2. 生成验证密码的加盐加密密码
            String checkPassword = encrypt(password, salt);
            if(dbPassword.equals(checkPassword)) {
                result = true;
            }
        }
        return  result;
    }
}