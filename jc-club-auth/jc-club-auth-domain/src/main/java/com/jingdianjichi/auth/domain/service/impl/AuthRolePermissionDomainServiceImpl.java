package com.jingdianjichi.auth.domain.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jingdianjichi.auth.common.enums.IsDeleteFlagEnum;
import com.jingdianjichi.auth.domain.convert.AuthRolePermissionBOConvert;
import com.jingdianjichi.auth.domain.entity.AuthRolePermissionBO;
import com.jingdianjichi.auth.domain.service.AuthRolePermissionDomainService;
import com.jingdianjichi.basic.entity.AuthRolePermission;
import com.jingdianjichi.basic.mapper.AuthRolePermissionMapper;
import com.jingdianjichi.basic.service.AuthRolePermissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.security.Permission;
import java.util.LinkedList;
import java.util.List;

/**
 * 角色权限关联操作的业务逻辑实现类
 *
 * 主要负责针对角色权限表（auth_role_permission）的数据库操作
 * @author WuYimin
 * @description 针对表【auth_role_permission(角色权限表)】的数据库操作Service实现
 * @createDate 2024-02-09 11:49:51
 */
@Service
public class AuthRolePermissionDomainServiceImpl implements AuthRolePermissionDomainService {

	@Resource
	private AuthRolePermissionService authRolePermissionService; // 注入角色权限服务接口

	/**
	 * 添加角色权限关联信息
	 *
	 * 根据角色业务对象中的权限ID列表，为指定角色批量添加权限关联
	 * @param authRolePermissionBO 角色权限业务对象，包含角色ID和权限ID列表
	 * @return 操作是否成功的布尔值
	 */
	@Override
	public Boolean add(AuthRolePermissionBO authRolePermissionBO) {
		List<AuthRolePermission> list = new LinkedList<>(); // 创建角色权限关联信息列表
		// 遍历权限ID列表，为每个权限ID创建一个角色权限关联实体，并设置角色ID和权限ID
		authRolePermissionBO.getPermissionIdList().forEach(permissionId -> {
			AuthRolePermission authRolePermission = new AuthRolePermission(); // 创建角色权限关联实体
			authRolePermission.setPermissionId(permissionId); // 设置权限ID
			authRolePermission.setRoleId(authRolePermissionBO.getRoleId()); // 设置角色ID
			authRolePermission.setIsDeleted(IsDeleteFlagEnum.UN_DELETED.getCode());
			list.add(authRolePermission); // 将角色权限关联实体添加到列表中
		});
		// 调用角色权限服务接口的批量保存方法，将角色权限关联信息列表保存到数据库中
		boolean result = authRolePermissionService.saveBatch(list);
		return result; // 返回操作结果
	}
}
