package com.jingdianjichi.auth.common.enums;

import lombok.Getter;

/**
 * 删除状态枚举
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Getter
public enum AuthUserStatusEnum {

    OPEN(1,"禁用"),
    CLOSE(0,"启用");

    private int code;

    private String desc;

    AuthUserStatusEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static AuthUserStatusEnum getByCode(int codeVal) {
        for(AuthUserStatusEnum authUserStatusEnum : AuthUserStatusEnum.values()) {
            if(authUserStatusEnum.code == codeVal) {
                return authUserStatusEnum;
            }
        }
        return null;
    }

}
