package com.jingdianjichi.wx.handler;

import java.util.Map;

/**
 * 微信聊天消息处理器接口
 * 用于定义处理不同类型的微信聊天消息的处理器
 *
 * @author: WuYimin
 * @date: 2024-02-10
 */
public interface WxChatMsgHandler {

	/**
	 * 获取消息类型
	 * 用于标识该处理器能处理哪种类型的消息
	 *
	 * @return 消息类型枚举
	 */
	WxChatMsgTypeEnum getMsgType();

	/**
	 * 处理消息
	 * 根据传入的消息内容进行相应的处理逻辑
	 *
	 * @param messageMap 消息内容，以Map的形式传入，包含了消息的各种属性和值
	 * @return 处理结果，返回值为处理后需要回复给用户的消息内容
	 */
	String dealMsg(Map<String, String> messageMap);
}
