package com.jingdianjichi.wx.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 微信消息处理器工厂类
 * 用于管理和获取不同类型微信消息的处理器
 *
 * @author: WuYimin
 * @date: 2024-02-10
 */
@Component
@Slf4j
public class WxChatMsgFactory implements InitializingBean {

	@Resource
	private List<WxChatMsgHandler> wxChatMsgHandlerList; // 注入所有的消息处理器

	private Map<WxChatMsgTypeEnum, WxChatMsgHandler> handlerMap = new HashMap<>(); // 存储消息类型到处理器的映射

	/**
	 * 根据消息类型获取对应的处理器
	 *
	 * @param msgType 消息类型
	 * @return 对应的消息处理器
	 */
	public WxChatMsgHandler getHandlerByMsgType(String msgType) {
		WxChatMsgTypeEnum msgTypeEnum = WxChatMsgTypeEnum.getByMsgType(msgType); // 将字符串消息类型转换为枚举
		return handlerMap.get(msgTypeEnum); // 从映射中获取对应的事件处理器
	}

	/**
	 * 在所有属性设置完成后，初始化消息处理器映射
	 * 此方法会在所有的bean属性被初始化后调用
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		for (WxChatMsgHandler wxChatMsgHandler : wxChatMsgHandlerList) {
			handlerMap.put(wxChatMsgHandler.getMsgType(), wxChatMsgHandler); // 将处理器存入映射
		}
	}
}
