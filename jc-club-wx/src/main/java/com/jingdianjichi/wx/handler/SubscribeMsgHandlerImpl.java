package com.jingdianjichi.wx.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 微信公众号关注事件处理器实现
 * 用于处理用户关注公众号后的消息回复等逻辑
 *
 * @author: WuYimin
 * @date: 2024-02-10
 */
@Component
@Slf4j
public class SubscribeMsgHandlerImpl implements WxChatMsgHandler {

	/**
	 * 获取处理的消息类型
	 * @return 消息类型枚举，这里是关注事件
	 */
	@Override
	public WxChatMsgTypeEnum getMsgType() {
		return WxChatMsgTypeEnum.SUBSCRIBE;
	}

	/**
	 * 处理关注事件的业务逻辑
	 * @param messageMap 消息内容映射，包含了微信发送过来的消息数据
	 * @return 响应给用户的消息内容，这里是一段感谢关注的文本消息
	 */
	@Override
	public String dealMsg(Map<String, String> messageMap) {
		log.info("触发用户关注事件！");
		// 用户的微信号
		String fromUserName = messageMap.get("FromUserName");
		// 公众号的微信号
		String toUserName = messageMap.get("ToUserName");
		// 关注时回复给用户的感谢消息内容
		String subscribeContent = "感谢您的关注，我是温柔书生！欢迎来加入我的公众号一起学习编程！";
		// 构造返回的消息xml格式字符串
		String content = "<xml>\n" +
				"  <ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>\n" +
				"  <FromUserName><![CDATA[" + toUserName + "]]></FromUserName>\n" +
				"  <CreateTime>12345678</CreateTime>\n" +
				"  <MsgType><![CDATA[text]]></MsgType>\n" +
				"  <Content><![CDATA[" + subscribeContent + "]]></Content>\n" +
				"</xml>";
		return content;
	}
}
