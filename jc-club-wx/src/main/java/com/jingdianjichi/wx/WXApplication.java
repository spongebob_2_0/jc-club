package com.jingdianjichi.wx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * 微信微服务启动类
 *
 * @author: WuYimin
 * Date: 2024-02-02
  */
@SpringBootApplication
@ComponentScan("com.jingdianjichi")
public class WXApplication {
    public static void main(String[] args) {
        SpringApplication.run(WXApplication.class);
    }
}
