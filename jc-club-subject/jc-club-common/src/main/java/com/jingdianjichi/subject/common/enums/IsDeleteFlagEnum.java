package com.jingdianjichi.subject.common.enums;

import lombok.Getter;

/**
 * 删除状态枚举
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Getter
public enum IsDeleteFlagEnum {

    DELETED(1,"已删除"),
    UN_DELETED(0,"未删除");

    private int code;

    private String desc;

    IsDeleteFlagEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static IsDeleteFlagEnum getByCode(int codeVal) {
        for(IsDeleteFlagEnum resultCodeEnum : IsDeleteFlagEnum.values()) {
            if(resultCodeEnum.code == codeVal) {
                return resultCodeEnum;
            }
        }
        return null;
    }

}
