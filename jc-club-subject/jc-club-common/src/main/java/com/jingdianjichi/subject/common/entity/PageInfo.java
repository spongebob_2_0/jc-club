package com.jingdianjichi.subject.common.entity;

import lombok.Data;

/**
 * 分页请求实体类
 * 用于处理分页请求，包括页码（pageNo）和每页显示的条目数量（pageSize）。
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Data
public class PageInfo {

	// 页码，默认为1，表示默认从第一页开始
	private Integer pageNo = 1;

	// 每页显示的条目数量，默认为20
	private Integer pageSize = 20;

	/**
	 * 获取当前页码
	 * 如果提供的页码为null或小于1，则默认返回第一页。
	 *
	 * @return 当前页码
	 */
	public Integer getPageNo() {
		if(pageNo == null || pageNo < 1) {
			return 1;
		}
		return pageNo;
	}

	/**
	 * 获取每页显示的条目数量
	 * 如果提供的数量为null、小于1或大于Integer的最大值，则默认返回20。
	 *
	 * @return 每页的条目数量
	 */
	public Integer getPageSize() {
		if(pageSize == null || pageSize < 1 || pageSize > Integer.MAX_VALUE) {
			return  20;
		}
		return pageSize;
	}
}
