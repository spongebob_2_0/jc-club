package com.jingdianjichi.subject.common.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

/**
 * 分页返回的实体类
 * 用于封装分页查询结果，包括页码、每页大小、总条目数、总页数以及当前页的数据集合。
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Data
public class PageResult<T> implements Serializable {

	private Integer pageNo = 1; // 当前页码，默认为第一页
	private Integer pageSize = 20; // 每页条目数，默认为20
	private Integer total = 0; // 总条目数，默认为0
	private Integer totalPages = 0; // 总页数，默认为0
	private List<T> result = Collections.emptyList(); // 当前页的数据集合，默认为空集合
	private Integer start = 1; // 当前页的起始条目序号，默认为1
	private Integer end = 0; // 当前页的结束条目序号，默认为0

	/**
	 * 设置当前页的数据集合，并自动更新总条目数和总页数等信息。
	 *
	 * @param result 当前页的数据集合
	 */
	public void setRecords(List<T> result) {
		this.result = result;
		if (result != null && !result.isEmpty()) {
			setTotal(result.size());
		}
	}

	/**
	 * 设置总条目数，并基于此更新总页数、起始和结束条目的序号。
	 *
	 * @param total 总条目数
	 */
	public void setTotal(Integer total) {
		this.total = total; // 设置总条目数

		// 判断每页条目数是否大于0，以避免除以0的错误
		if (this.pageSize > 0) {
			// 计算总页数。如果总条目数能够被每页条目数整除，则总页数正好是这个商；
			// 如果不能整除，则总页数是商加1，因为最后一页将包含少于pageSize的条目。
			this.totalPages = (total / this.pageSize) + (total % this.pageSize == 0 ? 0 : 1);
		} else {
			// 如果每页条目数没有设置（或设置不正确），则总页数为0。
			this.totalPages = 0;
		}

		// 计算当前页的起始条目序号。如果页码大于0，则起始序号为 (页码 - 1) * 每页条目数 + 1；
		// 这样做是因为分页是从第1页开始的，而不是从第0页。
		// 如果页码小于1（理论上不应该发生），则将起始条目序号设为1。
		this.start = (this.pageNo > 0 ? (this.pageNo - 1) * this.pageSize : 0) + 1;

		// 计算当前页的结束条目序号。这是通过起始序号加上本页应有的条目数（pageSize）再减去1得到的。
		// 注意，如果页码不大于0（理论上不应该发生），则乘以0，确保结束序号不会错误地计算。
		this.end = this.start - 1 + this.pageSize * (this.pageNo > 0 ? 1 : 0);
	}


	/**
	 * 设置每页条目数。
	 *
	 * @param pageSize 每页的条目数量
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	/**
	 * 设置页码。
	 *
	 * @param pageNo 页码
	 */
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
}
