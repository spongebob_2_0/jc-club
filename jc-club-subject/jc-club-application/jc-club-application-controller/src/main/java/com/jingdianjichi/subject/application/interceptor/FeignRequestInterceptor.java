package com.jingdianjichi.subject.application.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * Feign请求拦截器
 *
 * @author: WuYimin
 * Date: 2024-02-16
 */
@Component
public class FeignRequestInterceptor implements RequestInterceptor {

	// 实现RequestInterceptor接口中的apply方法，这个方法会在发送Feign请求之前被调用
	@Override
	public void apply(RequestTemplate requestTemplate) {
		// 从RequestContextHolder中获取当前请求的属性，这是一个线程局部变量，保证了请求的隔离
		ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		// 从请求属性中获取HttpServletRequest对象
		HttpServletRequest request = requestAttributes.getRequest();
		// 判断当前是否有请求存在，避免在异步或非HTTP请求环境下出现空指针异常
		if(Objects.nonNull(request)) {
			// 从当前HTTP请求中获取名为"loginId"的请求头
			String loginId = request.getHeader("loginId");
			// 判断获取到的loginId是否为空或者空白字符，如果不是，则将其添加到Feign请求的头部中
			if(StringUtils.isNotBlank(loginId)) {
				// 把loginId添加到Feign请求的头部，这样被调用的服务就可以接收到这个信息
				requestTemplate.header("loginId", loginId);
			}
		}
	}
}

