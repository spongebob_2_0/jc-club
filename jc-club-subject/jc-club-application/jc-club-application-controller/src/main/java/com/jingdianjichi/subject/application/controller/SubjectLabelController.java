package com.jingdianjichi.subject.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.subject.application.convert.SubjectLabelDTOConvert;
import com.jingdianjichi.subject.application.dto.SubjectLabelDTO;
import com.jingdianjichi.subject.common.entity.Result;
import com.jingdianjichi.subject.domain.entity.SubjectLabelBO;
import com.jingdianjichi.subject.domain.service.SubjectLabelDomainService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 标签Controller层
 *
 * @author: WuYimin
 * Date: 2024-02-04
  */
@RestController
@RequestMapping("/subject/label")
@Slf4j
public class SubjectLabelController {


    @Resource
    private SubjectLabelDomainService subjectLabelDomainService;

    /**
     * 新增标签
     *
     * @param subjectLabelDTO
     * @return
     */
    @PostMapping("/add")
    public Result<Boolean> add(@RequestBody SubjectLabelDTO subjectLabelDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectLabelController.add.dto:{}", JSON.toJSONString(subjectLabelDTO));
            }
            // 校验输入参数
            Preconditions.checkArgument(!StringUtils.isBlank(subjectLabelDTO.getLabelName()), "标签名称不能为空！");
            // 转换DTO为BO
            SubjectLabelBO subjectLabelBo = SubjectLabelDTOConvert.INSTANCE.convertDtoToLabelBo(subjectLabelDTO);
            // 调用领域服务添加分类
            Boolean result = subjectLabelDomainService.add(subjectLabelBo);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("SubjectLabelController.add.error：{}", e.getMessage(), e);
            return Result.fail("新增标签失败!");
        }
    }

    /**
     * 更新标签
     *
     * @param subjectLabelDTO
     * @return
     */
    @PostMapping("/update")
    public Result<Boolean> update(@RequestBody SubjectLabelDTO subjectLabelDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectLabelController.update.dto:{}", JSON.toJSONString(subjectLabelDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(subjectLabelDTO.getId(), "标签id不能为空！");
            // 转换DTO为BO
            SubjectLabelBO subjectLabelBo = SubjectLabelDTOConvert.INSTANCE.convertDtoToLabelBo(subjectLabelDTO);
            // 调用领域服务添加分类
            Boolean result = subjectLabelDomainService.update(subjectLabelBo);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("SubjectLabelController.update.error：{}", e.getMessage(), e);
            return Result.fail("更新标签失败!");
        }
    }

    /**
     * 删除标签
     *
     * @param subjectLabelDTO
     * @return
     */
    @PostMapping("/delete")
    public Result<Boolean> delete(@RequestBody SubjectLabelDTO subjectLabelDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectLabelController.delete.dto:{}", JSON.toJSONString(subjectLabelDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(subjectLabelDTO.getId(), "标签id不能为空！");
            // 转换DTO为BO
            SubjectLabelBO subjectLabelBo = SubjectLabelDTOConvert.INSTANCE.convertDtoToLabelBo(subjectLabelDTO);
            // 调用领域服务添加分类
            Boolean result = subjectLabelDomainService.delete(subjectLabelBo);
            return Result.ok(result);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("SubjectLabelController.delete.error：{}", e.getMessage(), e);
            return Result.fail("删除标签失败!");
        }
    }

    /**
     * 根据分类id查询标签
     *
     * @param subjectLabelDTO
     * @return
     */
    @PostMapping("/queryLabelByCategoryId")
    public Result<List<SubjectLabelDTO>> queryLabelByCategoryId(@RequestBody SubjectLabelDTO subjectLabelDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectLabelController.queryByCategory.dto:{}", JSON.toJSONString(subjectLabelDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(subjectLabelDTO.getCategoryId(), "标签id不能为空！");
            // 转换DTO为BO
            SubjectLabelBO subjectLabelBo = SubjectLabelDTOConvert.INSTANCE.convertDtoToLabelBo(subjectLabelDTO);
            // 调用领域服务通过分类id进行查询
            List<SubjectLabelBO> subjectLabelBOList = subjectLabelDomainService.queryByCategoryId(subjectLabelBo);
            // 转换BO为DTO
            List<SubjectLabelDTO> subjectLabelDTOS = SubjectLabelDTOConvert.INSTANCE.convertBoTOLabelDTOList(subjectLabelBOList);
            return Result.ok(subjectLabelDTOS);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("SubjectLabelController.queryByCategory.error：{}", e.getMessage(), e);
            return Result.fail("根据分类id查询标签失败!");
        }
    }

}
