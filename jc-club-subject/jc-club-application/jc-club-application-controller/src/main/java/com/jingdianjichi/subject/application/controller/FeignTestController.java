package com.jingdianjichi.subject.application.controller;

import com.alibaba.fastjson.JSON;
import com.jingdianjichi.subject.common.entity.PageResult;
import com.jingdianjichi.subject.infra.basic.entity.SubjectInfo;
import com.jingdianjichi.subject.infra.basic.entity.SubjectInfoEs;
import com.jingdianjichi.subject.infra.basic.service.SubjectEsService;
import com.jingdianjichi.subject.infra.basic.service.SubjectEsTestService;
import com.jingdianjichi.subject.infra.entity.UserInfo;
import com.jingdianjichi.subject.infra.rpc.UserRPC;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 刷题分类Controller
 * 负责处理与刷题分类相关的请求
 * @author: WuYimin
 * Date: 2024-02-03
  */
@RestController
@RequestMapping("/feign")
@Slf4j
public class FeignTestController {

    @Resource
    private UserRPC userRPC;

    @Resource
    private SubjectEsTestService subjectEsTestService;

    @Resource
    private SubjectEsService subjectEsService;

    /**
     * 远程调用接口测试
     * 获取用户信息
     * @return
     */
    @GetMapping("/testFeign")
    public UserInfo testFeign() {
        UserInfo userInfo = userRPC.getUserInfo("o-Pg16Vjvee8j--X3IzYCaTKpGmY");
        log.info("testFeign.userInfo：{}",userInfo);
        return userInfo;
    }

    /**
     * SpringData集成Es接口测试
     * 创建索引
     */
    @GetMapping("/testCreateMapping")
    public void testCreateMapping() {
        subjectEsTestService.createIndex();;
    }

    /**
     * SpringData集成Es接口测试
     * 创建文档
     */
    @GetMapping("/addDocs")
    public void addDocs() {
        subjectEsTestService.addDocs();;
    }

    /**
     * SpringData集成Es接口测试
     * 查看文档
     */
    @GetMapping("/find")
    public void find() {
        subjectEsTestService.find();
    }

    /**
     * SpringData集成Es接口测试
     * 匹配文档
     */
    @GetMapping("/search")
    public void search() {
        subjectEsTestService.search();
    }


    /**
     * 自定义es接口测试
     * 匹配文档
     * @return
     */
    @PostMapping("/querySubjectByKeyWord")
    public void querySubjectByKeyWord() {
        SubjectInfoEs subjectInfoEs = new SubjectInfoEs();
        subjectInfoEs.setKeyWord("Mysql");
        PageResult<SubjectInfoEs> pageResult = subjectEsService.querySubjectList(subjectInfoEs);
        log.info("querySubjectByKeyWord：{}", JSON.toJSONString(pageResult));
    }

}