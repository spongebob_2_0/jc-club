package com.jingdianjichi.subject.application.convert;


import com.jingdianjichi.subject.application.dto.SubjectCategoryDTO;
import com.jingdianjichi.subject.application.dto.SubjectInfoDTO;
import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现DTO（数据传输对象）与BO（业务对象）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface SubjectInfoDTOConvert {

    // 通过MapStruct的Mappers工厂类获取SubjectInfoDTOConvert的实例
    SubjectInfoDTOConvert INSTANCE = Mappers.getMapper(SubjectInfoDTOConvert.class);

    List<SubjectInfoDTO> convertBoToDTOList(List<SubjectInfoBO> subjectInfoBOList);

    // 定义一个方法，将SubjectInfoDTO转换为SubjectInfoBO
    SubjectInfoBO convertDtoToBo(SubjectInfoDTO subjectInfoDTO);

    // 定义一个方法，将 SubjectInfoBO 转换为 SubjectInfoDTO
    SubjectInfoDTO convertBoToDto(SubjectInfoBO subjectInfoBO);

}