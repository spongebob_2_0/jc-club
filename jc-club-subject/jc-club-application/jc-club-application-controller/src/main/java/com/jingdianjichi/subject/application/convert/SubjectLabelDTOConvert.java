package com.jingdianjichi.subject.application.convert;


import com.jingdianjichi.subject.application.dto.SubjectLabelDTO;
import com.jingdianjichi.subject.domain.entity.SubjectLabelBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 标签dto的转换
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface SubjectLabelDTOConvert {

    // 通过MapStruct的Mappers工厂类获取SubjectLabelDTOConvert的实例
    SubjectLabelDTOConvert INSTANCE = Mappers.getMapper(SubjectLabelDTOConvert.class);


    // 定义一个方法，将SubjectLabelBo列表转换为SubjectLabelDTO列表
    List<SubjectLabelDTO> convertBoTOLabelDTOList(List<SubjectLabelBO> subjectLabelBOList);

    // 定义一个方法，将SubjectLabelDTO转换为SubjectLabelBo
    SubjectLabelBO convertDtoToLabelBo(SubjectLabelDTO subjectLabelDTO);

    SubjectLabelDTO convertBoToLabelDTO(SubjectLabelBO subjectLabelBO);
}