package com.jingdianjichi.subject.application.convert;


import com.jingdianjichi.subject.application.dto.SubjectAnswerDTO;
import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现DTO（数据传输对象）与BO（业务对象）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface SubjectAnswerDTOConvert {

    // 通过MapStruct的Mappers工厂类获取SubjectAnswerDTOConvert的实例
    SubjectAnswerDTOConvert INSTANCE = Mappers.getMapper(SubjectAnswerDTOConvert.class);

    // 定义一个方法，将SubjectAnswerBO列表转换为SubjectAnswerDTO表
    List<SubjectAnswerBO> convertDTOToBOList(List<SubjectAnswerDTO> subjectAnswerDTOList);

    // 定义一个方法，将SubjectAnswerDTO转换为SubjectInfoBO
    SubjectInfoBO convertDtoToBo(SubjectAnswerDTO subjectAnswerDTO);

}