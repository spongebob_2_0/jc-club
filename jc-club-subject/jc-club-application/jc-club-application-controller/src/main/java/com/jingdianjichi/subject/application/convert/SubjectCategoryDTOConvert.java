package com.jingdianjichi.subject.application.convert;


import com.jingdianjichi.subject.application.dto.SubjectCategoryDTO;
import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 使用MapStruct库来自动实现DTO（数据传输对象）与BO（业务对象）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface SubjectCategoryDTOConvert {

    // 通过MapStruct的Mappers工厂类获取SubjectCategoryDTOConvert的实例
    SubjectCategoryDTOConvert INSTANCE = Mappers.getMapper(SubjectCategoryDTOConvert.class);

    // 定义一个方法，将SubjectCategoryDTO转换为SubjectCategoryBo
    //SubjectCategoryBO convertBoTOCategory(SubjectCategoryDTO subjectCategoryDTO);

    // 定义一个方法，将SubjectCategoryBo列表转换为SubjectCategoryDTO列表
    List<SubjectCategoryDTO> convertBoTOCategoryDTOList(List<SubjectCategoryBO> subjectCategoryBOList);

    // 定义一个方法，将SubjectCategoryDTO转换为SubjectCategoryBo
    SubjectCategoryBO convertDtoToCategoryBo(SubjectCategoryDTO subjectCategoryDTO);

    SubjectCategoryDTO convertBoToCategoryDTO(SubjectCategoryBO subjectCategoryBO);
}