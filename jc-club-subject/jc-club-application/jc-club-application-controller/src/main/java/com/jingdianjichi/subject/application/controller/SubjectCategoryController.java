package com.jingdianjichi.subject.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.subject.application.convert.SubjectCategoryDTOConvert;
import com.jingdianjichi.subject.application.convert.SubjectLabelDTOConvert;
import com.jingdianjichi.subject.application.dto.SubjectCategoryDTO;
import com.jingdianjichi.subject.application.dto.SubjectLabelDTO;
import com.jingdianjichi.subject.common.entity.Result;
import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import com.jingdianjichi.subject.domain.service.SubjectCategoryDomainService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

/**
 * 刷题分类Controller
 * 负责处理与刷题分类相关的请求
 * @author: WuYimin
 * Date: 2024-02-03
  */
@RestController
@RequestMapping("/subject/category")
@Slf4j
public class SubjectCategoryController {

    @Resource
    private SubjectCategoryDomainService subjectCategoryDomainService; // 注入题目分类的领域服务

    /**
     * 添加题目分类
     * @param subjectCategoryDTO
     * @return
     */
    @PostMapping("/add")
    public Result<Boolean> add(@RequestBody SubjectCategoryDTO subjectCategoryDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectCategoryController.add.dto:{}", JSON.toJSONString(subjectCategoryDTO));
            }
            // 校验输入参数
            Preconditions.checkNotNull(subjectCategoryDTO.getCategoryType(), "分类类型不能为空！");
            Preconditions.checkArgument(!StringUtils.isBlank(subjectCategoryDTO.getCategoryName()), "分类名称不能为空！");
            Preconditions.checkNotNull(subjectCategoryDTO.getParentId(), "分类父级ID不能为空！");
            // 转换DTO为BO
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDtoToCategoryBo(subjectCategoryDTO);
            // 调用领域服务添加分类
            subjectCategoryDomainService.add(subjectCategoryBo);
            return Result.ok(true);
        } catch (Exception e) {
            // 异常处理，记录错误日志并返回失败结果
            log.error("SubjectCategoryController.add.error：{}", e.getMessage(), e);
            return Result.fail("新增分类失败！");
        }
    }

    /**
     * 根据分类类型查询分类
     * @return
     */
    @PostMapping("/queryPrimaryCategory")
    public Result<List<SubjectCategoryDTO>> queryPrimaryCategory(@RequestBody SubjectCategoryDTO subjectCategoryDTO) {
        try {
            // 转换DTO为BO
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDtoToCategoryBo(subjectCategoryDTO);
            // 调用领域服务的queryCategory方法查询分类
            List<SubjectCategoryBO> subjectCategoryBOList = subjectCategoryDomainService.queryCategory(subjectCategoryBo);
            // 将查询结果的BO列表转换为DTO列表
            List<SubjectCategoryDTO> subjectCategoryDTOList = SubjectCategoryDTOConvert.INSTANCE.convertBoTOCategoryDTOList(subjectCategoryBOList);
            // 返回成功结果，包含DTO列表
            return Result.ok(subjectCategoryDTOList);
        } catch (Exception e) {
            log.error("SubjectCategoryController.queryPrimaryCategoryList.error：{}", e.getMessage(), e);
            return Result.fail("查询失败");
        }
    }

    /**
     *  更新分类
     * @param subjectCategoryDTO
     * @return
     */
    @PostMapping("/update")
    public Result<Boolean> update(@RequestBody SubjectCategoryDTO subjectCategoryDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectCategoryController.update.dto:{}", JSON.toJSONString(subjectCategoryDTO));
            }

            // 转换DTO为BO
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDtoToCategoryBo(subjectCategoryDTO);

            // 调用领域服务的update方法更新分类信息
            Boolean result = subjectCategoryDomainService.update(subjectCategoryBo);
            // 返回成功结果
            return Result.ok(result);
        } catch (Exception e) {
            log.error("SubjectCategoryController.update.error：{}", e.getMessage(), e);
            return Result.fail("更新分类失败！");
        }
    }

    /**
     * 删除分类
     * @param subjectCategoryDTO
     * @return
     */
    @PostMapping("/delete")
    public Result<Boolean> delete(@RequestBody SubjectCategoryDTO subjectCategoryDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectCategoryController.delete.dto:{}", JSON.toJSONString(subjectCategoryDTO));
            }

            // 转换DTO为BO
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDtoToCategoryBo(subjectCategoryDTO);

            // 调用领域服务的update方法删除分类信息
            Boolean result = subjectCategoryDomainService.delete(subjectCategoryBo);
            // 返回成功结果
            return Result.ok(result);
        } catch (Exception e) {
            log.error("SubjectCategoryController.delete.error：{}", e.getMessage(), e);
            return Result.fail("删除分类失败！");
        }
    }

    /**
     * 根据id（分类表中的父级id）查询下级分类
     * @param subjectCategoryDTO
     * @return
     */
    @PostMapping("/queryCategoryByPrimary")
    public Result<List<SubjectCategoryDTO>> queryCategoryByPrimary(@RequestBody SubjectCategoryDTO subjectCategoryDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectCategoryController.add.dto:{}", JSON.toJSONString(subjectCategoryDTO));
            }
            // 校验父级ID不能为空
            Preconditions.checkNotNull(subjectCategoryDTO.getParentId(),"父级id不能为空！");
            // 转换DTO为BO进行查询
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDtoToCategoryBo(subjectCategoryDTO);
            // 调用领域服务的queryCategory方法根据BO查询分类
            List<SubjectCategoryBO> subjectCategoryBOList = subjectCategoryDomainService.queryCategory(subjectCategoryBo);
            // 将查询结果的BO列表转换为DTO列表
            List<SubjectCategoryDTO> subjectCategoryDTOList = SubjectCategoryDTOConvert.INSTANCE.convertBoTOCategoryDTOList(subjectCategoryBOList);
            // 返回成功结果，包含DTO列表
            return Result.ok(subjectCategoryDTOList);
        } catch (Exception e) {
            log.error("SubjectCategoryController.queryCategoryByPrimary.error：{}", e.getMessage(), e);
            return Result.fail("查询失败");
        }
    }

    /**
     * 一次性查询分类及其标签
     * @param subjectCategoryDTO
     * @return
     */
    @RequestMapping("/queryCategoryAndLabel")
    public Result<List<SubjectCategoryDTO>> queryCategoryAndLabel(@RequestBody SubjectCategoryDTO subjectCategoryDTO) {
        try {
            // 记录日志
            if (log.isInfoEnabled()) {
                log.info("SubjectCategoryController.queryCategoryAndLabel.dto:{}", JSON.toJSONString(subjectCategoryDTO));
            }
            // 校验父级ID不能为空
            Preconditions.checkNotNull(subjectCategoryDTO.getId(),"id不能为空！");
            // 转换DTO为BO进行查询
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDtoToCategoryBo(subjectCategoryDTO);
            // 调用领域服务的queryCategory方法根据BO查询分类和标签
            List<SubjectCategoryBO> subjectCategoryBOList = subjectCategoryDomainService.queryCategoryAndLabel(subjectCategoryBo);
            List<SubjectCategoryDTO> dtoList = new LinkedList<>();
            subjectCategoryBOList.forEach(bo -> {
                SubjectCategoryDTO categoryDTO = SubjectCategoryDTOConvert.INSTANCE.convertBoToCategoryDTO(bo);
                List<SubjectLabelDTO> labelDTOList = SubjectLabelDTOConvert.INSTANCE.convertBoTOLabelDTOList(bo.getLabelBOList());
                categoryDTO.setLabelDTOList(labelDTOList);
                dtoList.add(categoryDTO);
            });
            // 返回成功结果，包含DTO列表
            return Result.ok(dtoList);
        } catch (Exception e) {
            log.error("SubjectCategoryController.queryCategoryAndLabel.error：{}", e.getMessage(), e);
            return Result.fail("查询失败");
        }
    }

}