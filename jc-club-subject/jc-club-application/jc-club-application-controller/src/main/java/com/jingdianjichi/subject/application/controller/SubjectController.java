package com.jingdianjichi.subject.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.subject.application.convert.SubjectAnswerDTOConvert;
import com.jingdianjichi.subject.application.convert.SubjectInfoDTOConvert;
import com.jingdianjichi.subject.application.dto.SubjectInfoDTO;
import com.jingdianjichi.subject.common.entity.PageResult;
import com.jingdianjichi.subject.common.entity.Result;
import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.service.SubjectInfoDomainService;
import com.jingdianjichi.subject.infra.basic.entity.SubjectInfoEs;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 题目控制器
 * 负责处理与题目相关的HTTP请求，如新增题目。
 *
 * @author: WuYimin
 * Date: 2024-02-02
  */
@RestController // 表示这是一个处理HTTP请求的控制器
@Slf4j // Lombok注解，为这个类提供一个日志对象
@RequestMapping("/subject")
public class SubjectController {

    @Resource
    private SubjectInfoDomainService subjectInfoDomainService; // 注入处理题目领域逻辑的服务

    /**
     * 新增题目
     * @param subjectInfoDTO 从请求体中获取的SubjectInfoDTO对象，包含了要新增的题目信息
     * @return 返回操作结果，成功时返回true，失败时返回错误信息
     */
    @PostMapping("/add")
    public Result<Boolean> add(@RequestBody SubjectInfoDTO subjectInfoDTO) {
        try {
            // 使用日志记录接收到的DTO信息，便于调试和记录操作
            if (log.isInfoEnabled()) {
                log.info("SubjectController.add.dto:{}", JSON.toJSONString(subjectInfoDTO));
            }
            // 使用Guava的Preconditions进行参数校验，确保必要信息完整
            Preconditions.checkArgument(!StringUtils.isBlank(subjectInfoDTO.getSubjectName()), "题目名称不能为空！");
            Preconditions.checkNotNull(subjectInfoDTO.getSubjectDifficult(), "题目难度不能为空！");
            Preconditions.checkNotNull(subjectInfoDTO.getSubjectType(), "题目类型不能为空！");
            Preconditions.checkNotNull(subjectInfoDTO.getSubjectScore(), "题目分数不能为空！");
            Preconditions.checkArgument(!CollectionUtils.isEmpty(subjectInfoDTO.getCategoryIds()), "分类id不能为空！");
            Preconditions.checkArgument(!CollectionUtils.isEmpty(subjectInfoDTO.getLabelIds()), "标签id不能为空！");

            // 将DTO对象转换为BO对象，准备进行业务逻辑处理
            SubjectInfoBO subjectInfoBO = SubjectInfoDTOConvert.INSTANCE.convertDtoToBo(subjectInfoDTO);

            // 转换DTO中的选项列表为BO列表，以供后续处理
            List<SubjectAnswerBO> subjectAnswerBOS = SubjectAnswerDTOConvert.INSTANCE.convertDTOToBOList(subjectInfoDTO.getOptionList());
            subjectInfoBO.setOptionList(subjectAnswerBOS);

            // 调用领域服务的add方法，传入BO对象进行业务处理
            subjectInfoDomainService.add(subjectInfoBO);

            // 如果操作成功，返回成功的结果
            return Result.ok(true);
        } catch (Exception e) {
            // 如果操作过程中发生异常，记录错误日志并返回失败的结果
            log.error("SubjectController.add.error：{}", e.getMessage(), e);
            return Result.fail("新增失败！");
        }
    }

    /**
     * 查询题目列表信息
     * @param subjectInfoDTO
     * @return
     */
    @PostMapping("/getSubjectPage")
    public Result<PageResult<SubjectInfoDTO>> getSubjectPage(@RequestBody SubjectInfoDTO subjectInfoDTO) {
        try {
            // 使用日志记录接收到的DTO信息，便于调试和记录操作
            if (log.isInfoEnabled()) {
                log.info("SubjectController.getSubjectPage.dto:{}", JSON.toJSONString(subjectInfoDTO));
            }
            // 使用Guava的Preconditions进行参数校验，确保必要信息完整
            Preconditions.checkNotNull(subjectInfoDTO.getCategoryId(), "分类id不能为空！");
            Preconditions.checkNotNull(subjectInfoDTO.getLabelId(), "标签id不能为空！");

            // 将DTO对象转换为BO对象，准备进行业务逻辑处理
            SubjectInfoBO subjectInfoBO = SubjectInfoDTOConvert.INSTANCE.convertDtoToBo(subjectInfoDTO);

            // 调用领域服务的 getSubjectPage 方法，传入BO对象进行业务处理
            PageResult<SubjectInfoBO> boPageResult = subjectInfoDomainService.getSubjectPage(subjectInfoBO);

            // 如果操作成功，返回成功的结果
            return Result.ok(boPageResult);
        } catch (Exception e) {
            // 如果操作过程中发生异常，记录错误日志并返回失败的结果
            log.error("SubjectController.getSubjectPage.error：{}", e.getMessage(), e);
            return Result.fail("分页查询失败！");
        }
    }

    /**
     * 查询题目详情
     * @param subjectInfoDTO
     * @return
     */
    @PostMapping("/querySubjectInfo")
    public Result<SubjectInfoDTO> querySubjectInfo(@RequestBody SubjectInfoDTO subjectInfoDTO) {
        try {
            // 使用日志记录接收到的DTO信息，便于调试和记录操作
            if (log.isInfoEnabled()) {
                log.info("SubjectController.querySubjectList.dto:{}", JSON.toJSONString(subjectInfoDTO));
            }
            // 使用Guava的Preconditions进行参数校验，确保必要信息完整
            Preconditions.checkNotNull(subjectInfoDTO.getId(), "题目id不能为空！");
            // 将DTO对象转换为BO对象，准备进行业务逻辑处理
            SubjectInfoBO subjectInfoBO = SubjectInfoDTOConvert.INSTANCE.convertDtoToBo(subjectInfoDTO);

            // 调用领域服务的 querySubjectInfo 方法，传入BO对象进行业务处理
            SubjectInfoBO boResult = subjectInfoDomainService.querySubjectInfo(subjectInfoBO);
            // 将业务处理的结果BO对象转换回DTO对象
            SubjectInfoDTO dto = SubjectInfoDTOConvert.INSTANCE.convertBoToDto(boResult);
            // 如果操作成功，返回成功的结果
            return Result.ok(dto);
        } catch (Exception e) {
            // 如果操作过程中发生异常，记录错误日志并返回失败的结果
            log.error("SubjectController.querySubjectList.error：{}", e.getMessage(), e);
            return Result.fail("查询题目详情失败！");
        }
    }


    /**
     * 通过关键词进行全文检索题目
     *
     * @param subjectInfoDTO 前端传递的题目信息数据传输对象，包含分页信息和搜索关键字
     * @return 统一返回结果对象，包含分页的题目信息
     */
    @PostMapping("/getSubjectPageBySearch")
    public Result<PageResult<SubjectInfoEs>> getSubjectPageBySearch(@RequestBody SubjectInfoDTO subjectInfoDTO) {
        try {
            // 记录日志，打印接收到的DTO信息
            if (log.isInfoEnabled()) {
                log.info("SubjectController.getSubjectPageBySearch.dto:{}", JSON.toJSONString(subjectInfoDTO));
            }
            // 校验关键字不能为空
            Preconditions.checkArgument(StringUtils.isNotBlank(subjectInfoDTO.getKeyWord()), "关键词不能为空");
            // 将DTO转换为业务对象BO
            SubjectInfoBO subjectInfoBO = SubjectInfoDTOConvert.INSTANCE.convertDtoToBo(subjectInfoDTO);
            // 设置分页信息
            subjectInfoBO.setPageNo(subjectInfoDTO.getPageNo());
            subjectInfoBO.setPageSize(subjectInfoDTO.getPageSize());
            // 调用领域服务执行搜索操作
            PageResult<SubjectInfoEs> boPageResult = subjectInfoDomainService.getSubjectPageBySearch(subjectInfoBO);
            // 操作成功，返回查询结果
            return Result.ok(boPageResult);
        } catch (Exception e) {
            // 捕获异常，记录错误日志，并返回失败信息
            log.error("SubjectCategoryController.getSubjectPageBySearch.error:{}", e.getMessage(), e);
            return Result.fail("全文检索失败");
        }
    }

    /**
     * 获取题目贡献排行榜
     */
    @PostMapping("/getContributeList")
    public Result<List<SubjectInfoDTO>> getContributeList() {
        try {
            // 调用领域服务获取题目贡献者的业务对象（BO）列表
            List<SubjectInfoBO> boList = subjectInfoDomainService.getContributeList();
            // 将BO列表转换为DTO列表
            List<SubjectInfoDTO> dtoList = SubjectInfoDTOConvert.INSTANCE.convertBoToDTOList(boList);
            // 封装DTO列表到Result对象中并返回
            return Result.ok(dtoList);
        } catch (Exception e) {
            // 异常处理：记录错误日志并返回包含错误信息的Result对象
            log.error("SubjectCategoryController.getContributeList.error:{}", e.getMessage(), e);
            return Result.fail("获取贡献榜失败");
        }
    }

}
