package com.jingdianjichi.subject.domain.hadler.subject;

import com.jingdianjichi.subject.common.enums.IsDeleteFlagEnum;
import com.jingdianjichi.subject.common.enums.SubjectInfoTypeEnum;
import com.jingdianjichi.subject.domain.convert.BriefSubjectConvert;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.entity.SubjectOptionBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectBrief;
import com.jingdianjichi.subject.infra.basic.service.SubjectBriefService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 简答题类型的策略类
 * 用于处理简答题的业务逻辑，如添加新的简答题到数据库。
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Component // 将该类标记为Spring的组件，使其被Spring容器管理
public class BriefTypeHandlerImpl implements SubjectTypeHandler {

	@Resource // 通过Spring注解自动注入SubjectBriefService服务
	private SubjectBriefService subjectBriefService;

	/**
	 * 指定处理器类型为简答题。
	 *
	 * @return 返回该处理器对应的题目类型枚举，这里是简答题（BRIEF）。
	 */
	@Override
	public SubjectInfoTypeEnum getHandlerType() {
		return SubjectInfoTypeEnum.BRIEF;
	}

	/**
	 * 添加简答题目的具体实现。
	 * 将题目信息转换为简答题实体并批量插入数据库。
	 *
	 * @param subjectInfoBO 需要添加的题目信息，封装在SubjectInfoBO业务对象中。
	 */
	@Override
	public void add(SubjectInfoBO subjectInfoBO) {

		// 将业务对象 Bo 转成 实体类对象 SubjectBrief
		SubjectBrief subjectBrief = BriefSubjectConvert.INSTANCE.convertBOToEntity(subjectInfoBO);
		subjectBrief.setSubjectId(Math.toIntExact(subjectInfoBO.getId())); // 设置题目ID，确保与题目信息对应
		subjectBrief.setIsDeleted(IsDeleteFlagEnum.UN_DELETED.getCode());  // 设置题目存在

		// 调用service层方法，插入简答题答案到数据库
		subjectBriefService.insert(subjectBrief);
	}

	/**
	 * 查询简答题目的详细信息
	 * 根据题目ID查询简答题的答案等信息，并封装返回。
	 *
	 * @param subjectId 题目ID
	 * @return 简答题的选项信息，封装在SubjectOptionBO中。
	 */
	@Override
	public SubjectOptionBO query(int subjectId) {
		SubjectBrief subjectBrief = new SubjectBrief();
		subjectBrief.setSubjectId(subjectId);
		// 根据题目ID查询简答题详细信息
		SubjectBrief result = subjectBriefService.queryByCondition(subjectBrief);
		SubjectOptionBO subjectOptionBO = new SubjectOptionBO(); // 创建返回对象
		subjectOptionBO.setSubjectAnswer(result.getSubjectAnswer()); // 设置简答题答案
		return subjectOptionBO; // 返回封装好的简答题选项信息
	}
}
