package com.jingdianjichi.subject.domain.service;

import com.jingdianjichi.subject.common.entity.PageResult;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectInfoEs;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 题目领域服务
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Service
public interface SubjectInfoDomainService {

	/**
	 * 新增题目
	 * @param subjectInfoBO
	 * @return
	 */
	void add(SubjectInfoBO subjectInfoBO);

	/**
	 * 分页查询
	 * @param subjectInfoBO
	 * @return
	 */
	PageResult<SubjectInfoBO> getSubjectPage(SubjectInfoBO subjectInfoBO);

	/**
	 * 查询题目详情
	 * @param subjectInfoBO
	 * @return
	 */
	SubjectInfoBO querySubjectInfo(SubjectInfoBO subjectInfoBO);

	/**
	 * 全文检索
	 * @param subjectInfoBO
	 * @return
	 */
	PageResult<SubjectInfoEs> getSubjectPageBySearch(SubjectInfoBO subjectInfoBO);

	/**
	 * 获取题目贡献榜
	 * @return
	 */
	List<SubjectInfoBO> getContributeList();
}
