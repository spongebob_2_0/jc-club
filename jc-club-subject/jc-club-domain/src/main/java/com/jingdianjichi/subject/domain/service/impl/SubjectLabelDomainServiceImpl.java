package com.jingdianjichi.subject.domain.service.impl;

import com.alibaba.fastjson.JSON;
import com.jingdianjichi.subject.common.enums.CategoryTypeEnum;
import com.jingdianjichi.subject.common.enums.IsDeleteFlagEnum;
import com.jingdianjichi.subject.domain.convert.SubjectLabelConvert;
import com.jingdianjichi.subject.domain.entity.SubjectLabelBO;
import com.jingdianjichi.subject.domain.service.SubjectLabelDomainService;
import com.jingdianjichi.subject.infra.basic.entity.SubjectCategory;
import com.jingdianjichi.subject.infra.basic.entity.SubjectLabel;
import com.jingdianjichi.subject.infra.basic.entity.SubjectMapping;
import com.jingdianjichi.subject.infra.basic.service.SubjectCategoryService;
import com.jingdianjichi.subject.infra.basic.service.SubjectLabelService;
import com.jingdianjichi.subject.infra.basic.service.SubjectMappingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 题目标签表(SubjectLabel)表服务实现类
 *
 * @author makejava
 * @since 2024-02-04 08:42:07
 */
@Service
@Slf4j
public class SubjectLabelDomainServiceImpl implements SubjectLabelDomainService {

	@Resource
	private SubjectLabelService subjectLabelService;

	@Resource
	private SubjectMappingService subjectMappingService;

	@Resource
	private SubjectCategoryService subjectCategoryService;

	/**
	 * 添加标签
	 *
	 * @param subjectLabelBo
	 * @return
	 */
	public Boolean add(SubjectLabelBO subjectLabelBo) {
		// 记录日志
		if (log.isInfoEnabled()) {
			log.info("SubjectCategoryController.add.dto:{}", JSON.toJSONString(subjectLabelBo));
		}
		// 将业务对象转换为实体对象
		SubjectLabel subjectLabel = SubjectLabelConvert.INSTANCE.convertBoToLabel(subjectLabelBo);
		// 设置该分类为未删除状态
		subjectLabel.setIsDeleted(IsDeleteFlagEnum.UN_DELETED.getCode());
		// 调用基础服务添加题目分类
		int count = subjectLabelService.insert(subjectLabel);
		return count > 0;
	}

	/**
	 * 更新标签
	 *
	 * @param subjectLabelBo
	 * @return
	 */
	public Boolean update(SubjectLabelBO subjectLabelBo) {
		// 记录日志
		if (log.isInfoEnabled()) {
			log.info("SubjectCategoryController.update.dto:{}", JSON.toJSONString(subjectLabelBo));
		}
		// 将业务对象转换为实体对象
		SubjectLabel subjectLabel = SubjectLabelConvert.INSTANCE.convertBoToLabel(subjectLabelBo);
		// 调用基础服务添加题目分类
		int count = subjectLabelService.update(subjectLabel);
		return count > 0;
	}

	/**
	 * 删除标签
	 *
	 * @param subjectLabelBo
	 * @return
	 */
	public Boolean delete(SubjectLabelBO subjectLabelBo) {
		// 记录日志
		if (log.isInfoEnabled()) {
			log.info("SubjectCategoryController.delete.dto:{}", JSON.toJSONString(subjectLabelBo));
		}
		// 将业务对象转换为实体对象
		SubjectLabel subjectLabel = SubjectLabelConvert.INSTANCE.convertBoToLabel(subjectLabelBo);
		// 设置该分类为删除状态
		subjectLabel.setIsDeleted(IsDeleteFlagEnum.DELETED.getCode());
		// 调用基础服务添加题目分类
		int count = subjectLabelService.update(subjectLabel);
		return count > 0;
	}

    /**
     * 根据分类id查询标签
     *
	 * 先用分类id从 subject_mapping表（用于映射分类表和标签表之间的关系）中查到所有符合要求的标签id，
	 * 然后使用Steam六流将所有标签id提取出来放到一个list集合中，再通过这个集合里面的所有标签id从数据库标签表中查出所有对应数据
	 * 最后使用foreach将查出来的所有数据封装到业务对象BO中返回
	 *
     * @param subjectLabelBo 传入的业务对象，包含了要查询的分类ID
     * @return 返回一个SubjectLabelBo的列表，包含了查询到的所有标签信息
     */
    public List<SubjectLabelBO> queryByCategoryId(SubjectLabelBO subjectLabelBo) {

		// 如果说当前分类是一级分类，则查询所有标签 （这个是对应前端出题模块的时候就会触发查询一级分类）
		SubjectCategory subjectCategory = subjectCategoryService.queryById(subjectLabelBo.getCategoryId());
		if(CategoryTypeEnum.PRIMARY.getCode() == subjectCategory.getCategoryType()) {
			SubjectLabel subjectLabel = new SubjectLabel();
			subjectLabel.setCategoryId(subjectLabelBo.getCategoryId());
			List<SubjectLabel> labelList = subjectLabelService.queryByCondition(subjectLabel);
			List<SubjectLabelBO> labelBOList = SubjectLabelConvert.INSTANCE.convertTOLabelBOList(labelList);
			return labelBOList;
		}

		// 从传入的业务对象中获取分类id
        Long categoryId = subjectLabelBo.getCategoryId();

        // 创建一个SubjectMapping对象，用于封装查询条件
        SubjectMapping subjectMapping = new SubjectMapping();
        subjectMapping.setCategoryId(categoryId); // 设置分类ID作为查询条件
        subjectMapping.setIsDeleted(IsDeleteFlagEnum.UN_DELETED	.getCode()); // 设置查询条件为未删除的记录

        // 调用subjectMappingService的queryLabelId方法，根据分类ID查询标签ID列表
        List<SubjectMapping> mappingList = subjectMappingService.queryLabelId(subjectMapping);

        // 如果查询结果为空，则直接返回一个空列表
        if (CollectionUtils.isEmpty(mappingList)) {
            return Collections.emptyList();
        }

        // 使用Java 8的流（Stream）处理，从mappingList中提取标签ID，并收集到lableIdList中
        List<Long> lableIdList = mappingList.stream().map(SubjectMapping::getLabelId).collect(Collectors.toList());

        // 调用subjectLabelService的batchQueryById方法，批量查询标签详细信息
        List<SubjectLabel> labelList = subjectLabelService.batchQueryById(lableIdList);

        // 创建一个空的SubjectLabelBo列表，用于存放转换后的标签业务对象
        List<SubjectLabelBO> boList = new LinkedList<>();
        // 遍历labelList，将每个SubjectLabel转换为SubjectLabelBo，并添加到boList中
        labelList.forEach(label -> {
            SubjectLabelBO bo = new SubjectLabelBO(); // 创建一个新的SubjectLabelBo对象
            bo.setId(label.getId()); // 设置标签ID
            bo.setLabelName(label.getLabelName()); // 设置标签名称
            bo.setCategoryId(categoryId); // 设置分类ID
			bo.setSortNum(label.getSortNum()); // 设置排序id
            boList.add(bo); // 将转换后的对象添加到列表中
        });
        // 返回转换后的标签业务对象列表
        return boList;
    }


}
