package com.jingdianjichi.subject.domain.convert;


import com.jingdianjichi.subject.domain.entity.SubjectLabelBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectLabel;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 负责Bo和实体类label之间的转换
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的Mapper注解标记这个接口为一个映射器
public interface SubjectLabelConvert {

    // 通过MapStruct的Mappers工厂类获取SubjectLabelConvert的实例
    SubjectLabelConvert INSTANCE = Mappers.getMapper(SubjectLabelConvert.class);

    // 定义一个方法，将SubjectLabelBo转换为SubjectLabel
    SubjectLabel convertBoToLabel(SubjectLabelBO subjectLabelBo);

    // 定义一个方法，将SubjectLabel列表转换为SubjectLabelBo列表
    List<SubjectLabelBO> convertTOLabelBOList(List<SubjectLabel> subjectLabelList);


}