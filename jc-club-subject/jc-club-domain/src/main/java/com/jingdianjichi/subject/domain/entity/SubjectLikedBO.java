package com.jingdianjichi.subject.domain.entity;

import com.jingdianjichi.subject.common.entity.PageInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 点赞表 bo
 *
 * @author wuyimin
 * @since 2024-02-21 08:04:54
 */
@Data
public class SubjectLikedBO extends PageInfo implements Serializable {

    /**
     * 主键id
     */
    private Long id;

    /**
     * 题目id
     */
    private Long subjectId;

    /**
     * 点赞人的id
     */
    private String likeUserId;

    /**
     * 点赞状态（0未点赞，1点赞）
     */
    private Integer status;

    /**
     * 创建人名称
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 修改人名称
     */
    private String updateBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 是否删除（0未删除，1删除）
     */
    private Integer isDeleted;

}

