package com.jingdianjichi.subject.domain.hadler.subject;

import com.jingdianjichi.subject.common.enums.IsDeleteFlagEnum;
import com.jingdianjichi.subject.common.enums.SubjectInfoTypeEnum;
import com.jingdianjichi.subject.domain.convert.MultipleSubjectConvert;
import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.entity.SubjectOptionBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectMultiple;
import com.jingdianjichi.subject.infra.basic.service.SubjectMultipleService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

/**
 * 多选题目的策略类
 * 实现SubjectTypeHandler接口，专门处理多选题目的增加操作。
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Component // 标识为Spring组件，使其被Spring容器管理
public class MultipleTypeHandlerImpl implements SubjectTypeHandler {

	@Resource // 注入多选题目相关的服务类
	private SubjectMultipleService subjectMultipleService;

	/**
	 * 指定处理器类型为多选题。
	 *
	 * @return 返回该处理器对应的题目类型枚举，这里是多选题（MULTIPLE）。
	 */
	@Override
	public SubjectInfoTypeEnum getHandlerType() {
		return SubjectInfoTypeEnum.MULTIPLE;
	}

	/**
	 * 实现添加多选题目的逻辑。
	 * 将题目业务对象（SubjectInfoBO）中的选项转换为多选题目实体（SubjectMultiple），然后批量插入到数据库中。
	 *
	 * @param subjectInfoBO 需要添加的题目信息业务对象。
	 */
	@Override
	public void add(SubjectInfoBO subjectInfoBO) {
		// 创建一个SubjectMultiple对象列表，用于存储转换后的多选题目选项
		List<SubjectMultiple> subjectMultipleList = new LinkedList<>();
		// 遍历SubjectInfoBO中的选项列表，将每个选项转换为SubjectMultiple对象
		subjectInfoBO.getOptionList().forEach(option -> {
			SubjectMultiple subjectMultiple = MultipleSubjectConvert.INSTANCE.convertBOToEntity(option);
			subjectMultiple.setSubjectId(subjectInfoBO.getId()); // 设置题目ID
			subjectMultiple.setIsDeleted(IsDeleteFlagEnum.UN_DELETED.getCode());  // 设置题目存在
			subjectMultipleList.add(subjectMultiple); // 添加到列表中
		});
		// 调用service层方法，批量插入多选题目选项到数据库
		subjectMultipleService.batchInsert(subjectMultipleList);
	}

	/**
	 * 查询多选题的详细信息
	 * 根据题目ID查询多选题的答案等信息，并封装返回。
	 *
	 * @param subjectId 题目ID
	 * @return 多选题的选项信息，封装在SubjectOptionBO中。
	 */
	@Override
	public SubjectOptionBO query(int subjectId) {
		// 创建SubjectJudge实例用于查询条件封装
		SubjectMultiple subjectMultiple = new SubjectMultiple();
		// 设置题目ID作为查询条件
		subjectMultiple.setSubjectId(Long.valueOf(subjectId));

		// 根据题目ID查询判断题的详细信息，包括答案
		List<SubjectMultiple> result = subjectMultipleService.queryByCondition(subjectMultiple);
		List<SubjectAnswerBO> subjectAnswerBOList = MultipleSubjectConvert.INSTANCE.convertEntityToBoList(result);

		// 创建SubjectOptionBO实例用于最终封装返回结果
		SubjectOptionBO subjectOptionBO = new SubjectOptionBO();
		// 将答案封装到 subjectOptionBO 对象中
		subjectOptionBO.setOptionList(subjectAnswerBOList);

		// 返回封装好的答案信息
		return subjectOptionBO;
	}

}
