package com.jingdianjichi.subject.domain.hadler.subject;

import com.jingdianjichi.subject.common.enums.IsDeleteFlagEnum;
import com.jingdianjichi.subject.common.enums.SubjectInfoTypeEnum;
import com.jingdianjichi.subject.domain.convert.JudgeSubjectConvert;
import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.entity.SubjectOptionBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectJudge;
import com.jingdianjichi.subject.infra.basic.service.SubjectJudgeService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 判断题目的策略类
 * 用于处理判断题类型的题目，实现SubjectTypeHandler接口，定义了特定于判断题的处理逻辑。
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Component // 表示这是一个Spring管理的组件
public class JudgeTypeHandlerImpl implements SubjectTypeHandler {

	@Resource // 通过Spring的@Resource注解自动注入SubjectJudgeService服务
	private SubjectJudgeService subjectJudgeService;

	/**
	 * 返回处理器类型为判断题。
	 *
	 * @return SubjectInfoTypeEnum.JUDGE，代表这个处理器处理的是判断题。
	 */
	@Override
	public SubjectInfoTypeEnum getHandlerType() {
		return SubjectInfoTypeEnum.JUDGE;
	}

	/**
	 * 添加判断题目的具体实现。
	 * 将题目信息转换为判断题实体并批量插入数据库。
	 *
	 * @param subjectInfoBO 需要添加的题目信息，封装在SubjectInfoBO业务对象中。
	 */
	@Override
	public void add(SubjectInfoBO subjectInfoBO) {
		// 创建一个SubjectJudge对象，用于存储判断题目答案
		SubjectJudge subjectJudge = new SubjectJudge();
		SubjectAnswerBO subjectAnswerBO = subjectInfoBO.getOptionList().get(0); // 获取题目的答案信息
		subjectJudge.setSubjectId(subjectInfoBO.getId()); // 设置题目ID
		subjectJudge.setIsCorrect(subjectAnswerBO.getIsCorrect()); // 设置答案是否正确
		subjectJudge.setIsDeleted(IsDeleteFlagEnum.UN_DELETED.getCode()); // 设置删除标志为未删除
		// 调用service层方法，插入判断题目答案到数据库
		subjectJudgeService.insert(subjectJudge);
	}

	/**
	 * 查询判断题的详细信息
	 * 根据题目ID查询判断题的答案等信息，并封装返回。
	 *
	 * @param subjectId 题目ID
	 * @return 判断题正确还是错误，封装在SubjectOptionBO中。
	 */
	@Override
	public SubjectOptionBO query(int subjectId) {
		// 创建SubjectJudge实例用于查询条件封装
		SubjectJudge subjectJudge = new SubjectJudge();
		// 设置题目ID作为查询条件
		subjectJudge.setSubjectId(Long.valueOf(subjectId));

		// 根据题目ID查询判断题的详细信息，包括答案
		List<SubjectJudge> result = subjectJudgeService.queryByCondition(subjectJudge);
		List<SubjectAnswerBO> subjectAnswerBOList = JudgeSubjectConvert.INSTANCE.convertEntityToBoList(result);

		// 创建SubjectOptionBO实例用于最终封装返回结果
		SubjectOptionBO subjectOptionBO = new SubjectOptionBO();
		// 设置判断题的选项列表（实际上是判断题的答案列表）
		subjectOptionBO.setOptionList(subjectAnswerBOList);

		// 返回封装好的判断题答案信息，这个对象包含了判断题所有可能的答案
		// 在判断题的上下文中，虽然答案是确定的（正确或错误），但是使用列表形式是为了保持接口的一致性
		return subjectOptionBO;
	}

}
