package com.jingdianjichi.subject.domain.convert;


import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectMultiple;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 定义了一个接口用于实现SubjectAnswerBO的业务对象（BO）与基础设施层实体（SubjectMultiple）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的@Mapper注解标记这个接口，让MapStruct处理器能够识别并生成实现
public interface MultipleSubjectConvert {

    // 使用 MapStruct 提供的Mappers工厂类来创建接口的实例
    MultipleSubjectConvert INSTANCE = Mappers.getMapper(MultipleSubjectConvert.class);

    // 定义从 SubjectAnswerBO（业务对象）SubjectMultiple（基础设施层实体）的转换方法
    SubjectMultiple convertBOToEntity(SubjectAnswerBO subjectAnswerBO);

    List<SubjectAnswerBO> convertEntityToBoList(List<SubjectMultiple> subjectMultipleList);


}