package com.jingdianjichi.subject.domain.service;

import com.jingdianjichi.subject.domain.entity.SubjectLabelBO;

import java.util.List;

/**
 * 题目标签领域服务
 *
 * @author makejava
 * @since 2024-02-04 08:42:07
 */
public interface SubjectLabelDomainService {


    /**
     * 新增标签
     * @param subjectLabelBo
     * @return
     */
    Boolean add(SubjectLabelBO subjectLabelBo);

    /**
     * 更新标签
     * @param subjectLabelBo
     * @return
     */
    Boolean update(SubjectLabelBO subjectLabelBo);

    /**
     * 删除标签
     * @param subjectLabelBo
     * @return
     */
    Boolean delete(SubjectLabelBO subjectLabelBo);

    /**
     * 根据分类id查询标签
     * @param subjectLabelBo
     * @return
     */
    List<SubjectLabelBO> queryByCategoryId(SubjectLabelBO subjectLabelBo);
}
