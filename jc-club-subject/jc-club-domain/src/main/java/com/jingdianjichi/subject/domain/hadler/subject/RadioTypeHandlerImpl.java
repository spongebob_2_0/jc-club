package com.jingdianjichi.subject.domain.hadler.subject;

import com.jingdianjichi.subject.common.enums.IsDeleteFlagEnum;
import com.jingdianjichi.subject.common.enums.SubjectInfoTypeEnum;
import com.jingdianjichi.subject.domain.convert.MultipleSubjectConvert;
import com.jingdianjichi.subject.domain.convert.RadioSubjectConvert;
import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.entity.SubjectOptionBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectMultiple;
import com.jingdianjichi.subject.infra.basic.entity.SubjectRadio;
import com.jingdianjichi.subject.infra.basic.service.SubjectRadioService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

/**
 * 单选题目的策略类
 * 实现SubjectTypeHandler接口，用于处理单选题目的具体逻辑。
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Service
public class RadioTypeHandlerImpl implements SubjectTypeHandler {

	@Resource
	private SubjectRadioService subjectRadioService; // 注入用于操作单选题目数据库的服务

	/**
	 * 返回该处理器对应的题目类型枚举。
	 *
	 * @return SubjectInfoTypeEnum.RADIO，表示这是处理单选题的处理器。
	 */
	@Override
	public SubjectInfoTypeEnum getHandlerType() {
		return SubjectInfoTypeEnum.RADIO;
	}

	/**
	 * 添加单选题目的实现方法。
	 * 将SubjectInfoBO对象转换为SubjectRadio对象，并调用service层方法将其持久化到数据库。
	 *
	 * @param subjectInfoBO 需要添加的题目信息，包含题目的详细信息和选项。
	 */
	@Override
	public void add(SubjectInfoBO subjectInfoBO) {
		// 创建一个SubjectRadio对象列表，用于存储转换后的单选题目选项
		List<SubjectRadio> subjectRadioList = new LinkedList<>();
		// 遍历SubjectInfoBO中的选项列表，将每个选项转换为SubjectRadio对象
		subjectInfoBO.getOptionList().forEach(option -> {
			SubjectRadio subjectRadio = RadioSubjectConvert.INSTANCE.convertBOToEntity(option);
			subjectRadio.setSubjectId(subjectInfoBO.getId()); // 设置题目ID
			subjectRadio.setIsDeleted(IsDeleteFlagEnum.UN_DELETED.getCode()); // 设置题目存在
			subjectRadioList.add(subjectRadio); // 添加到列表中
		});
		// 调用service层方法，批量插入单选题目选项到数据库
		subjectRadioService.batchInsert(subjectRadioList);
	}

	/**
	 * 查询单选题的详细信息
	 * 根据题目ID查询单选题的答案等信息，并封装返回。
	 *
	 * @param subjectId 题目ID
	 * @return 单选题的选项信息，封装在SubjectOptionBO中。
	 */
	@Override
	public SubjectOptionBO query(int subjectId) {
		// 创建 SubjectRadio 实例用于查询条件封装
		SubjectRadio subjectRadio = new SubjectRadio();
		// 设置题目ID作为查询条件
		subjectRadio.setSubjectId(Long.valueOf(subjectId));

		// 根据题目ID查询判断题的详细信息，包括答案
		List<SubjectRadio> result = subjectRadioService.queryByCondition(subjectRadio);
		List<SubjectAnswerBO> subjectAnswerBOList = RadioSubjectConvert.INSTANCE.convertEntityToBoList(result);

		// 创建SubjectOptionBO实例用于最终封装返回结果
		SubjectOptionBO subjectOptionBO = new SubjectOptionBO();
		// 将答案封装到 subjectOptionBO 对象中
		subjectOptionBO.setOptionList(subjectAnswerBOList);

		// 返回封装好的答案信息
		return subjectOptionBO;
	}
}
