package com.jingdianjichi.subject.domain.config;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class CacheConfig {

    // 使用@Bean注解声明一个方法，Spring容器会调用这个方法，并将方法返回的对象注册为一个bean。
    // 这里定义的bean是一个用于本地缓存的Cache实例，泛型为<String, String>，意味着这个缓存用于存储键值都是字符串类型的数据。
    @Bean
    public Cache<String, String> localCache() {
        // 使用Guava的CacheBuilder来构建一个Cache实例。
        return CacheBuilder.newBuilder()
                // 设置缓存的最大容量为5000个元素。
                // 当缓存大小达到这个阈值时，Guava的缓存会根据其算法（如LRU算法）来移除旧的元素，以确保缓存不会无限增长，避免消耗过多内存。
                .maximumSize(5000)
                // 设置缓存中的元素在写入后10秒内有效。
                // 过期策略是基于写操作的，也就是说，从最后一次写入开始计时，当达到指定的时间阈值后，缓存项就会被自动移除。
                .expireAfterWrite(10, TimeUnit.SECONDS)
                // 构建Cache实例。
                .build();
    }
}