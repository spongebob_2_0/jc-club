package com.jingdianjichi.subject.domain.convert;


import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.entity.SubjectOptionBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectInfo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 定义了一个接口用于实现SubjectInfoBO的业务对象（BO）与基础设施层实体（SubjectInfo）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的@Mapper注解标记这个接口，让MapStruct处理器能够识别并生成实现
public interface SubjectInfoConvert {

    // 使用MapStruct提供的Mappers工厂类来创建接口的实例
    SubjectInfoConvert INSTANCE = Mappers.getMapper(SubjectInfoConvert.class);

    // 定义从SubjectInfoBO（业务对象）SubjectInfo（基础设施层实体）的转换方法
    SubjectInfo convertBOToInfo(SubjectInfoBO subjectInfoBO);

    // 定义从SubjectInfo（基础设施层实体）列表到SubjectInfoBO（业务对象）列表的转换方法
    List<SubjectInfoBO> convertListInfoToBo(List<SubjectInfo> subjectInfoList);

    // 将 SubjectOptionBO 对象 转换到 SubjectInfoBO 对象
    SubjectInfoBO convertOptionToBo(SubjectOptionBO subjectOptionBO);

    SubjectInfoBO convertOptionAndInfoToBo(SubjectOptionBO subjectOptionBO, SubjectInfo subjectInfo);
}