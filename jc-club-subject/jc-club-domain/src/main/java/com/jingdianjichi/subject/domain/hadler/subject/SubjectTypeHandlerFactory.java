package com.jingdianjichi.subject.domain.hadler.subject;

import com.jingdianjichi.subject.common.enums.SubjectInfoTypeEnum;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 题目类型工厂
 * 用于根据题目类型获取相应的处理器。
 * 实现了InitializingBean接口，这允许在所有的bean属性被Spring容器设置之后，执行自定义初始化方法。
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Component
public class SubjectTypeHandlerFactory implements InitializingBean {

	@Resource
	private List<SubjectTypeHandler> subjectTypeHandlerList; // 注入所有的题目类型处理器

	private Map<SubjectInfoTypeEnum,SubjectTypeHandler> handlerMap = new HashMap<>(); // 存储题目类型与处理器的映射关系

	/**
	 * 根据题目类型返回对应的处理器。
	 *
	 * @param subjectType 题目类型的编码
	 * @return 对应的题目类型处理器
	 */
	public SubjectTypeHandler getHandler(int subjectType) {
		// 根据传入的题目类型编码，从枚举中获取对应的枚举实例
		SubjectInfoTypeEnum subjectInfoTypeEnum = SubjectInfoTypeEnum.getByCode(subjectType);
		// 从map中根据key（这里是指处理器的类型）获取对应的value（这是指处理器）
		return handlerMap.get(subjectInfoTypeEnum);
	}

	/**
	 * Spring容器设置完所有bean属性之后的回调方法，用于初始化映射关系。
	 *
	 * @throws Exception 如果初始化过程中发生错误
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		// 遍历注入的题目类型处理器列表
		for (SubjectTypeHandler subjectTypeHandler : subjectTypeHandlerList) {
			// 将每个处理器的处理类型与处理器本身存入映射中
			handlerMap.put(subjectTypeHandler.getHandlerType(), subjectTypeHandler);
		}
	}
}
