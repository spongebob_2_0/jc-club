package com.jingdianjichi.subject.domain.convert;


import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectCategory;
import com.jingdianjichi.subject.infra.basic.mapper.SubjectCategoryDao;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 定义了一个接口用于实现SubjectCategory的业务对象（BO）与基础设施层实体（Entity）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的@Mapper注解标记这个接口，让MapStruct处理器能够识别并生成实现
public interface SubjectCategoryConvert {

    // 使用MapStruct提供的Mappers工厂类来创建接口的实例
    SubjectCategoryConvert INSTANCE = Mappers.getMapper(SubjectCategoryConvert.class);

    // 定义从SubjectCategoryBo（业务对象）到SubjectCategory（基础设施层实体）的转换方法
    SubjectCategory convertBoTOCategory(SubjectCategoryBO subjectCategoryBo);

    // 定义从SubjectCategory（基础设施层实体）列表到SubjectCategoryBo（业务对象）列表的转换方法
    List<SubjectCategoryBO> convertCategoryToBoList(List<SubjectCategory> categoryList);

}