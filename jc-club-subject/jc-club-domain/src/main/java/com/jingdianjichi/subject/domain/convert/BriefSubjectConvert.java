package com.jingdianjichi.subject.domain.convert;


import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectBrief;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * 定义了一个接口用于实现SubjectAnswerBO的业务对象（BO）与基础设施层实体（SubjectBrief）之间的转换。
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
@Mapper // 使用MapStruct的@Mapper注解标记这个接口，让MapStruct处理器能够识别并生成实现
public interface BriefSubjectConvert {

    // 使用 MapStruct 提供的Mappers工厂类来创建接口的实例
    BriefSubjectConvert INSTANCE = Mappers.getMapper(BriefSubjectConvert.class);

    // 定义从 SubjectInfoBO（业务对象）SubjectBrief（基础设施层实体）的转换方法
    SubjectBrief convertBOToEntity(SubjectInfoBO subjectInfoBO);

}