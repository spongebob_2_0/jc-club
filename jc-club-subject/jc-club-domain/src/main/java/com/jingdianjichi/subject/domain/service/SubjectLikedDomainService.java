package com.jingdianjichi.subject.domain.service;


import com.jingdianjichi.subject.common.entity.PageResult;
import com.jingdianjichi.subject.domain.entity.SubjectLikedBO;

/**
 * 点赞表 领域service
 *
 * @author wuyimin
 * @since 2024-02-21 08:04:54
 */
public interface SubjectLikedDomainService {

    /**
     * 添加 点赞表 信息
     */
    void add(SubjectLikedBO subjectLikedBO);

    /**
     * 获取当前是否被点赞过
     */
    Boolean isLiked(String subjectId, String userId);

    /**
     * 获取点赞数量
     */
    Integer getLikedCount(String subjectId);

    /**
     * 更新 点赞表 信息
     */
    Boolean update(SubjectLikedBO subjectLikedBO);

    /**
     * 删除 点赞表 信息
     */
    Boolean delete(SubjectLikedBO subjectLikedBO);


    /**
     * 同步点赞数据
     */
    void syncLiked();


    /**
     * 查询我的点赞列表
     * @param subjectLikedBO
     * @return
     */
    PageResult<SubjectLikedBO> getSubjectLikedPage(SubjectLikedBO subjectLikedBO);
}
