package com.jingdianjichi.subject.domain.service;

import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;

import java.util.List;

/**
 * Description:
 *
 * @author: WuYimin
 * Date: 2024-02-03
  */
public interface SubjectCategoryDomainService {

    void add(SubjectCategoryBO subjectCategoryBo);

    /**
     * 查询岗位大类
     * @return
     */
    List<SubjectCategoryBO> queryCategory(SubjectCategoryBO subjectCategoryBo);

    /**
     * 更新分类
     * @param subjectCategoryBo
     * @return
     */
    Boolean update(SubjectCategoryBO subjectCategoryBo);

    /**
     * 删除分类
     * @param subjectCategoryBo
     * @return
     */
    Boolean delete(SubjectCategoryBO subjectCategoryBo);

    /**
     * 查询分类及标签
     * @param subjectCategoryBo
     * @return
     */
    List<SubjectCategoryBO> queryCategoryAndLabel(SubjectCategoryBO subjectCategoryBo);
}
