package com.jingdianjichi.subject.domain.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.*;

/**
 * 线程池config管理
 *
 * @author: WuYimin
 * Date: 2024-02-12
 */
@Configuration
public class TheadPoolConfig {

	/**
	 * 获取标签的线程池管理
	 * @return
	 */
	@Bean(name = "labelThreadPool")
	public ThreadPoolExecutor  getLabelThreadPool() {
			return new ThreadPoolExecutor(20,100,5,
					TimeUnit.SECONDS,new LinkedBlockingDeque<>(40),
					new CustomNameThreadFactory("label"), // 传入自己定义的线程前缀名
					new ThreadPoolExecutor.CallerRunsPolicy());
	}
}
