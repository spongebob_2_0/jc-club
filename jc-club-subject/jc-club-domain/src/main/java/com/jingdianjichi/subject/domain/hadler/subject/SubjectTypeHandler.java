package com.jingdianjichi.subject.domain.hadler.subject;

import com.jingdianjichi.subject.common.enums.SubjectInfoTypeEnum;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.entity.SubjectOptionBO;
import org.springframework.stereotype.Service;

/**
 * Description:
 *
 * @author: WuYimin
 * Date: 2024-02-05
  */
@Service
public interface SubjectTypeHandler {

	/**
	 * 枚举身份的识别
	 * @return
	 */
	SubjectInfoTypeEnum getHandlerType();

	/**
	 * 实际题目的插入
	 * @param subjectInfoBO
	 */
	void add(SubjectInfoBO subjectInfoBO);

	/**
	 * 查询
	 *
	 * @param subjectId
	 * @return
	 */
	SubjectOptionBO query(int subjectId);

}
