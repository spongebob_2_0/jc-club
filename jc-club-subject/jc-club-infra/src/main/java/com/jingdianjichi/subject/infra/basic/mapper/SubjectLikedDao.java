package com.jingdianjichi.subject.infra.basic.mapper;

import com.jingdianjichi.subject.infra.basic.entity.SubjectLiked;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 点赞表 表数据库访问层
 *
 * @author wuyimin
 * @since 2024-02-21 08:04:54
 */
@Repository
public interface SubjectLikedDao extends BaseMapper<SubjectLiked> {

	int countByCondition(SubjectLiked subjectLiked);

	List<SubjectLiked> queryPage(@Param("entity") SubjectLiked subjectLiked,
								 @Param("start") int start,
								 @Param("pageSize") Integer pageSize);

	int insertBatch(@Param("entities") List<SubjectLiked> entities);
}

