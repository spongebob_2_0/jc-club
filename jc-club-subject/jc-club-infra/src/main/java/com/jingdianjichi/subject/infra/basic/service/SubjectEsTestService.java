package com.jingdianjichi.subject.infra.basic.service;

import org.springframework.stereotype.Service;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-02-18
 */
@Service
public interface SubjectEsTestService {

	 void createIndex();

	 void addDocs();

	 void find();

	 void search();
}
