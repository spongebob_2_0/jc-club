package com.jingdianjichi.subject.infra.basic.service;

import com.jingdianjichi.subject.common.entity.PageResult;
import com.jingdianjichi.subject.infra.basic.entity.SubjectInfoEs;

public interface SubjectEsService {

    /**
     * 新增
     * @param subjectInfoEs
     * @return
     */
    boolean insert(SubjectInfoEs subjectInfoEs);

    /**
     * 查询
     * @param subjectInfoEs
     * @return
     */
    PageResult<SubjectInfoEs> querySubjectList(SubjectInfoEs subjectInfoEs);

}
