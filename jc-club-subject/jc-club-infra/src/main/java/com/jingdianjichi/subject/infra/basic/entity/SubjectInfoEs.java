package com.jingdianjichi.subject.infra.basic.entity;

import com.jingdianjichi.subject.common.entity.PageInfo;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Elasticsearch中的题目信息实体类
 * 继承了分页信息PageInfo，实现了Serializable接口，以便可以进行序列化
 */
@Data
public class SubjectInfoEs extends PageInfo implements Serializable {

    private Long subjectId; // 题目的唯一标识ID

    private Long docId; // 文档ID，用于Elasticsearch中的唯一标识

    private String subjectName; // 题目名称

    private String subjectAnswer; // 题目答案

    private String createUser; // 题目创建者

    private Long createTime; // 题目创建时间，存储的是时间戳

    private Integer subjectType; // 题目类型

    private String keyWord; // 搜索关键词，用于查询时匹配题目名称或答案

    private BigDecimal score; // 题目相关度评分，用于搜索结果的排序

}
