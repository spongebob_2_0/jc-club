package com.jingdianjichi.subject.infra.basic.esRepo;

import com.jingdianjichi.subject.infra.basic.entity.SubjectInfoTestES;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-02-18
 */
public interface SubjectESRepository extends ElasticsearchRepository<SubjectInfoTestES,Long> {

	
}
