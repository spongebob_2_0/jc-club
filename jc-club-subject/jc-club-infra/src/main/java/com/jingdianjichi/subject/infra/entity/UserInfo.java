package com.jingdianjichi.subject.infra.entity;

import lombok.Data;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-02-16
 */
@Data
public class UserInfo {

	private String userName;

	private String nickName;

	private String avatar;

}
