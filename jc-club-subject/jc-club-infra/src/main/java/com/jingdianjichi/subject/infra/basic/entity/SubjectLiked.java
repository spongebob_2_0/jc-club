package com.jingdianjichi.subject.infra.basic.entity;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

/**
 * 点赞表 实体类
 *
 * @author wuyimin
 * @since 2024-02-21 08:04:54
 */
@Data
@TableName("subject_liked")
public class SubjectLiked implements Serializable {

    /**
     * 主键id
     */
    @TableId(value = "`id`", type = IdType.AUTO)
    private Long id;

    /**
     * 题目id
     */
    @TableField("`subject_id`")
    private Long subjectId;

    /**
     * 点赞人的id
     */
    @TableField("`like_user_id`")
    private String likeUserId;

    /**
     * 点赞状态（0未点赞，1点赞）
     */
    @TableField("`status`")
    private Integer status;

    /**
     * 创建人名称
     */
    @TableField("`created_by`")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField("`created_time`")
    private Date createdTime;

    /**
     * 修改人名称
     */
    @TableField("`update_by`")
    private String updateBy;

    /**
     * 修改时间
     */
    @TableField("`update_time`")
    private Date updateTime;

    /**
     * 是否删除（0未删除，1删除）
     */
    @TableField("`is_deleted`")
    private Integer isDeleted;

}

