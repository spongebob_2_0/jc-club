package com.jingdianjichi.subject.infra.rpc;

import com.jingdianjichi.auth.api.UserFeignService;
import com.jingdianjichi.auth.entity.AuthUserDTO;
import com.jingdianjichi.auth.entity.Result;
import com.jingdianjichi.subject.infra.entity.UserInfo;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Description
 *
 * @author: WuYimin
 * Date: 2024-02-16
 */
@Component
public class UserRPC {

	@Resource
	private UserFeignService userFeignService;

	public UserInfo getUserInfo(String username) {
		AuthUserDTO authUserDTO = new AuthUserDTO();
		authUserDTO.setUserName(username);
		Result<AuthUserDTO> result = userFeignService.getUserInfo(authUserDTO);
		UserInfo userInfo = new UserInfo();
		if(!result.getSuccess()) {
			return userInfo;
		}
		AuthUserDTO data = result.getData();
		userInfo.setUserName(data.getUserName());
		userInfo.setNickName(data.getNickName());
		userInfo.setAvatar(data.getAvatar());
		return userInfo;
	}

}
