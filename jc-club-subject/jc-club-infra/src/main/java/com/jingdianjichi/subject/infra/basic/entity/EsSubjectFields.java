package com.jingdianjichi.subject.infra.basic.entity;

/**
 * 定义了Elasticsearch中题目信息文档的字段名称常量类
 * 用于确保在应用程序中引用这些字段时的一致性和减少出错的概率
 * 该类不需要实例化，所有字段都是静态常量，直接通过类名访问。
 */
public class EsSubjectFields {

    // 文档ID字段，每个文档在Elasticsearch中的唯一标识
    public static final String DOC_ID = "doc_id";

    // 题目ID字段，题目的唯一标识
    public static final String SUBJECT_ID = "subject_id";

    // 题目名称字段
    public static final String SUBJECT_NAME = "subject_name";

    // 题目答案字段
    public static final String SUBJECT_ANSWER = "subject_answer";

    // 题目类型字段，用于区分题目的不同类别
    public static final String SUBJECT_TYPE = "subject_type";

    // 创建用户字段，记录题目的创建者
    public static final String CREATE_USER = "create_user";

    // 创建时间字段，记录题目的创建时间
    public static final String CREATE_TIME = "create_time";

    // 用于查询的字段列表，包含了上述所有字段，方便在执行查询操作时引用
    public static final String[] FIELD_QUERY = {
            SUBJECT_ID, SUBJECT_NAME, SUBJECT_ANSWER, SUBJECT_TYPE, DOC_ID, CREATE_USER, CREATE_TIME
    };

}
